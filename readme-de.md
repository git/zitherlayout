# PiperSoft ZitherLayout
PiperSoft ZitherLayout ist ein kleines Programm, mit dem Noten im Zither-typischen Stil (Unterlegnoten) erstellt werden können. \
Dafür werden von diesem Programm Einstellungen und Noten aus Textdateien eingelesen, und daraus eine LaTeX-Datei erstellt. \
Diese kann dann mit LaTeX übersetzt werden, hierfür wird MusixTeX benötigt.

## Aufruf
Das Programm wird wie folgt aufgerufen:
```
PS_ZitherLayout [-q|-v] [-p|-c] [-s <setfile>]* [-S <set>]* [-i <infile>*]* [-O <outmode>] [-o <outfile>]
```
(`*` bedeutet, mehrere gleiche Argumente sind möglich) \
Jede Option hat ein Kurz- und ein Langformat. Wie unter Linux üblich werden Kurzformen mit einem einfachen (`-`) und Langformen mit einem doppelten Bindestrich (`--`) eingeleitet.

### Programmargumente
Folgende Optionen (Argumente) sind definiert:
* `-q  --quiet` \
    'Quiet'-Modus einschalten \
    es werden nur Fehler und Warnungen auf die Standardfehlerausgabe geschrieben \
    wenn weder `-v` noch `-q` übergeben werden, läuft das Programm im 'normalen' Modus und gibt einige Statusinformationen (und natürlich Fehler und Warnungen) aus
* `-v  --verbose` \
    'Verbose'-Modus einschalten \
    es werden mehr Informationen ausgegeben
* `-p  --part` \
    aktiviert den Teil-Modus, es werden nur Teil-Dokumente erzeugt, diese können später mit `-c` (`--combine`) kombiniert werden
* `-c  --combine` \
    kombiniert mehrere Teil-Dokumente (mit `-i` angegeben) zu einem Ausgabedokument
* `-s  --settings-file  <setfile>` \
    Einstellungsdatei angeben (wird als normale Eingabedatei eingelesen, siehe [Eingabedateiformat](#eingabedateiformat)) \
    `<setfile>` ist der Name der Einstellungsdatei
* `-S  --set  <set>` \
    Feldwert angeben \
    Feldwerte, die über die Kommandozeile übergeben werden, überschreiben entsprechende Werte aus den Eingabedateien, können also für 'letzte Änderungen' oder temporäre Einstellungen benutzt werden (wie 'g=1') \
    `<set>` ist der Feldname und der Wert, siehe [Eingabeformat für Feldwerte](#eingabeformat-für-feldwerte)
* `-i  --input-file  <infile>*` \
    Eingabedatei angeben \
    wenn keine Eingabedatei (aber eventuell eine oder mehrere Einstellungsdateien) angegeben wurde, wird zusätzlich die Standardeingabe gelesen \
    `<infile>` ist der Name der Eingabedatei, mehrere Dateien können angegeben werden
* `-O  --output-mode  <outmode>` \
    Ausgabemodus angeben, siehe auch [Ausgabeformate](#ausgabeformate) \
    `<outmode>` ist der gewünschte Modus: \
    * `z` | `zither` : erstellt [Unterlegnoten](#unterlegnoten) (Voreinstellung)
    * `l` | `lyrics` : erzeugt [Liedtexte im normalen Modus](#normaler-modus) \
        *ersetzt die obsolete Option `-l 0` (`--lyrics-mode 0`)*
    * `lc` | `lyrics-cmd` : erzeugt [Liedtexte als LaTeX-Kommandos](#latex-kommandos) \
        *ersetzt die obsolete Option `-l 1` (`--lyrics-mode 1`)*
    * `lca` | `lyrics-cmd-alt` : erzeugt [Liedtexte als LaTeX-Kommandos für alternative Texte](#latex-kommandos-alternative-texte) \
        *ersetzt die obsolete Option `-l 2` (`--lyrics-mode 2`)*
    * `nc` | `note-codes` : gibt die [Notenkodierung](#notenkodierung) im Textformat aus (z.B. nach dem Transponieren)
    * `mtx` : gibt die Notem im M-TX-Format aus, z.B. für die Erstellung von [Gesangsnoten](#gesangsnoten)
    * `midi` : erzeugt Ausgabe im [MIDI-Format](#midi-dateien)
* `-o  --output-file  <outfile>` \
    Ausgabedatei angeben \
    wenn keine Ausgabedatei angegeben ist, wird die Standardausgabe genutzt \
    `<outfile>` ist der Name der Ausgabedatei
* `-h  --help` \
    kurze Hilfe ausgeben und beenden

Eingabe- und Einstellungsdateien werden in der auf der Kommandozeile übergebenen Reihenfolge eingelesen. Jede Eingabedatei kann dabei Feldwerte aus vorhergehenden Eingabedateien überschreiben, fügt jedoch Noten immer! hinzu. \
Wenn eine Datei `~/.PiperSoft/ZitherLayout/default-settings` existiert, wird diese vor allen anderen Eingabe- und Einstellungsdateien gelesen.

In der Voreinstellung wird durch das Programm eine Datei `<file>.tex` erzeugt (wenn `-o=<file>.tex` angegeben wurde). \
Zur Erstellung einer PDF-Datei für die Unterlegnoten wird danach das Kommando `pdflatex <file>` aufgerufen, wobei `<file>` den Namen der Ausgabedatei (siehe Option `-o`) ohne Endung angibt.


## Eingabedateiformat

Jede Eingabedatei kann Noten und Feldwerte enthalten, zu den Feldwerten gehören Liedinformationen wie z.B. Strophentexte oder der Titel, sowie Ausgabe-Einstellungen wie Strophenpositionen, Saitenabstände und viele mehr. \
Jede Zeile kann dabei einen oder meherere Einträge enthalten, welche durch Leerzeichen (Tabuatoren werden wie Leerzeichen behandelt) getrennt werden. Eine Ausnahme bilden Feldwertzeilen, die mit einem `@` beginnen (siehe unten), hier werden Leerzeichen als Teil des Wertes behandelt (wichtig z.B. für Strophentexte). Leerzeichen am Anfang einer Zeile werden ignoriert. \
Jede Zeile, die mit `;` beginnt wird ignoriert, und kann für Kommentare genutzt werden, oder auch für bestimmte Inhalte, die zwar nicht gelöscht werden sollen, aber derzeit auch nicht in der Ausgabe landen sollen. \
Jede mit `\` oder `@` beginnende Zeile enthält Feldwerte, also Einstellungen oder Liedinformationen. Beginnt die Zeile mit `\`, endet der Feldwert beim folgenden Leerzeichen, in einer `@`-Zeile endet der Wert am Zeilenende. \
Alle anderen Zeilen beinhalten die Noten, Pause, Notengruppen und Separatoren. \
Jede Eingabezeile kann ein `;` enthalten, der nachfolgende Text wird dann als Kommentar behandelt und ignoriert. Eine Ausnahme bilden wieder mit `@` beginnende Zeilen, hier ist auch das `;` selbst sowie der nachfolgende Text Teil des Wertes. \
Ein spezielles Feld `end` markiert das Ende der Eingabe, und kann z.B. auf der Standardeingabe genutzt werden.

Spezielle Bedeutung haben Zeilen, die mit `#` beginnen, diese können genutzt werden, um wie beispielsweise in der Programmiersprache C üblich eine Warnung (`#warning <message>`) oder einen Fehler (`#error <message>`) auf der Konsole auszugeben, sobald die Zeile gelesen wurde. Im Falle eines Fehlers (`#error`) wird PS ZitherLayout danach mit einem Fehlercode beendet, im Falle einer Warnung läuft das Programm normal weiter.

In den folgenden Formatbeschreibungen repräsentiert `<xx>` einen Wert, `[xx]` ein optionales Argument und `x|y` mögliche alternative Werte.


### Eingabeformat für Noten
Jeder Noteneintrag enthält den Notenwert (Länge), die Tonhöhe und optional einen Akkord und hat folgendes Format:
```
[N][<länge>][.][+|-]<tonname>[<oktave>][(A|C)<akkord>[.|_]]*
```
wobei die einzelnen Felder folgende Bedeutungen haben:
* `[N]` : optionale Markierung für eine Note, siehe auch [Pausen](#pausennotation), [Separierung](#separierung-von-liedteilen) und [Markierungen](#nutzung-von-markierungen-an-melodieelementen)
* `[<länge>]` : Grundlänge des Notenwerts, gültig sind `1` (ganze Note), `2` (halbe Note), `4` (Viertel), `8`, ... \
    wird auf Vorgabelänge gesetzt, wenn nicht angegeben *)
* `[.]` : markiert eine punktierte Note
* `[+|-]` : erhöht / verringert die Notenhöhe um einen Halbton
* `<tonname>` : Name des Tons (`a`-`h`)
* `[<oktave>]` : Oktave (`1`-x) \
    wird auf Vorgabewert gesetzt, wenn nicht angegeben *)
* `A|C` : gibt an, dass Akkord folgt (eng. chord)
* `<akkord>` : Akkordnummer (`1`-x)
* `[.|_]` : Akkordtyp (produziert punktierten oder unterstrichenen Akkord)
* `*` mehrere Akkorde können angegeben werden

*) Die Vorgabelänge ist 4 (Viertelnote), Vorgabeoktave ist 1. Diese Werte können mit bestimmten Kommandos geändert werden, siehe `dl` / `do`. \
Welche Tonhöhen genutzt werden können, hängt von den Minimal- und Maximal-Werten ab, normalerweise `c1` bis `c3`.

#### Beispiele
  Code          |   Ergebnis
----------------|-----------------------------------------------
`4g1`           | Viertelnote der Höhe g1
`2.e2A2`        | punktierte halbe Note der Höhe e2 mit Akkord 2
`32-h1A3_`      | 32tel Note auf b1 mit unterstrichenem Akkord 3
`c d e f g`     | Anfang der C-Dur-Tonleiter mit Vorgabelänge in der Vorgabeoktave
`2.e1A1A1.A1.`  | punktierte halbe Note auf e1 mit 3 zu spielenden Akkorden

### Pausennotation
Pausen können im folgenden Format notiert werden:
```
P[<länge>][.][+|-]<tonname>[<oktave>][(A|C)<akkord>[.|_]]*
```
wobei `P` eine Pause kennzeichnet, und die anderen Felder denen für 'normale' Noten entsprechen.

Die Tonhöhe ist erforderlich um das Pausenzeichen positionieren zu können. Auch Akkorde können während einer Pause (in der Melodie) gespielt werden.

#### Beispiele
  Code          |   Ergebnis
----------------|------------------------------------------------
`P4g1`          | Viertelpause der Höhe g1
`P2.e2A2`       | punktierte halbe Pause der Höhe e2 mit Akkord 2

### Gruppierung von Noten und Pausen
Sollten in einem Musikstück mehrere Noten gleicher Tonhöhe aufeinander folgen, können diese gruppiert werden. Dann werden sie auf dem Unterlegblatt an der 'gleichen' Stelle direkt nebeneinander angezeigt. Dadurch wird verhindert, dass viele Zeilen im Layout verbraucht werden, da ansosten durch die gleichbleibende Tonhöhe keine horizontale Anordnung möglich ist. Gruppierte Noten und Pausen belegen demnach (nahezu) nur den Platz einer einfachen Noten der gleichen Tonhöhe. \
Notengruppen (die auch Pausen enthalten können) werden im folgenden Format notiert:
```
{ <note|pause>* }
```
zu gruppierende Noten und Pausen werden also in geschweiften Klammern zusammengefasst. Die Notation der einzelnen Noten und Pausen bleibt unverändert, mit folgenden Einschränkungen:
* jede Note und Pause einer Gruppe muss die gleiche Tonhöhe vorweisen
* jede Note oder Pause darf nur einen Akkord haben

#### Notengruppen mit Haltebogen
Wenn die Noten einer Gruppe einen einzelnen auszuhaltenden Ton abbilden, können diese mit einem Haltebogen verbunden werden. Dazu wird wieder eine Notengruppe erstellt (siehe oben), wobei die geschweiften Klammern durch runde ersetzt werden:
```
( <note|pause>* )
```

Gruppen mit Haltebogen sind hilfreich, wenn die benötigten Notenlängen nicht anderweitig abgebildet werden können, beispielsweise eine Viertelnote gefolgt von einer Ganzen, ohne erneuten Saitenanschlag.

#### Beispiele
  Code             |   Ergebnis
-------------------|-----------------------------------------------------------------
`{ 8g1 8g1 8g1 }`  | drei gruppierte Achtelnoten der Höhe g1
`{ 2.e1A2 4e1A1 }` | Gruppe mit einer punktierten halben und einer Viertelnote der Höhe e1, jeweils mit eigenem Akkord
`( 2.e1A2 4e1A1 )` | Gruppe der gleichen Noten mit Haltebogen

### Separierung von Liedteilen
Um mehrere Teile eines Liedes visuell von einander zu trennen können Separatoren eingefügt werden. Das sind freie Bereiche, auf denen keine Noten ausgegeben werden.

Für Separatoren wird folgendes Format genutzt:
```
S[<höhe>][<linienstil>[<position>]]
```
wobei die einzelnen Felder folgende Bedeutung haben:
* `S` : markiert einen Separator
* `[<höhe>]` : Höhe des Separators, Vorgabewert ist 1 \
    \>1, wenn eine Linie ausgegeben werden soll
* `[<linienstil>]` : Stil der Teilungslinie:
    * `s` : *(space)* keine Teilungslinie erzeugen (Vorgabe)
    * `l` : *(line)* durchgezogene Linie ausgeben
    * `d` : *(dotted line)* gepunktete Linie erzeugen
* `[<position>]` : Position der Teilungslinie von oben, Vorgabewert ist 1 (ganz oben) \
    muss kleiner als die Höhe des Separators sein

Separatoren können zum Beispiel genutzt werden, um Strophen und Refrain eines Liedes getrennt zu notieren.

#### Beispiele
  Code          |   Ergebnis
----------------|-----------------------------------------------------------------
`S`             | leerer Separator (der Höhe 1)
`S5d`           | Separator der Höhe 5 mit gepunkteter Linie ganz oben
`S4l3`          | Separator der Höhe 4 mit durchgezogener Linie am underen Rand


### Nutzung von Markierungen an Melodieelementen
Sogenannte (Positions-) Markierungen können genutzt werden, um später auf die Position eines Melodieelements (einer Note, Pause, Gruppe oder einer Separierung) zuzugreifen, um z.B. einen Strophentext oder einen Zusatztext relativ zu diesem Element zu positionieren.

Eine Markierung innerhalb der Melodie bezieht sich immer auf das **folgende** Melodieelement.

Folgendes Format wird für Markierungen innerhalb der Melodie genutzt:
```
M<id>
```
wobei die folgenden Felder definiert sind:
* `M` : startet eine Markierungsdefinition
* `<id>` : is eine Markernummer

#### Beispiele
  Code          |   Ergebnis
----------------|-----------------------------------------------------------------
`M1 4g1 M2 4a1` | setzt zwei Markierungen mit den Nummern `1` und `2` auf verschiedene Notenpositionen
`4e1 M3 M4 4d1` | setzt zwei Markierungen `3` and `4` auf die gleiche Notenposition (von `4d1`)


### Eingabeformat für Feldwerte
Um Feldwerte (also Liedinformationen oder Einstellungen) einzugeben, muss die Zeile mit `\` oder `@` begonnen werden, siehe oben. Jeder Eintrag wird dabei im folgenden Format angegeben:
```
<name>[op]=<wert>
```
wobei die einzelnen Felder folgende Bedeutungen haben:
* `<name>` : Feld-Name (siehe unten) \
    jedes Feld hat eine Kurz- und eine Langform, beide sind gleichwertig \
    bei Eingabe eines unbekannten Feldnamens wird eine Warnung ausgegeben, und der Eintrag ignoriert
* `[op]` : optionaler Operator \
    folgende Operatoren werden unterstützt:
    * `+`  angegebener Wert wird zum aktuellen Feldwert addiert (oder Zeichenketten aneinander gehängt)
    * `-` angegebener Wert wird vom aktuellen Feldwert abgezogen (nur für numerische Werte)
* `<wert>`  : zu setzender Feldwert \
    hier kann mittels `@<name2>` oder `^<name2>` auf den Wert oder eine Komponente eines beliebigen Feldes referenziert werden, siehe unten \
    *(`\<name2>` geht hier nicht, da oft LaTeX-Befehle genutzt werden, die ebenfalls mit `\` beginnen)*

#### Feldreferenzen
Bei der definition von Feldwerten kann der (komplette) Wert eines anderen Feldes übernommen werden, indem als Wert die Feldreferenz im Format `@<name2>` angegeben wird. Hierbei wird der Wert des Feldes mit dem Namen `name2` kopiert.

In einigen Fällen ist es notwendig, einzelne Komponenten eines anderen Feldewertes zu übernehmen, das wird durch die `^<name2>`-Notation erreicht. Hierbei wird nur eine bestimmte Komponente des Feldes auf den Wert der gleichen Komponente eines anderen Feldes `name2` gesetzt. \
Mit dieser Vorgehensweise ist es beispielsweise möglich, die Position einer Strophe zu definieren, wobei der Spaltenwert eines Feldes und der Zeilenwert eines anderen Feldes übernommen werden.

#### Beispiele
Code | Ergebnis
-----|----------
`@l1=Dies ist die erste Strophe` | definiert den Strophentext der ersten Strophe
`\lp1=1,1`                       | definiert die Position für die erste Strophe
`\ l2=@l1 l2+=nochmal`           | setzt den Strohpentext für die zweite Strophe, wobei der Text der ersten Strophe kopiert, und dann ergänzt wird
`\lp2=@lp1 lp2+=0,2`             | definiert die Position der zweiten Strophe genau 2 Zeilen unter der ersten Strophe
`\lp3=@M1`                       | setzt die Position für die dritte Strophe auf die mit Markierung `1` versehene Position
`@at1=^lp1,5,ein kleiner Text`   | definiert den ersten Zusatztext `ein kleiner Text`, welcher in der gleichen Spalte wie die erste Strophe in der festen Zeile `5` ausgegebn wird
`@at2=2,6,^at1`                  | setzt den zweiten Zusatztext, welches den gleichen Text vom ersten Zusatztext auf einer anderen (festen) Position ausgibt


#### Felder
folgende Felder stehen zur Verfügung, wobei teilweise kurze und  lange Form definiert sind:
1. Spezialfelder:
    * `end` : Ende der Eingabe \
        markiert das Ende der Eingabedatei (nutzbar z.B. wenn Standardeingabe gelesen wird)
    * `in` | `input-file` : Eingabedatei \
        angegebene Datei als zusätzliche Eingabedatei einlesen \
        `<wert>` ist ein relativer Dateiname (relativ zur Position der aktuellen Datei)
    * `tr` | `transpose` : transponieren \
        alle Noten um den angegebenen Wert nach oben (>0) oder unten (<0) transponieren \
        `<wert>` gibt an, um wie viele Halbtonschritte transponiert werden soll \
        wenn beim Transponieren eine Note außerhalb des gültigen Bereichs liegt, wird eine Warnung ausgegeben, und die Note um eine Oktave verschoben
2. Felder für das Seitenlayout *):
    * `pb` | `page-borders` : Seitenränder \
        Rand angeben (Vorgabewert is 5mm an jeder Seite) \
        `<wert>` ist die Breite des Randes in 1/10mm \
        ändert alle Ränder (oben, unten, links und rechts)
    * `pb(t|b|l|r)` | `page-border-(top|botton|left|right)` : Seitenrand \
        ändert nur den oberen / unteren / linken / rechten Rand
    * `nb(l|r)` | `note-border-(left|right)` : Notenrand \
        Notenrand angeben (Vorgabewert links abhängig von Saitenabstand, rechts standardmäßig 20mm) \
        `<wert>` ist der Rand in 1/10mm
3. Felder für das Zitherlayout *):
    * `sc` | `string-count` : Saitenanzahl \
        Saitenanzahl einstellen (Vorgabewert ist 25 für Bereich c1 bis c3) \
        `<wert>` ist die Anzahl der Melodieseiten (eine ganze Zahl)
    * `sd` | `string-distance` : Saitenabstand \
        Saitenabstand einstellen (Vorgabewert sind 8,8mm für eine Zither 3 1/2) \
        `<wert>` ist der Saitenabstand in 1/10mm
    * `cc` | `chord-count` : Akkordanzahl \
        Anzahl der Akkorde angeben (Vorgabe ist 6) \
        `<wert>` ist eine ganze Zahl
    * `cd` | `chord-distance` : Akkordabstand \
        Abstand der Akkorde zueinander \
        `<wert>` ist der Abstand in 1/10mm
    * `cw` | `chord-width` : Akkordbreite \
        Breite eines Akkords einstellen \
        `<wert>` ist die Akkordbreite in 1/10mm
4. Felder für Vorgabewerte:
    * `dl` | `default-length` : Vorgabelänge \
        Vorgabewert für Notenlänge ändern \
        `<wert>` ist die Vorgabelänge, 1,2,4,... (siehe oben)
    * `do` | `default-octave` : Vorgabeoktave \
        Vorgabeoktave ändern \
        `<wert>` ist die Vorgabeoktave, 1,2,3,...
5. Felder für Notenhöhen und umgestimmte Saiten
    * `r` | `retune` : umgestimmte Saiten \
        alte und neue Tonhöhe umgestimmter Saiten festlegen, siehe [Hinweise zur Nutzung](#nutzung-umgestimmter-saiten) \
        `<wert>` ist eine Liste mit `<alt>[:<neu>]` Paaren von Tonhögen \
        wenn die Tonhöhe `<neu>` nicht angegeben ist oder `none` ist, wird der Umstimm-Eintrag für die Tonhöhe `<ald>` entfernt \
        Eingabeformat für die Tonhöhen ist `[+|-]<tonname>[<oktave>]` wie [oben](#eingabeformat-für-noten) spezifiziert \
        für die neue Tonhöhe wird auch die Oktave `0` unterstützt
    * `hr` | `highlight-retuned` : Hervorhebung umgestimmter Saiten \
        festlegen ob und wie umgestimmte Saiten hervorgehoben werden \
        `<wert>` gibt die Art des Hervorhebens an:
        * `n` | `none` : umgestimmte Saiten nicht hervorheben
        * `c` | `color` : umgestimmte Noten einfärben
        * `l` | `line` : vertikale Linie für umgestimmte Saiten zeichnen
        * `cl` | `color-and-line` : Kombination aus eingefäbten Noten und vertikaler Linie
    * `pm` | `pitch-marks` : Markierung von Saiten \
        Tonhöhen festlegen, die markiert und beschriftet werden sollen \
        `<wert>` ist eine Liste mit Tonhöhen \
        Eingabeformat für die Tonhöhen ist `[+|-]<tonname>[<oktave>]` wie [oben](#eingabeformat-für-noten) spezifiziert
6. Felder für Liedinformationen:
    * `sn` | `short-name` : kurzer Name des Liedes
    * `t` | `title` : Titel des Liedes
    * `s` | `sub-title` : Untertitel
    * `a` | `artist` : Komponist (Musik und Text)
    * `c` | `composer` : Komponist (Musik)
    * `te` | `texter` : Texter (Text)
    * `arr` | `arrangement` : Satz (Arrangement)
7. Markierungen:
    * `m<x>` | `M<x>` | `marker-<x>` : Positionsmarkierung \
        setzt den Marker auf eine feste Position (oder per Referenz auf eine andere Positionsmarkierung)
        `<x>` ist die Markernummer \
        *die `M<x>` Schreibweise wird hier zusätzlich unterstützt, damit die gleiche Schreibweise wie für [Markierungen auf Melodieelemente](#nutzung-von-markierungen-an-melodieelementen) genutzt werden kann*
8. Felder für Strophentexte:
    * `l<x>` | `lyrics-<x>` : Strophentext \
        Strophentext für die x-te Strophe, bis zu 16 Strophen möglich \
        `<x>` ist die Strophen-Nummer \
        `<wert>` ist der Strophentext
    * `lp<x>` | `lyrics-position-<x>` : Strophenposition \
        Position der x-ten Strophe angeben (siehe [Positionieren von Strophentexten](#positionieren-von-strophentexten)) \
        `<x>` ist die Strophen-Nummer \
        `<wert>` ist die Position im Format `<col>,<row>` \
        je nach Format (siehe `lpf`) sind dies relative oder absolute Positionswerte
    * `lr` | `lyrics-refrain` : Refrain-Text \
        `<wert>` ist der Refrain-Text
    * `lpr` | `lyrics-position-refrain` : Position des Refrain-Texts \
        `<wert>` ist die Position des Refrains im Format `<col>,<row>`
    * `ln` | `print-lyric-numbers` : Strophennummerierung \
        Ein- / Ausschalten der Strophennummerierung \
        `<wert>` kann dabei `1`|`true` (Strophen nummerieren) oder `0`|`false` (nicht nummerieren) sein
    * `lpf` | `lyrics-positioning-format` : Format von Strophen-Positionen \
        `<wert>` ist das Format, folgende Formate sind verfügbar:
        * `cr` | `col-row` : Positionsangaben Angabe in Spalten/Zeilen, relativ zu Notenpositionen
        * `cra` | `col-row-absolute` : Angabe in Spalten/Zeilen, jedoch ohne Berücksichtigung der Komprimierung
        * `xy` : Angabe der Position mit absoluten X-Y-Koordinaten
        * `<x>f` | `<x>-fine` : mit erhöhter Genauigkeit, Skalierung in 1/10-Einheiten
    * `lv<x>` | `lyrics-visible-<x>` : Sichtbarkeit von Strophentexten \
        `<x>` ist die Strophen-Nummer \
        `<wert>` gibt an, ob die Strophe sichtbar ist (`1`|`true`) oder nicht (`0`|`false`) \
        als Voreinstellung werden alle angegebenen Strophentexte ausgegeben
    * `lv` | `lyrics-visible` : Sichtbarkeit von Strophentexten \
        `<wert>` ist eine Liste von Strophen-Nummern \
        je nach Operator `<op>` ändert sich hier das Verhalten:
        * `=` : genau die angegebenen Strophen werden ausgegeben
        * `+=` : die angegebenen Strophen werden ausgegeben, zusätzlich zu ggf. schon markierten
        * `-=` : die angegebenen Strophen werden nicht ausgegeben
    * `lrs` | `lyrics-rel-size` : relative Text-Größe der Strophentexte \
        * `<wert>` ist die relative Schriftgröße (<0: kleinere Schrift, >0: größere Schrift)
9. Felder für Zusatztexte:
    * `h` | `hint` : Hinweis
    * `w` | `warning` : Warnung \
        (kann zum Beispiel für den Satz 'Vor dem Spielen müssen die Akkorde umgestimmt werden!' genutzt werden) \
        Text wird rot dargestellt.
    * `atpf` | `additional-text-positioning-format` : Format von Positionsangaben bei Zusatztexten \
        `<wert>` ist das Format, siehe `lpf` | `lyrics-positioning-format`
    * `at<x>` | `additional-text-<x>` : Zusatztext inklusive Position \
        `<wert>` ist die Position und der Text im Format `<col>,<row>,<text>`
10. Felder für Layout-Parameter:
    * `sty` | `style` : Ausgabestil \
        `<wert>` ist der Stil, folgende Stile sind verfügbar:
        * `m` | `melody` : Melodie-Stil, Ausgabe von Notenblättern für die Melodie-Saiten
        * `b` | `bass` : Bass-Stil, Ausgabe von Notenblättern für die Bass-Saiten (in Vorbereitung)
    * `np` | `notes-position` : Notenposition \
        siehe [Positionieren von Noten](#positionieren-von-noten) \
        `<wert>` gibt die Position an:
        * `t` | `top` : Noten am oberen Rand anordnen
        * `c` | `center` : Noten vertikal zentrieren
        * `b` | `bottom` : Noten am unteren Rand anordnen
    * `(l|h|f)c` | `(lowest|highest|fix)-compression` : Komprimierung \
        minimale, maximale oder feste Komprimierung angeben (siehe [Positionieren von Noten](#positionieren-von-noten)) \
        `<wert>` ist eine ganze Zahl \
        `fc` setzt minimale und maximale Komprimierung auf den gleichen Wert
11. Felder für Schneidemarkierungen und Hilfsraster:
    * `g` | `print-grid` : Hilfraster \
        Hilfsraster anzeigen oder ausblenden \
        `<wert>` kann `1`|`true` (Hilfsraster anzeigen) oder `0`|`false` (kein Hilfsraster) sein \
        das Hilfsraster kann genutzt werden, um die besten Positionen für die Strophentexte zu finden (siehe [Positionieren von Strophentexten](#positionieren-von-strophentexten))
    * `gf` | `grid-format` : Format des Hilfsrasters \
        `<wert>` ist das Format des Hilfsrasters, siehe `lpf` | `lyrics-positioning-format` \
        im Spalten-/Zeilen-Modus werden Spalten und Zeilennummern angezeigt \
        im XY-Format werden absolute Koordinaten angezeigt
    * `lg` | `print-light-grid` : schwaches, reduziertes Raster \
        reduziertes Hilfsraster anzeigen oder ausblenden \
        `<wert>` kann `1`|`true` (Hilfsraster anzeigen) oder `0`|`false` (kein Hilfsraster) sein \
        das reduzierte Hilfsraster nutzt dünnere Linien ohne Koordinatenbeschriftungen
    * `cew` | `cut-edge-width` : Eckenbreite \
        Breite der abzuschneidenden Ecke ändern \
        `<wert>` gibt die Breite in 1/10mm an (gemessen vom rechten Bildrand, siehe grüne Umrandung des Hilfsrasters)
    * `ceh` | `cut-edge-height` : Eckenhöhe \
        Höhe der abzuschneidenden Ecke ändern \
        `<wert>` gibt die Höhe in 1/10mm an (gemessen vom oberen Bildrand, siehe grüne Umrandung des Hilfsrasters)
    * `ce` | `print-cut-edge` : Ecke ausgeben \
        abzuschneidende Ecke anzeigen oder ausblenden \
        `<wert>` kann `1`|`true` (anzeigen) oder `0`|`false` (nicht anzeigen) sein \
        die abzuschneidende Ecke wird mit dem Hilfsraster automatisch mit angezeigt
    * `csw` | `cut-side-width` : Randbreite \
        Breite des abzuschneidenden rechten Randes ändern \
        `<wert>` ist die Breite in 1/10mm (gemessen vom rechten Bildrand)
    * `cs` | `print-cut-side` : Rand ausgeben \
        abzuschneidenden Rand anzeigen oder ausblenden \
        `<wert>` kann `1`|`true` (anzeigen) oder `0`|`false` (nicht anzeigen) sein
    * `caw` | `clip-area-width` : Breite des Halteklammerbereichs \
        Breite des auszuschneidenden Rechtechs für den Halteklammerbereichs angeben \
        `<wert>` gibt die Breite in 1/10mm an (gemessen vom rechten Bildrand, siehe grüne Umrandung des Hilfsrasters)
    * `cah` | `clip-area-height` : Höhe des Halteklammerbereichs \
        Höhe des auszuschneidenden Rechtechs für den Halteklammerbereich angeben \
        `<wert>` gibt die Höhe in 1/10mm an
    * `cach` | `clip-area-center-height` : horizontale Position des Halteklammerbereichs \
        horizontale Position des auszuschneidenden Rechtechs für den Halteklammerbereich angeben \
        `<wert>` gibt die Höhe in 1/10mm an (gemessen vom unteren Bildrand, siehe grüne Umrandung des Hilfsrasters, bis zur Mitte des Halteklammerbereichs)
    * `ca` | `print-clip-area` : Halteklammerbereich ausgeben \
        auszuschneidendes Rechteck für den Halteklammerbereich anzeigen oder ausblenden \
        `<wert>` kann `1`|`true` (anzeigen) oder `0`|`false` (nicht anzeigen) sein \
        das auszuschneidende Rechteck wird mit dem Hilfsraster automatisch mit angezeigt

*) Hinweis: Die Einstellungen für das Seiten- und Zitherlayout können in spezifische Einstellungsdateien geschrieben werden (wie z.B. `page-layout-A4ls.conf` oder `zither-layout-3,5.conf`). Auf diese Weise kann einfach zwischen mehreren Seiten- und Zitherlayouts gewechselt werden, indem diese spezifischen Dateien auf der Kommandozeile angegeben werden, siehe [Aufruf](#aufruf).


## Positionieren von Noten
Wenn ein Layout für Unterlegnoten erstellt werden soll, muss beachtet werden, dass möglicherweise die rechte obere Ecke des Blattes abgeschnitten werden muss. \
Zu diesem Zweck wird mit dem Hilfsraster gleichzeitig auch eine Schnittlinie ausgegeben, die es ermöglicht, Noten zu finden, die abgeschnitten würden. Wenn solche Noten gefunden werden, muss ZitherLayout so kommandiert werden, dass diese Noten außerhalb des abzuschneidenden Bereichs positioniert werden. \
Das kann erreicht werden indem eine höhere Kompression (siehe `lc` und `fc` Kommandos) genutzt wird, und die Noten am unteren Blattrand ausgerichtet werden (siehe `np=b` Kommando). Auf diese Weise wird im oberen Bereich der nötige Freiraum erzeugt. Die beste Kompressionsrate erhält man einfach durch Ausprobieren verschiedener Werte.


## Positionieren von Strophentexten
Wenn Strophentexte auf dem Notenblatt ausgegeben werden sollen, müssen sowohl die Texte selbst als auch deren Positionen angegeben werden. \
Das gleiche gilt auf für die Zusatztexte.

Um mögliche Positionen zu ermitteln, kann das Notenblatt zunächst ohne Strophentexte, aber mit Hilfsraster erzeugt werden. Auf diese Weise kann zunächst der nötige Freiraum für die Texte ermittelt werden.

Danach können die ermittelten Positionen in die Eingabedatei geschrieben (siehe `lp<x>=<position>` Kommando) und das Hilfsraster wieder ausgeschaltet werden. \
Man benötigt also mindestens zwei Schritte, auch hier ist jedoch mehrmaliges Probieren die erste Wahl, um die besten Positionen zu ermitteln...

Mit den Feldern `lpf` (`lyrics-positioning-format`) und `atpf` kann das Positionierunsformat umgeschaltet werden, damit ist es möglich, relative Positionen anzugeben (relativ zu den Noten), oder auch absolute Positionen. Im `fine`-Modus können die Werte auf 1/10 Genauigkeit angegeben werden.

Um Strophentexte oder Zusatztexte relativ zu Melodieelementen (wie Noten, Pausen oder Separierungen) zu positionieren, können innerhalb der Melodie Positionsmrker gesetzt werden, welche dann beim Positionieren der Strophentexte oder Zusatztexte genutzt werden, siehe oben.


## Nutzung Umgestimmter Saiten
In manchen Liedern werden Töne benötigt, die nicht im normalen Tonumfang der Zither enthalten sind, ein oft vorkommendes Beispiel ist der Ton H oder B unter dem tiefen C (`c1`). Solche Töne können genutzt und gespielt werden, wenn nicht benötigte Saiten auf diese Töne umgestimt werden, oft wird zum Beispiel der Ton Dis (`+d1`) nicht benötigt, und kann je nach Bedarf in H (`h0`) oder B (`-h0`) umgestimmt werden.

Wenn ein Lied umgestimmte Saiten nutzt, sollte eine entsprechende Warnung auf den Unterlegnoten darauf aufmerksam machen. \
Zusätzlich kann es sinnvoll sein, umgestimmte Saiten auch formal zu definieren, damit zum Beispiel MIDI oder MTX-Ausgaben mit den korrekten Tonhögen erzeugt werden können. Für diese Definition wird das Feld `r`|`retune` genutzt. \
Wenn umgestimmte Saiten formal definiert sind, können zusätzlich zur oben genannten Warnung auch die umgestimmten Noten an sich auf dem Notenblatt hervorgehoben werden, dafür wird das Feld `hr`|`highlight-retuned` entsprechend gesetzt.

### Beispiele
Kommando     | Beschreibung
-------------|------------------------
`@r=+d1:-h0` | Umstimmen der Saite Dis1 in den Ton B unterhalb von C1
`@hr=c`      | alle als Dis1 notierten Noten, die aber als B klingen, farblich hervorheben
`@hr=l`      | die umgestimmte Saite Dis1 mit einer vertikalen Linie hervorheben
`@hr=cl`     | obige Noten- und Saitenhervorhebung kombinieren


## Ausgabeformate
PS ZitherLayout unterstützt verschiedene Ausgabeformate, welche mit dem Ausgabemodus (Parameter `-O`|`--output-mode`) gewählt werden können, siehe [Programmargumente](#programmargumente). Voreinstellung ist die Erstellung von Unterlegnoten für eine Zither (Modus `z`|`zither`).

Im Folgenden werden die einzelnen Ausgabeformate beschrieben, entsprechende Beispiele sind verlinkt.
Zusätzlich zu diesen Beispielen sei auf das [makefile](samples/sdgnk/makefile) verwiesen, welches die notwendigen Schritte zur Erstellung der Beispieldateien enthält.

### Unterlegnoten
Unterlegnoten werden genutzt, um sie unter die Saiten einer Zither zu legen, sodass die Melodie ganz einfach nachgespielt werden kann.

Hier ist als Beispiel ein recht bekanntes deutsches Weihnachtslied:
![](samples/sdgnk/sdgnk_znG.png "Süßer die Glocken nie klingen")

PS ZitherLayout erstellt (mit Ausgabemodus `z`|`zither`) als Ausgabe dabei eine [LaTeX-Quelldatei](samples/sdgnk/sdgnk_znG.tex), welche mit `pdflatex` in ein [PDF-Dokument](samples/sdgnk/sdgnk_znG.pdf) umgewandelt werden kann.

Über weitere Programmargumente (`-p`|`--part` und `-c`|`--combine`) kann das Ausgabeformat verändert werden, sodass mehrere Unterlegnotenblätter in einem PDF-Dokument zusammengefasst werden können.

### Liedtexte

#### normaler Modus
Im normalen Modus für Liedtexte (Ausgabemodus `l`|`lyrics`) wird als Ausgabe ein einfaches [LaTeX-Dokument](samples/sdgnk/sdgnk_l.tex) erzeugt, welches sich wiederum mit `pdflatex` in ein [PDF](samples/sdgnk/sdgnk_l.pdf) umwandeln lässt. Dieses Dokument enthält die Liedtexte in einem sehr einfachen Format.

#### LaTeX-Kommandos
An Stelle dieses einfachen Formats können die Liedtexte auch als [LaTeX-Kommandos](samples/sdgnk/sdgnk_lc.tex) ausgegeben werden (Ausgabemodus `lc`|`lyrics-cmd`). In dem erzeugten Dokument werden alle Liedinformationen wie Titel und auch die Texte als Parameter an spezielle LaTeX-Kommandos übergeben. Das ermöglicht es, gerade im Teilemodus (Parameter `-p`|`--part`), das spätere Aussehen eigenen Vorlieben anzupassen. Wird der Teilemodus nicht verwendet, werden einfache Standarddefinitionen für diese Kommandos mit generiert, sodass die erzeugte Ausgabe wiederum mit `pdflatex` in ein [PDF-Dokument](samples/sdgnk/sdgnk_lc.pdf) ungewandelt werden kann.

#### LaTeX-Kommandos (alternative Texte)
Der LaTeX-Modus für alternative Texte (Ausgabemodus `lca`|`lyrics-cmd-alt`) ist nahezu identisch zum 'normalen' LaTeX-Modus, nur werden [alternative LaTeX-Kommandos](samples/sdgnk/sdgnk_lca.tex) generiert. Hierdurch wird es zum Beispiel ermöglicht, in einer Textsammlung alternative Texte anders darzustellen.

#### generierte Kommandos im LaTeX-Modus
Das Ausgabedokument kann folgende LaTeX-Kommandos enthalten:
Kommando | alt       | Beschreibung / Inhalt
---------|-----------|-----------------------
`\ZLt`   | `\ZLat`   | Titel des Liedes
`\ZLs`   | `\ZLas`   | Untertitel
`\ZLi`   | `\ZLai`   | Informationen (Komponist, Satz, ..)
`\ZLl`   | `\ZLal`   | Liedtext-Umgebung
`\ZLlof` | `\ZLalof` | Liedtext-Umgebung bei nur einer Strophe
`\ZLlf`  | `\ZLalf`  | Text der ersten Strophe
`\ZLlr`  | `\ZLalr`  | Refrain
`\ZLlx`  | `\ZLalx`  | Text der 2.-x. Strophe

### Notenkodierung
PS ZitherLayout erlaubt es zudem, die Noten im eigenen Format auszugeben (Ausgabemodus `nc`|`note-codes`). Die Ausgabe enthält dann [Notenkodierungen](samples/sdgnk/sdgnk_nc.txt), die wiederum von PS ZitherLayout selbst als Eingabe genutzt werden können. Das ist zum Beispiel hilfreich, wenn Lieder transponiert werden (Feld `tr`|`transpose`), und das Ergebnis der Transponierung zwischengespeichert werden muss.

### Gesangsnoten
Im Ausgabemodus für Gesangsnoten (Ausgabemodus `mtx`) werden die Noten im [MTX-Format](samples/sdgnk/sdgnk_mtx.mtx) ausgegeben. Dieses Format kann mit `prepmx` und `pmxab` ins kompliziertere MusiXTeX-Format umgewandelt werden. Dieses wiederum kann, zum Beispiel unter Verwendung des LaTeX-Modus für die Liedtexte, in ein [LaTeX-Dokument](samples/sdgnk/sdgnk_gnmain.tex) eingebunden werden, um [Gesangsnoten](samples/sdgnk/sdgnk_gn.pdf) oder ganze Partituren oder Liederbücher zu erhalten.

### MIDI-Dateien
Im Ausgabemodus für MIDI-Dateien (Modus `midi`) erstellt PS ZitherLayout eine Datei im [MIDI-Format](samples/sdgnk/sdgnk_znG.mid). Diese kann mit einem externen Programm angehört werden, somit kann man beispielsweise prüfen, ob die Melodie korrekt notiert wurde.

Im Kombinier-Modus (`-c`|`--combine`) können mehrere Eingabedateien im MIDI-Format zu einer Datei zusammengefasst werden, um zum Beispiel bei mehrstimmigen Liedern eine einzelne MIDI-Datei mit allen Stimmen zu erhalten.
