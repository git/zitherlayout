# PS ZitherLayout - history


### 2023-02-01 release 2023.2.1
* added pitch mark and light grid support
  * added field `pm`|`pitch-marks`
  * added field `lg`|`print-light-grid`


### 2022-12-28 release 2022.12.28
* added clip area support
  * added fields `caw`|`clip-area-width`, `cah`|`clip-area-height` for size of clip area
  * added field `cach`|`clip-area-center-height` for horizontal position of clip area
  * added field `ca`|`print-clip-area` for printing clip area
* fixed settings file handling in combine mode
* fixed refrain lyrics handling in M-TX mode
* fixed info text positions (to be independent from compression)
* fixed page border handling to fix tex-caused right-shift of page content


### 2022-12-15 release 2022.12.15
* added tie group support
  * added `( .. )` support in melody lines
* added support for retuned strings
  * added field `r`|`retune` for retuned strings
  * added field `hr`|`highlight-retuned` for highlight of retuned strings and notes


### 2022-12-08 release 2022.12.8
* switched from epic to tikz/pgf when generating Zither notes
  * simplifies building pdf by just using `pdflatex` instead of `latex` and `dvipdfmx`
* added refrain support when exporting lyrics


### 2022-12-04 release 2022.12.4
* added marker support
  * added `M<x>` support in melody lines
  * added `m<x>` | `M<x>` | `marker-<x>` field
  * added support for lately resolved references to markers in position definition
* added `^<field>` support for referencing field components instead of complete field value
* fixed #warning handling


### 2022-03-23 release 2022.3.23
* added support for short command line help (option -h|--help)
* fixed command line flag for verbose mode
#### including several non-published updates
* changed versioning scheme (now data based)
* added `-O  --output-mode  <mode>` command line option
  * added support for output in m-tx format (output mode `mtx`)
  * added support for MIDI output (output mode `midi`)
  * removed `-l  --lyrics-mode  <mode>` option (replaced by output modes)
* added support for pause (rest) notation
* added note group support
* added support for C-like #warning and #error messages
* added support for splitting song into several parts using separators
* added support for transposing songs (`\tr` command)
* added support for creating note code output (e.g. after transposition)
* added support for refrain lyrics
* added support for additional texts
* added support for different positioning formats
* added support for field value references
* added operator support (`+=`, `-=`)
* added support for cut side marker
* added support for long form commands and song info fields
* added support for specifying relative (sub) input files in other input files
* added support for appending to current song info field values
* added IO flags (combine and part mode)
* added support for multiple input and settings files in one command line option
* extended composer and texter field length
* changed output message prefix to three characters
* fixed command line option handling
* fixed `style` shortcut to `sty` (`s` used by `sub-title`)


### 2012-01-03 release 1.0.1.1
* ZitherLayout version 1.0.1.0
  * extended song dump
* published to savannah.nongnu
