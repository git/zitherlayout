/*
  This file is part of PS ZitherLayout.

  PS ZitherLayout is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  PS ZitherLayout is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with PS ZitherLayout.  If not, see <http://www.gnu.org/licenses/>.


  Diese Datei ist Teil von PS ZitherLayout.

  PS ZitherLayout ist Freie Software: Sie können es unter den Bedingungen
  der GNU General Public License, wie von der Free Software Foundation,
  Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren
  veröffentlichten Version, weiterverbreiten und/oder modifizieren.

  PS ZitherLayout wird in der Hoffnung, dass es nützlich sein wird, aber
  OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
  Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
  Siehe die GNU General Public License für weitere Details.

  Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
  Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
*/

/*
**  © 2011-2023 Carsten Pfeffer (PiperSoft)
**
**  @project: PS ZitherLayout
**  @file: pszl_output.cpp
**  @brief implementation of output module
**
**  @date: 2011-11-13
**  @author: Carsten Pfeffer (nee Gaede)
**           Carsten Gaede
*/

#include "pszl_output.h"

#define RC_ERR_BASE RC_ERR_BASE_OUTPUT

#include <string.h>
#include <stdint.h>

#include "pszl_layout.h"
#include "pszl_version.h"


// dimensions, all in 1/10 mm
#define DIM_UNIT_LENGTH      1  // 0.1mm
#define DIM_TO_PIC_UNIT(x)  ((x) / DIM_UNIT_LENGTH)
#define DIM_TO_MILLI(x)        ((x) / 10)   // to mm
#define DIM_TO_MICRO(x)        ((x) * 100)  // to µm


//*** retune handling **********************************************************

bool IsPitchRetuned(SETTINGS *set, unsigned char pitch)
{
  for (unsigned int r = 0; r < MAX_RETUNED_NOTES; ++r) {
    RETUNED_NOTE *rn = &set->retuned.notes[r];
    if (rn->old_pitch == pitch) {
      if (rn->new_pitch != PITCH_NONE) return(pitch != rn->new_pitch);
    }
  }
  return(false);
}

int RetunePitch(SESSION *session, unsigned char *lpPitch)
{
  for (unsigned int r = 0; r < MAX_RETUNED_NOTES; ++r) {
    RETUNED_NOTE *rn = &session->set.retuned.notes[r];
    if (rn->old_pitch == *lpPitch) {
      if (rn->new_pitch != PITCH_NONE) *lpPitch = rn->new_pitch;
      return(RC_OK);
    }
  }
  return(RC_OK);
}


//*** notes and lyrics mode ****************************************************

int ExportNote(FILE *lpFile, unsigned char len)
{
  switch(len & LENGTH_MASK) {
    case LENGTH_1:
      fprintf(lpFile, "\\wh%s{1}", (len & LENGTH_PT) ? "p" : "");
    break;
    case LENGTH_2:
      fprintf(lpFile, "\\hu%s{1}", (len & LENGTH_PT) ? "p" : "");
    break;
    case LENGTH_4:
      fprintf(lpFile, "\\qu%s{1}", (len & LENGTH_PT) ? "p" : "");
    break;
    case LENGTH_8:
      fprintf(lpFile, "\\cu%s{1}", (len & LENGTH_PT) ? "p" : "");
    break;
    case LENGTH_16:
      fprintf(lpFile, "\\ccu%s{1}", (len & LENGTH_PT) ? "p" : "");
    break;
    case LENGTH_32:
      fprintf(lpFile, "\\cccu%s{1}", (len & LENGTH_PT) ? "p" : "");
    break;
    case LENGTH_64:
      fprintf(lpFile, "\\ccccu%s{1}", (len & LENGTH_PT) ? "p" : "");
    break;
    default: return(RC_ERR);
  }
  return(RC_OK);
}

int ExportPause(FILE *lpFile, unsigned char len)
{
  switch(len & LENGTH_MASK) {
    case LENGTH_1:
      fprintf(lpFile, "\\liftpause%s{0}", (len & LENGTH_PT) ? "p" : "");
    break;
    case LENGTH_2:
      fprintf(lpFile, "\\lifthpause%s{0}", (len & LENGTH_PT) ? "p" : "");
    break;
    case LENGTH_4:
      fprintf(lpFile, "\\qp%s{}", (len & LENGTH_PT) ? "p" : "");
    break;
    case LENGTH_8:
      fprintf(lpFile, "\\ds%s{}", (len & LENGTH_PT) ? "p" : "");
    break;
    case LENGTH_16:
      fprintf(lpFile, "\\qs%s{}", (len & LENGTH_PT) ? "p" : "");
    break;
    case LENGTH_32:
      fprintf(lpFile, "\\hs%s{}", (len & LENGTH_PT) ? "p" : "");
    break;
    case LENGTH_64:
      fprintf(lpFile, "\\qqs%s{}", (len & LENGTH_PT) ? "p" : "");
    break;
    default: return(RC_ERR);
  }
  return(RC_OK);
}

int ExportChords(FILE *lpFile, SETTINGS *set, unsigned char *chords, unsigned int chord_count, unsigned char chord_pos, int x, int y)
{
  if (chord_count == 0) return(RC_OK);

  char anchor[16];
  switch(chord_pos) {
    case RPOS_LEFT:
      x += set->dim.notes.chord.dx_l;
      snprintf(anchor, 16, "east");
    break;
    case RPOS_RIGHT:
      x += set->dim.notes.chord.dx_r;
      snprintf(anchor, 16, "west");
    break;
    case RPOS_BOTTOM:
      y += set->dim.notes.chord.dy_b;
      snprintf(anchor, 16, "north");
    break;
    default: return(RC_ERR);
  }
  fprintf(lpFile, "    \\node [anchor=%s] at (%i,%i) {", anchor, x, y);
  for (unsigned char iChord = 0; iChord < chord_count; ++iChord) {
    if (chords[iChord] & CHORD_SLR) {
      fprintf(lpFile, "\\itied{0}{1}");
    }
    else if (chords[iChord] & CHORD_PT) {
      fprintf(lpFile, "\\lpz{1}");
    }
    if (set->compression.current <= MAX_COMPRESSION_NORMAL_SIZE) {
      fprintf(lpFile, "\\large{%i}", chords[iChord] & CHORD_VALUE_MASK);
    }
    else if (set->compression.current <= MAX_COMPRESSION_SMALL_SIZE) {
      fprintf(lpFile, "%i", chords[iChord] & CHORD_VALUE_MASK);
    }
    else {
      fprintf(lpFile, "\\small{%i}", chords[iChord] & CHORD_VALUE_MASK);
    }
    if (chords[iChord] & CHORD_SLR) {
      if (set->compression.current <= MAX_COMPRESSION_SMALL_SIZE) {
        fprintf(lpFile, "\\hspace{1.5pt}");
      }
      else {
        fprintf(lpFile, "\\hspace{2pt}");
      }
      fprintf(lpFile, "\\ttie{0}");
    }
  }
  fprintf(lpFile, "};\r\n");

  return(RC_OK);
}

int ExportSong(FILE *lpFile, SETTINGS *set, SONG *song)
{
  if ((lpFile == NULL) || (set == NULL) || (song == NULL)) return(RC_ERR);
  int rc = RC_OK;

  // print retune lines
  if (set->retuned.highlight_mode & HLM_LINE) {
    int y_t = DIM_TO_PIC_UNIT(set->dim.picture.height - set->dim.picture.notes.borders.top);
    int y_b = DIM_TO_PIC_UNIT(set->dim.picture.notes.borders.bottom);
    fprintf(lpFile, "    %% retune lines\r\n");
    for (unsigned int r = 0; r < MAX_RETUNED_NOTES; ++r) {
      RETUNED_NOTE *rn = &set->retuned.notes[r];
      if (!rn->in_use) continue;
      int x = rn->pos.x;
      fprintf(lpFile, "    \\draw [red,dotted] (%i,%i) -- (%i,%i); %% retuned pitch %u\r\n", x, y_t, x, y_b, rn->old_pitch);
    }
  }
  // print separator lines
  fprintf(lpFile, "    %% separator lines\r\n");
  for (unsigned int mi_idx = 0; mi_idx < song->melody.item_count; ++mi_idx) {
    MELODY_ITEM *item = &song->melody.items[mi_idx];
    if (item->base.type != MI_TYPE_SEPARATOR) continue;
    SEPARATOR *sep = &item->sep;
    if (sep->style == SEP_STYLE_SPACE) continue;
    int x_le = DIM_TO_PIC_UNIT(set->dim.picture.notes.offset + set->dim.picture.notes.borders.left);
    int x_ri = DIM_TO_PIC_UNIT(set->dim.picture.width - set->dim.picture.notes.borders.right);
    int y = sep->pos.y;
    if (sep->style == SEP_STYLE_SOLID) {
      fprintf(lpFile, "    \\draw (%i,%i) -- (%i,%i); %% sep [%03u]\r\n", x_le, y, x_ri, y, item->base.idx);
    }
    else if (sep->style == SEP_STYLE_DOTTED) {
      fprintf(lpFile, "    \\draw [dotted] (%i,%i) -- (%i,%i); %% sep [%03u]\r\n", x_le, y, x_ri, y, item->base.idx);
    }
  }
  // print start needles
  fprintf(lpFile, "    %% start needles\r\n");
  unsigned char prev_pitch = PITCH_NONE;
  bool first_note = true;
  for (unsigned int mi_idx = 0; mi_idx < song->melody.item_count; ++mi_idx) {
    MELODY_ITEM *item = &song->melody.items[mi_idx];
    NOTE *lpNote = (NOTE*)NULL;
    switch (item->base.type) {
      case MI_TYPE_NOTE:
        lpNote = &item->note;
      break;
      case MI_TYPE_SEPARATOR:
        prev_pitch = PITCH_NONE;
        continue;
      break;
      case MI_TYPE_GROUP:
        if (item->grp.note_count == 0) return(RC_ERR);
        lpNote = &item->grp.notes[0];
      break;
      default: return(RC_ERR);
    }
    if (lpNote == NULL) {
      prev_pitch = PITCH_NONE;
      continue;
    }
    if (prev_pitch == PITCH_NONE) {
      int x = DIM_TO_PIC_UNIT(lpNote->pos.x);
      int y_top =  DIM_TO_PIC_UNIT(lpNote->start_needle_pos.y);
      int y_bot = DIM_TO_PIC_UNIT(lpNote->pos.y);
      int r = DIM_TO_PIC_UNIT(set->dim.notes.start_needle_size) / 2;
      fprintf(lpFile, "    \\draw [fill,red] (%i,%i) circle (%i);\r\n", x, y_top, r);
      fprintf(lpFile, "    \\draw [red] (%i,%i) -- (%i,%i);\r\n", x, y_top, x, y_bot);
      if (first_note && (strlen(song->warning) > 0)) {
        if (set->compression.current <= MAX_COMPRESSION_NORMAL_SIZE) {
          fprintf(lpFile, "    \\node [red,anchor=west,xshift=1mm] at (%i,%i) {\\large{*}};\r\n", x, y_top);
        }
        else if (set->compression.current <= MAX_COMPRESSION_SMALL_SIZE) {
          fprintf(lpFile, "    \\node [red,anchor=west,xshift=1mm] at (%i,%i) {*};\r\n", x, y_top);
        }
        else {
          fprintf(lpFile, "    \\node [red,anchor=west,xshift=1mm] at (%i,%i) {\\small{*}};\r\n", x, y_top);
        }
      }
    }
    prev_pitch = lpNote->pitch;
    first_note = false;
  }
  // print note connecting lines
  fprintf(lpFile, "    %% note connecting lines\r\n");
  prev_pitch = PITCH_NONE;
  int prev_x = 0;
  int prev_y = 0;
  for (unsigned short mi_idx = 0; mi_idx < song->melody.item_count; ++mi_idx) {
    MELODY_ITEM *item = &song->melody.items[mi_idx];
    NOTE *lpNote = (NOTE*)NULL;
    switch (item->base.type) {
      case MI_TYPE_NOTE:
        lpNote = &item->note;
      break;
      case MI_TYPE_SEPARATOR:
        prev_pitch = PITCH_NONE;
        continue;
      break;
      case MI_TYPE_GROUP:
        if (item->grp.note_count == 0) return(RC_ERR);
        lpNote = &item->grp.notes[0];
      break;
      default: return(RC_ERR);
    }
    if (lpNote == NULL) {
      prev_pitch = PITCH_NONE;
      continue;
    }
    int x = lpNote->pos.x;
    int y = lpNote->pos.y;
    if (prev_pitch != PITCH_NONE) {
      fprintf(lpFile, "    \\draw (%i,%i) -- (%i,%i); %% to note|group [%03u]\r\n", prev_x, prev_y, x, y, item->base.idx);
    }
    prev_x = x;
    prev_y = y;
    prev_pitch = lpNote->pitch;
  }
  // erase circle around notes
  fprintf(lpFile, "    %% erasing circles around notes\r\n");
  for (unsigned short mi_idx = 0; mi_idx < song->melody.item_count; ++mi_idx) {
    MELODY_ITEM *item = &song->melody.items[mi_idx];
    if (item->base.type == MI_TYPE_NOTE) {
      NOTE *lpNote = &item->note;
      int x = DIM_TO_PIC_UNIT(lpNote->pos.x);
      int y = DIM_TO_PIC_UNIT(lpNote->pos.y);
      int r = DIM_TO_PIC_UNIT(set->dim.notes.circle_size) / 2;
      fprintf(lpFile, "    \\draw [fill,white] (%i,%i) circle (%i); %% note [%03u]\r\n", x, y, r, item->base.idx);
    }
    else if (item->base.type == MI_TYPE_SEPARATOR) {}
    else if (item->base.type == MI_TYPE_GROUP) {
      NOTE_GROUP *grp = &item->grp;
      if (grp->note_count == 0) return(RC_ERR);
      int y = DIM_TO_PIC_UNIT(grp->notes[0].pos.y);
      int r = DIM_TO_PIC_UNIT(set->dim.notes.circle_size) / 2;
      for (unsigned int n_idx = 0; (RC_IS_OK(rc)) && (n_idx < grp->note_count); ++n_idx) {
        int x = DIM_TO_PIC_UNIT(grp->note_pos_x[n_idx]);
        fprintf(lpFile, "    \\draw [fill,white] (%i,%i) circle (%i); %% group [%03u] note [%u]\r\n", x, y, r, grp->base.idx, n_idx + 1);
      }
    }
    else return(RC_ERR);
  }
  // print notes
  fprintf(lpFile, "    %% notes\r\n");
  for (unsigned short mi_idx = 0; mi_idx < song->melody.item_count; ++mi_idx) {
    MELODY_ITEM *item = &song->melody.items[mi_idx];
    if (item->base.type == MI_TYPE_NOTE) {
      NOTE *lpNote = &item->note;
      bool is_retuned = IsPitchRetuned(set, lpNote->pitch);
      char color[16] = {0};
      int x = DIM_TO_PIC_UNIT(lpNote->pos.x + set->dim.notes.dx);
      int y = DIM_TO_PIC_UNIT(lpNote->pos.y + set->dim.notes.dy);
      if (lpNote->is_pause) {
        y += DIM_TO_PIC_UNIT(set->dim.notes.dy);
      }
      if (is_retuned && !lpNote->is_pause && (set->retuned.highlight_mode & HLM_COLOR)) snprintf(color, 16, ",red");
      fprintf(lpFile, "    \\node[anchor=south%s] at (%i,%i) {", color, x, y);
      if (lpNote->is_pause) {
        if (RC_IS_OK(rc)) rc = ExportPause(lpFile, lpNote->length);
      }
      else {
        if (RC_IS_OK(rc)) rc = ExportNote(lpFile, lpNote->length);
      }
      fprintf(lpFile, "}; %% note [%03u]\r\n", item->base.idx);
    }
    else if (item->base.type == MI_TYPE_SEPARATOR) {}
    else if (item->base.type == MI_TYPE_GROUP) {
      NOTE_GROUP *grp = &item->grp;
      if (grp->note_count == 0) return(RC_ERR);
      bool is_retuned = IsPitchRetuned(set, grp->notes[0].pitch);
      char color[16] = {0};
      int y = DIM_TO_PIC_UNIT(grp->notes[0].pos.y + set->dim.notes.dy);
      if (is_retuned && (set->retuned.highlight_mode & HLM_COLOR)) snprintf(color, 16, ",red");
      for (unsigned int n_idx = 0; (RC_IS_OK(rc)) && (n_idx < grp->note_count); ++n_idx) {
        int x = DIM_TO_PIC_UNIT(grp->note_pos_x[n_idx] + set->dim.notes.dx);
        NOTE *lpNote = &grp->notes[n_idx];
        fprintf(lpFile, "    \\node[anchor=south%s] at (%i,%i) {", color, x, y);
        if (lpNote->is_pause) {
          if (RC_IS_OK(rc)) rc = ExportPause(lpFile, lpNote->length);
        }
        else {
          if (RC_IS_OK(rc)) rc = ExportNote(lpFile, lpNote->length);
        }
        fprintf(lpFile, "}; %% group [%03u] note [%u]\r\n", grp->base.idx, lpNote->base.idx);
      }
      if ((grp->type == GRP_TYPE_TIE) && (grp->note_count > 1)) {
        // draw a tie bow, no simple solution found, so draw it
        int x_l = DIM_TO_PIC_UNIT(grp->note_pos_x[0]);
        int x_r = DIM_TO_PIC_UNIT(grp->note_pos_x[grp->note_count - 1]);
        int x_lm = DIM_TO_PIC_UNIT(grp->note_pos_x[0] + (set->dim.notes.dist_grouped / 2));
        int x_rm = DIM_TO_PIC_UNIT(grp->note_pos_x[grp->note_count - 1] - (set->dim.notes.dist_grouped / 2));
        int y_t = DIM_TO_PIC_UNIT(grp->notes[0].pos.y + set->dim.notes.dy);
        int y_bm = y_t - DIM_TO_PIC_UNIT(6);
        int y_b = y_t - DIM_TO_PIC_UNIT(8);
        int a_up = 45;  // upper curve angle at top point
        int a_dn = 55;  // lower curve angle at top point
        fprintf(lpFile, "    %% x_l=%i, x_lm=%i, x_rm=%i, x_r=%i\r\n", x_l, x_lm, x_rm, x_r);
        fprintf(lpFile, "    %% y_t=%i, y_bm=%i, y_b=%i\r\n", y_t, y_bm, y_b);
        fprintf(lpFile, "    %% a_up=%i°, a_dn=%i°\r\n", a_up, a_dn);
        fprintf(lpFile, "    \\draw [fill,very thin,line join=round]");
        fprintf(lpFile, " (%i,%i)", x_rm, y_bm);            // start at right end of upper ground line
        fprintf(lpFile, " to [out=0,in=%i]", 180 + a_up);   // draw upper right curve ..
        fprintf(lpFile, " (%i,%i)", x_r, y_t);              // .. to top right end point
        fprintf(lpFile, " to [out=%i,in=0]", 180 + a_dn);   // draw lower curve ..
        fprintf(lpFile, " (%i,%i)", x_rm, y_b);             // .. to right end of lower ground line
        fprintf(lpFile, " -- (%i,%i)", x_lm, y_b);          // draw lower ground line to left end
        fprintf(lpFile, " to [out=180,in=%i]", 360 - a_dn); // draw lower left curve ..
        fprintf(lpFile, " (%i,%i)", x_l, y_t);              // .. to top left end point
        fprintf(lpFile, " to [out=%i,in=180]", 360 - a_up);   // draw upper curve ..
        fprintf(lpFile, " (%i,%i)", x_lm, y_bm);            // .. to left end of upper ground lind
        fprintf(lpFile, " -- cycle");                       // draw line back to start point (right end of upper ground line)
        fprintf(lpFile, "; %% group [%03u] tie\r\n", grp->base.idx);
      }
    }
    else return(RC_ERR);
  }
  // print chords
  fprintf(lpFile, "    %% chords\r\n");
  for (unsigned short mi_idx = 0; mi_idx < song->melody.item_count; ++mi_idx) {
    MELODY_ITEM *item = &song->melody.items[mi_idx];
    if (item->base.type == MI_TYPE_NOTE) {
      NOTE *lpNote = &item->note;
      if (lpNote->chord_count == 0) continue;
      int x = DIM_TO_PIC_UNIT(lpNote->pos.x);
      int y = DIM_TO_PIC_UNIT(lpNote->pos.y);
      if (RC_IS_OK(rc)) rc = ExportChords(lpFile, set, lpNote->chords, lpNote->chord_count, lpNote->chord_pos, x, y);
    }
    else if (item->base.type == MI_TYPE_SEPARATOR) {}
    else if (item->base.type == MI_TYPE_GROUP) {
      NOTE_GROUP *grp = &item->grp;
      if (grp->note_count == 0) return(RC_ERR);
      int y = DIM_TO_PIC_UNIT(grp->notes[0].pos.y);
      if ((grp->type == GRP_TYPE_TIE) && (grp->note_count > 1)) {
        y -= DIM_TO_PIC_UNIT(5);  // set chords below group tie
      }
      for (unsigned int n_idx = 0; (RC_IS_OK(rc)) && (n_idx < grp->note_count); ++n_idx) {
        NOTE *lpNote = &grp->notes[n_idx];
        if (lpNote->chord_count == 0) continue;
        if (lpNote->chord_count > 1) return(RC_ERR);
        int x = DIM_TO_PIC_UNIT(grp->note_pos_x[n_idx]);
        if (RC_IS_OK(rc)) rc = ExportChords(lpFile, set, lpNote->chords, lpNote->chord_count, grp->notes[0].chord_pos, x, y);
      }
    }
    else return(RC_ERR);
  }

  // fill info area
  fprintf(lpFile, "    %% info area\r\n");
  // print my name... :)
  int y = DIM_TO_PIC_UNIT(set->dim.picture.height - set->dim.picture.info.borders.top);
  fprintf(lpFile, "    \\node [anchor=north east,rotate=90] at (0,%i) {\\small{%s: Carsten Pfeffer}};\r\n", y, TEXT_LAYOUT);
  // print zither type
  int x = DIM_TO_PIC_UNIT(set->dim.picture.info.width / 2);
  int w_mm = DIM_TO_MILLI(set->dim.picture.info.width);
  int h_mm = DIM_TO_MILLI(set->dim.picture.height - set->dim.picture.info.borders.top - set->dim.picture.info.borders.bottom);
  fprintf(lpFile, "    \\node [anchor=north,xshift=-2.5mm] at (%i,%i) {\\parbox{%imm}{\\centering{\\textbf{\\textit{{\\LARGE{%s $3 \\tfrac{1}{2}$}}\\\\(%i %s)}}}}};\r\n", x, y, w_mm, TEXT_ZITHER1, set->count.chords, TEXT_ZITHER2);
  unsigned char b = 0;  // kind of position marker of text at thick c1 line; 0: at line, 1: bit left of it, 2: more left of it
  char node_pos[64];
  char node_shift[64];
  char anchor[16];
  int r = set->count.rows / 2;
  // find best position for c1 line hint
  for (unsigned short mi_idx = 0; mi_idx < song->melody.item_count; ++mi_idx) {
    MELODY_ITEM *item = &song->melody.items[mi_idx];
    NOTE *lpNote = (NOTE*)NULL;
    switch (item->base.type) {
      case MI_TYPE_NOTE:
        lpNote = &item->note;
      break;
      case MI_TYPE_SEPARATOR:
        continue;
      break;
      case MI_TYPE_GROUP:
        if (item->grp.note_count == 0) return(RC_ERR);
        if (item->grp.notes[0].pitch == 60) if (b < 2) b = 2;
      break;
      default: return(RC_ERR);
    }
    if (lpNote == NULL) continue;
    if (((int)lpNote->pos.lrow.value > r) && (lpNote->pitch == 60)) {
      if (b < 1) b = 1;
      if (lpNote->chord_count != 0) {
        if (b < 2) b = 2;
        break;
      }
    }
  }
  switch(b) {
    case 0:
      snprintf(node_shift, 64, ",xshift=-1mm");
    break;
    case 1:
      snprintf(node_shift, 64, ",xshift=-2.5mm");
    break;
    case 2:
      snprintf(node_shift, 64, ",xshift=-4mm");
    break;
    default: return(RC_ERR);
  }
  x = DIM_TO_PIC_UNIT(set->dim.picture.notes.offset + set->dim.picture.notes.borders.left);
  y = DIM_TO_PIC_UNIT(set->dim.picture.info.borders.bottom);
  snprintf(node_pos, 64, "%i,%i", x, y);
  fprintf(lpFile, "    \\node [anchor=south west%s,rotate=90] at (%s) {\\large{\\textit{%s}}};\r\n", node_shift, node_pos, TEXT_C1_LINE);
  // print title and subtitle
  if (strlen(song->title) > 0) {
    if (strlen(song->subtitle) > 0) {
      snprintf(anchor, 16, "south");
    }
    else {
      snprintf(anchor, 16, "center");
    }
    x = DIM_TO_PIC_UNIT(set->dim.picture.info.width / 2);
    y = DIM_TO_PIC_UNIT(set->dim.picture.height / 2);
    fprintf(lpFile, "    \\node [anchor=%s,rotate=90] at (%i,%i) {\\Huge{\\textbf{%s}}};\r\n", anchor, x, y, song->title);
    if (strlen(song->subtitle) > 0) {
      snprintf(node_shift, 64, ",xshift=2mm");
      fprintf(lpFile, "    \\node [anchor=north%s,rotate=90] at (%i,%i) {\\Large{%s}};\r\n", node_shift, x, y, song->subtitle);
    }
  }
  // print artist information
  x = DIM_TO_PIC_UNIT(set->dim.picture.info.width);
  y = DIM_TO_PIC_UNIT(set->dim.picture.height - set->dim.picture.info.borders.top);
  snprintf(node_pos, 64, "%i,%i", x, y);
  node_shift[0] = '\0';
  bool first_line = true;
  if ((strlen(song->artists.composer) > 0) || (strlen(song->artists.texter) > 0) || (strlen(song->artists.arrangement) > 0)) {
    fprintf(lpFile, "    \\node [anchor=south east%s,align=right,rotate=90] at (%s) {\\parbox{%imm}{\\raggedleft{\\large{", node_shift, node_pos, h_mm);
    if ((strlen(song->artists.composer) > 0) || (strlen(song->artists.texter) > 0)) {
      if (strcmp(song->artists.composer, song->artists.texter) == 0) {
        fprintf(lpFile, "%s: \\textbf{%s}", TEXT_ARTIST, song->artists.composer);
        first_line = false;
      }
      else {
        if (strlen(song->artists.composer) > 0) {
          if (!first_line) fprintf(lpFile, "\\\\");
          fprintf(lpFile, "%s: \\textbf{%s}", TEXT_COMPOSER, song->artists.composer);
          first_line = false;
        }
        if (strlen(song->artists.texter) > 0) {
          if (!first_line) fprintf(lpFile, "\\\\");
          fprintf(lpFile, "%s: \\textbf{%s}", TEXT_TEXTER, song->artists.texter);
          first_line = false;
        }
      }
    }
    if (strlen(song->artists.arrangement) > 0) {
      if (!first_line) fprintf(lpFile, "\\\\");
      fprintf(lpFile, "%s: \\textbf{%s}", TEXT_ARRANGEMENT, song->artists.arrangement);
      first_line = false;
    }
    fprintf(lpFile, "}}}};\r\n");
  }
  // print warning and error
  y = DIM_TO_PIC_UNIT(set->dim.picture.info.borders.bottom);
  snprintf(node_pos, 64, "0,%i", y);
  node_shift[0] = '\0';
  first_line = true;
  if ((strlen(song->warning) > 0) || (strlen(song->hint) > 0)) {
    fprintf(lpFile, "    \\node [anchor=north west%s,rotate=90] at (%s) {\\parbox{%imm}{", node_shift, node_pos, h_mm);
    if (strlen(song->warning) > 0) {
        if (!first_line) fprintf(lpFile, "\\\\");
        fprintf(lpFile, "{\\color{red}*%s: %s\\color{black}}", TEXT_WARNING, song->warning);
        first_line = false;
    }
    if (strlen(song->hint) > 0) {
        if (!first_line) fprintf(lpFile, "\\\\");
        fprintf(lpFile, "%s: %s", TEXT_HINT, song->hint);
        first_line = false;
    }
    fprintf(lpFile, "}};\r\n");
  }

  // print lyrics
  fprintf(lpFile, "    %% lyrics\r\n");
  char lrs_start[16];
  char lrs_end[2];
  lrs_start[0] = '\0';
  lrs_end[0] = '\0';
  if (song->lyrics.rel_size != 0) {
    snprintf(lrs_start, 16, "\\relsize{%i}{", song->lyrics.rel_size);
    snprintf(lrs_end, 2, "}");
  }
  for (unsigned int ln = 0; ln < MAX_LYRICS; ++ln) {
    if (!song->lyrics.lyrics[ln].visible) continue;
    if (strlen(song->lyrics.lyrics[ln].text)) {
      x = DIM_TO_PIC_UNIT(song->lyrics.lyrics[ln].pos.x);
      y = DIM_TO_PIC_UNIT(song->lyrics.lyrics[ln].pos.y);
      w_mm = DIM_TO_MILLI(set->dim.picture.width - song->lyrics.lyrics[ln].pos.x);
      fprintf(lpFile, "    \\node [anchor=north west] (lyrics %i) at (%i,%i) {\\parbox{%imm}{%s%s%s}};\r\n", ln + 1, x, y, w_mm, lrs_start, song->lyrics.lyrics[ln].text, lrs_end);
      if (set->print_lyric_numbers) {
        fprintf(lpFile, "    \\node [anchor=north east,xshift=-1mm] at (lyrics %i.north west) {%s%i.%s};\r\n", ln + 1, lrs_start, ln + 1, lrs_end);
      }
    }
  }
  if (strlen(song->lyrics.refrain.text)) {
    x = DIM_TO_PIC_UNIT(song->lyrics.refrain.pos.x);
    y = DIM_TO_PIC_UNIT(song->lyrics.refrain.pos.y);
    w_mm = DIM_TO_MILLI(set->dim.picture.width - song->lyrics.refrain.pos.x);
    fprintf(lpFile, "    \\node [anchor=north west] (lyrics refrain) at (%i,%i) {\\parbox{%imm}{%s%s%s}};\r\n", x, y, w_mm, lrs_start, song->lyrics.refrain.text, lrs_end);
    if (set->print_lyric_numbers) {
      fprintf(lpFile, "    \\node [anchor=north east,xshift=-1mm] at (lyrics refrain.north west) {%sR:%s};\r\n", lrs_start, lrs_end);
    }
  }
  // print additions
  for (unsigned int atn = 0; atn < MAX_ADDITIONS; ++atn) {
    if (strlen(song->add.adds[atn].text)) {
      x = DIM_TO_PIC_UNIT(song->add.adds[atn].pos.x);
      y = DIM_TO_PIC_UNIT(song->add.adds[atn].pos.y);
      w_mm = DIM_TO_MILLI(set->dim.picture.width - song->add.adds[atn].pos.x);
      fprintf(lpFile, "    \\node [anchor=north west] at (%i,%i) {\\parbox{%imm}{%s}};\r\n", x, y, w_mm, song->add.adds[atn].text);
    }
  }

  return(RC_OK);
}

int InsertInputFiles(FILE *lpFile, SESSION *s, const char *sep)
{
  char lpLine[MAX_INPUT_LENGTH];
  FILE *lpInFile;
  bool first = true;
  for (unsigned int i = 0; i < s->input.uFileCount; ++i) {
    if (s->input.files[i].is_settings_file) continue;
    if (!first && (sep != NULL)) {
      fprintf(lpFile, sep);
    }
    first = false;
    if ((lpInFile = fopen(s->input.files[i].lpFileName, "r")) != NULL) {
      fprintf(lpFile, "  %% --- included from file '%s' ---\r\n", s->input.files[i].lpFileName);

      while (fgets(lpLine, MAX_INPUT_LENGTH, lpInFile) != NULL) {
        fprintf(lpFile, "%s", lpLine);
      }
      fprintf(lpFile, "  %% --- end of included file '%s' ---\r\n", s->input.files[i].lpFileName);
      fclose(lpInFile);
    }
    else {
      fprintf(stderr, "ERR: input file '%s' not found\r\n", s->input.files[i].lpFileName);
      return(RC_ERR);
    }
  }

  return(RC_OK);
}

#define PrintDim_mm(str, dim) \
  sprintf(str, "%i.%imm", DIM_TO_MILLI(dim), DIM_TO_MICRO(dim) % 1000)
int ExportFullSong(FILE *lpFile, SESSION *s)
{
  int rc = RC_OK;
  char pbt[16],pbb[16],pbl[16],pbr[16];
  int i;
  int x, y, x1, y1, x2, y2;
  int h, w;
  bool print_cut_edge, print_cut_side, print_clip_area;

  // preamble
  if (!(s->uIOFlags & IO_FLAG_PART)) {
    fprintf(lpFile, "\\documentclass[a4paper,10pt,landscape]{scrartcl}\r\n");
    fprintf(lpFile, "\\usepackage[paper=a4paper,landscape,tmargin=0pt,lmargin=0pt]{geometry}\r\n");
    fprintf(lpFile, "\\usepackage[utf8x]{inputenc}\r\n");
    fprintf(lpFile, "\\usepackage{tikz}\r\n");
    fprintf(lpFile, "\\usepackage{musixtex}\r\n");
    fprintf(lpFile, "\\usepackage{vmargin}\r\n");
    fprintf(lpFile, "\\usepackage{amsmath}\r\n");
    fprintf(lpFile, "\\usepackage{ngerman}\r\n");
    fprintf(lpFile, "\\usepackage{relsize}\r\n");
    fprintf(lpFile, "\r\n");
    fprintf(lpFile, "\\setpapersize[landscape]{A4}\r\n");
    PrintDim_mm(pbt, s->set.dim.page.borders.top + DIM_PBTB_OFFSET);
    PrintDim_mm(pbb, s->set.dim.page.borders.bottom - DIM_PBTB_OFFSET);
    PrintDim_mm(pbl, s->set.dim.page.borders.left + DIM_PBLR_OFFSET);
    PrintDim_mm(pbr, s->set.dim.page.borders.right - DIM_PBLR_OFFSET);
    fprintf(lpFile, "\\setmarginsrb{%s}{%s}{%s}{%s}{0pt}{0pt}{0pt}{0pt}\r\n", pbl, pbt, pbr, pbb);
    fprintf(lpFile, "\\begin{document}\r\n");
    fprintf(lpFile, "  \\pagestyle{empty}\r\n");
  }
  if (!(s->uIOFlags & IO_FLAG_COMBINE)) {
    if (s->set.compression.current <= MAX_COMPRESSION_NORMAL_SIZE) {
      fprintf(lpFile, "  \\normalnotesize\r\n");
    }
    else if (s->set.compression.current <= MAX_COMPRESSION_SMALL_SIZE) {
      fprintf(lpFile, "  \\smallnotesize\r\n");
    }
    else {
      fprintf(lpFile, "  \\tinynotesize\r\n");
    }
    PrintDim_mm(pbt, DIM_UNIT_LENGTH);
    fprintf(lpFile, "  \\begin{tikzpicture}[x=%s,y=%s,inner sep=0]\r\n", pbt, pbt);
    fprintf(lpFile, "    \\useasboundingbox[clip] (0,0) rectangle (%i,%i);\r\n", s->set.dim.picture.width, s->set.dim.picture.height);

    // print C line
    x = s->set.dim.picture.notes.offset + s->set.dim.picture.notes.borders.left;
    fprintf(lpFile, "    \\draw [very thick] (%i,%i) -- (%i,%i); %% C1 line\r\n", x, 0, x, s->set.dim.picture.height);

    if (RC_IS_OK(rc)) rc = ExportSong(lpFile, &s->set, &s->song);

    //*** print grid ***
    print_cut_edge = s->set.print_cut_edge && (s->set.dim.cut_edge.height > 0) && (s->set.dim.cut_edge.width > 0);
    print_cut_side = s->set.print_cut_side && (s->set.dim.cut_side.width > 0);
    print_clip_area = s->set.print_clip_area && (s->set.dim.clip_area.height > 0) && (s->set.dim.clip_area.width > 0) && (s->set.dim.clip_area.center_height > 0);
    if (s->set.print_grid || s->set.print_light_grid) {
      fprintf(lpFile, "    %% grid\r\n");
      unsigned char grid_fmt = s->set.grid_fmt;
      unsigned char axis_factor = 1;
      unsigned int dx, dy;
      if (!s->set.print_grid) grid_fmt = POSFMT_NONE;
      if (grid_fmt == POSFMT_NONE) {
        grid_fmt = s->song.lyrics.pos_fmt;
      }
      if (grid_fmt & POSMOD_FINE) {
        axis_factor = 10;
      }
      if (s->set.print_grid) {
        // print picture border
        // picture border
        h = DIM_TO_PIC_UNIT(s->set.dim.picture.height);
        w = DIM_TO_PIC_UNIT(s->set.dim.picture.width);
        fprintf(lpFile, "    \\draw [green] (0,0) -- (0,%i) -- (%i,%i) -- (%i,0) -- cycle; %% picture border\r\n", h, w, h, w);
        // line left of c1 (right of info area)
        x = DIM_TO_PIC_UNIT(s->set.dim.picture.notes.offset);
        fprintf(lpFile, "    \\draw [green] (%i,0) -- (%i,%i); %% info border\r\n", x, x, h);
        // print x axis
        y = h;
        switch (grid_fmt & POSFMT_MASK) {
          case POSFMT_NONE:
          case POSFMT_CR:
          case POSFMT_CR_ABS:
            for (unsigned int i = 0; i < s->set.count.cols; i += 5) {
              x = DIM_TO_PIC_UNIT(s->set.dim.picture.notes.offset + s->set.dim.picture.notes.borders.left + (i * s->set.dim.picture.col.width));
              fprintf(lpFile, "    \\node [anchor=north,blue] at (%i,%i) {%i}; %% x axis\r\n", x, y, i * axis_factor);
            }
          break;
          case POSFMT_XY:
            for (unsigned int i = 0; i < s->set.dim.picture.notes.width; i += 250) {
              x = DIM_TO_PIC_UNIT(s->set.dim.picture.notes.offset + s->set.dim.picture.notes.borders.left + i);
              fprintf(lpFile, "    \\node [anchor=north,blue] at (%i,%i) {%i}; %% x axis\r\n", x, y, ((i * axis_factor) / 10));
            }
          break;
          default: return(RC_ERR);
        }
        // print y axis
        x = DIM_TO_PIC_UNIT(s->set.dim.picture.notes.offset);
        switch (grid_fmt & POSFMT_MASK) {
          case POSFMT_NONE:
          case POSFMT_CR:
            for (unsigned int i = 0; i < s->set.count.rows; i += 5) {
              y = DIM_TO_PIC_UNIT(s->set.dim.picture.height - s->set.dim.picture.notes.borders.top - (i * s->set.dim.picture.row.height));
              fprintf(lpFile, "    \\node [anchor=west,blue] at (%i,%i) {%i}; %% y axis\r\n", s->set.dim.picture.notes.offset, y, i * axis_factor);
            }
          break;
          case POSFMT_CR_ABS:
            for (unsigned int i = 0; i < s->set.count.c0.rows; i += 5) {
              y = DIM_TO_PIC_UNIT(s->set.dim.picture.height - s->set.dim.picture.notes.borders.c0.top - (i * s->set.dim.picture.row.c0.height));
              fprintf(lpFile, "    \\node [anchor=west,blue] at (%i,%i) {%i}; %% y axis\r\n", s->set.dim.picture.notes.offset, y, i * axis_factor);
            }
          break;
          case POSFMT_XY:
            for (unsigned int i = 0; i < s->set.dim.picture.height; i += 250) {
              y = DIM_TO_PIC_UNIT(s->set.dim.picture.height - s->set.dim.picture.notes.borders.c0.top - i);
              fprintf(lpFile, "    \\node [anchor=west,blue] at (%i,%i) {%i}; %% y axis\r\n", s->set.dim.picture.notes.offset, y, ((i * axis_factor) / 10));
            }
          break;
          default: return(RC_ERR);
        }
      }
      // print grid itself
      char style[32];
      if (s->set.print_grid) snprintf(style, 32, "blue");
      else snprintf(style, 32, "lightgray, very thin, dashed");
      // top to bottom lines
      x1 = DIM_TO_PIC_UNIT(s->set.dim.picture.notes.offset + s->set.dim.picture.notes.borders.left);
      w = DIM_TO_PIC_UNIT(s->set.dim.picture.notes.width - s->set.dim.picture.notes.borders.left - s->set.dim.picture.notes.borders.right);
      x2 = x1 + w;
      if (s->set.print_grid) {
        y1 = DIM_TO_PIC_UNIT(s->set.dim.picture.height);
        h = DIM_TO_PIC_UNIT(s->set.dim.picture.height);
      }
      else {
        y1 = DIM_TO_PIC_UNIT(s->set.dim.picture.height - s->set.dim.picture.notes.borders.top);
        h = DIM_TO_PIC_UNIT(s->set.dim.picture.height - s->set.dim.picture.notes.borders.top - s->set.dim.picture.notes.borders.bottom);
      }
      y2 = y1 - h;
      switch (grid_fmt & POSFMT_MASK) {
        case POSFMT_NONE:
        case POSFMT_CR:
        case POSFMT_CR_ABS:
          dx = DIM_TO_PIC_UNIT(s->set.dim.picture.col.width);
        break;
        case POSFMT_XY:
          dx = DIM_TO_PIC_UNIT(50);  // 5mm distance between the lines
        break;
        default: return(RC_ERR);
      }
      fprintf(lpFile, "    \\foreach \\x in {%i,%i,...,%i} \\draw [%s] (\\x,%i) -- (\\x,%i); %% vertical grid lines\r\n", x1, x1 + dx, x2, style, y1, y2);
      // left to right lines
      if (s->set.print_grid) {
        x1 = DIM_TO_PIC_UNIT(s->set.dim.picture.notes.offset);
        w = DIM_TO_PIC_UNIT(s->set.dim.picture.notes.width);
      }
      else {
        x1 = DIM_TO_PIC_UNIT(s->set.dim.picture.notes.offset + s->set.dim.picture.notes.borders.left);
        w = DIM_TO_PIC_UNIT(s->set.dim.picture.notes.width - s->set.dim.picture.notes.borders.left - s->set.dim.picture.notes.borders.right);
      }
      x2 = x1 + w;
      y1 = DIM_TO_PIC_UNIT(s->set.dim.picture.height - s->set.dim.picture.notes.borders.top);
      h = DIM_TO_PIC_UNIT(s->set.dim.picture.height - s->set.dim.picture.notes.borders.top - s->set.dim.picture.notes.borders.bottom);
      y2 = y1 - h;
      switch (grid_fmt & POSFMT_MASK) {
        case POSFMT_NONE:
        case POSFMT_CR:
          dy = -DIM_TO_PIC_UNIT(s->set.dim.picture.row.height);
        break;
        case POSFMT_CR_ABS:
          dy = -DIM_TO_PIC_UNIT(s->set.dim.picture.row.c0.height);
        break;
        case POSFMT_XY:
          dy = -DIM_TO_PIC_UNIT(50); // 5mm distance between the lines
        break;
        default: return(RC_ERR);
      }
      fprintf(lpFile, "    \\foreach \\y in {%i,%i,...,%i} \\draw [%s] (%i,\\y) -- (%i,\\y); %% horizontal grid lines\r\n", y1, y1 + dy, y2, style, x1, x2);
    }
    // print note border
    if (s->set.print_grid || s->set.print_light_grid) {
      x1 = DIM_TO_PIC_UNIT(s->set.dim.picture.notes.offset + s->set.dim.picture.notes.borders.left);
      x2 = DIM_TO_PIC_UNIT(s->set.dim.picture.width - s->set.dim.picture.notes.borders.right);
      y1 = DIM_TO_PIC_UNIT(s->set.dim.picture.notes.borders.bottom);
      y2 = DIM_TO_PIC_UNIT(s->set.dim.picture.height - s->set.dim.picture.notes.borders.top);
      char style[32];
      if (s->set.print_grid) snprintf(style, 32, "thick");
      else snprintf(style, 32, "lightgray, dashed");
      fprintf(lpFile, "    \\draw [%s] (%i,%i) -- (%i,%i) -- (%i,%i) -- (%i,%i) -- cycle; %% note border\r\n", style, x1, y1, x1, y2, x2, y2, x2, y1);
    }
    if (s->set.print_grid || print_cut_edge || print_cut_side || print_clip_area) {
      fprintf(lpFile, "    %% cut markers\r\n");
      // print cut edge
      if (s->set.print_grid || print_cut_edge) {
        x1 = DIM_TO_PIC_UNIT(s->set.dim.picture.width - s->set.dim.cut_edge.width);
        y1 = DIM_TO_PIC_UNIT(s->set.dim.picture.height);
        x2 = DIM_TO_PIC_UNIT(s->set.dim.picture.width);
        y2 = DIM_TO_PIC_UNIT(s->set.dim.picture.height - s->set.dim.cut_edge.height);
        fprintf(lpFile, "    \\draw [thick,red] (%i,%i) -- (%i,%i); %% cut edge\r\n", x1, y1, x2, y2);
      }
      // print cut side
      if (s->set.print_grid || print_cut_side) {
        x = DIM_TO_PIC_UNIT(s->set.dim.picture.width - s->set.dim.cut_side.width);
        y1 = DIM_TO_PIC_UNIT(s->set.dim.picture.height);
        y2 = DIM_TO_PIC_UNIT(0);
        fprintf(lpFile, "    \\draw [thick,red] (%i,%i) -- (%i,%i); %% cut side\r\n", x, y1, x, y2);
      }
      // print clip area
      if (s->set.print_grid || print_clip_area) {
        x1 = DIM_TO_PIC_UNIT(s->set.dim.picture.width - s->set.dim.clip_area.width);
        y1 = DIM_TO_PIC_UNIT(s->set.dim.clip_area.center_height + (s->set.dim.clip_area.height / 2));
        x2 = DIM_TO_PIC_UNIT(s->set.dim.picture.width + 10);  // clipped by bounding box to make sure, rectangle is opened to the right
        y2 = DIM_TO_PIC_UNIT(s->set.dim.clip_area.center_height - (s->set.dim.clip_area.height / 2));
        if (y2 < 0) y2 = 0;
        fprintf(lpFile, "    \\draw [thick,red] (%i,%i) rectangle (%i,%i); %% clip area\r\n", x1, y1, x2, y2);
      }
    }
    // print pitch mark lines and labels
    {
      int y_t = DIM_TO_PIC_UNIT(s->set.dim.picture.height);
      int y_b = 0;
      int y_tl = DIM_TO_PIC_UNIT(s->set.dim.picture.height - s->set.dim.picture.notes.borders.top);
      int y_bl = DIM_TO_PIC_UNIT(s->set.dim.picture.notes.borders.bottom);
      int r = 35;
      fprintf(lpFile, "    %% pitch marks\r\n");
      for (unsigned int m = 0; m < MAX_MARKED_PITCHES; ++m) {
        PITCH_MARK *pm = &s->set.pitch_marks.marks[m];
        if (pm->pitch == PITCH_NONE) continue;
        int x = pm->pos.x;
        char pitch_name[32] = {0};
        // value
        unsigned char oct = 1;
        unsigned char val = pm->pitch - 60;
        while (val >= 12) {
          ++oct;
          val -= 12;
        }
        switch (val) {
          case 0: snprintf(pitch_name, 32, "C%u", oct); break;
          case 1: snprintf(pitch_name, 32, "Cis%u", oct); break;
          case 2: snprintf(pitch_name, 32, "D%u", oct); break;
          case 3: snprintf(pitch_name, 32, "Dis%u", oct); break;
          case 4: snprintf(pitch_name, 32, "E%u", oct); break;
          case 5: snprintf(pitch_name, 32, "F%u", oct); break;
          case 6: snprintf(pitch_name, 32, "Fis%u", oct); break;
          case 7: snprintf(pitch_name, 32, "G%u", oct); break;
          case 8: snprintf(pitch_name, 32, "Gis%u", oct); break;
          case 9: snprintf(pitch_name, 32, "A%u", oct); break;
          case 10: snprintf(pitch_name, 32, "B%u", oct); break;
          case 11: snprintf(pitch_name, 32, "H%u", oct); break;
          default: return(RC_ERR);
        }
        fprintf(lpFile, "    %% pitch mark %u\r\n", pm->pitch);
        fprintf(lpFile, "    \\draw [dashed] (%i,%i) -- (%i,%i);\r\n", x, y_tl, x, y_bl);
        fprintf(lpFile, "    \\draw [fill,white] (%i,%i) circle (%i);\r\n", x, y_tl, r);
        fprintf(lpFile, "    \\node at (%i,%i) {%s};\r\n", x, y_tl, pitch_name);
        fprintf(lpFile, "    \\draw [fill,white] (%i,%i) circle (%i);\r\n", x, y_bl, r);
        fprintf(lpFile, "    \\node at (%i,%i) {%s};\r\n", x, y_bl, pitch_name);
      }
    }

    fprintf(lpFile, "  \\end{tikzpicture}\r\n");
  }
  else { // combine mode
    if ((i = InsertInputFiles(lpFile, s, "  \r\n") != 0)) {
      return(i);
    }
  }
  if (!(s->uIOFlags & IO_FLAG_PART)) {
    fprintf(lpFile, "\\end{document}\r\n");
  }

  return(rc);
}

//*** lyrics only mode *********************************************************

int ExportLyrics(FILE *lpFile, SESSION *s)
{
  int rc = RC_OK;
  SETTINGS *set = &s->set;
  SONG *song = &s->song;

  if (!(s->uIOFlags & IO_FLAG_PART)) {
    // print preamble
    fprintf(lpFile, "\\documentclass[a4paper,DIV=12]{scrartcl}\r\n");
    fprintf(lpFile, "\\usepackage[utf8x]{inputenc}\r\n");
    fprintf(lpFile, "\r\n");
    fprintf(lpFile, "\\begin{document}\r\n");
//    if (s->uIOFlags & IO_FLAG_COMBINE) {
//      fprintf(lpfile, "  \\tableofcontents\r\n");
//    }
    if ((s->output.mode == OUTPUT_MODE_LYRICS_CMD) || (s->output.mode == OUTPUT_MODE_LYRICS_CMD_ALT)) {
      const char *alt = (s->output.mode == OUTPUT_MODE_LYRICS_CMD_ALT) ? "a" : "";
      fprintf(lpFile, "  \\newcommand{\\ZL%st}[2]{\\center{\\Huge{\\textbf{#2}}}\\\\}\r\n", alt);
      fprintf(lpFile, "  \\newcommand{\\ZL%ss}[2]{}\r\n", alt);
      fprintf(lpFile, "  \\newcommand{\\ZL%si}[4]{\\raggedleft{\\large{#2}}}\r\n", alt);
      fprintf(lpFile, "  \\newenvironment{ZL%sl}[1]{\\begin{enumerate}}{\\end{enumerate}}\r\n", alt);
      fprintf(lpFile, "  \\newenvironment{ZL%slof}[1]{\\begin{enumerate}}{\\end{enumerate}}\r\n", alt);
      fprintf(lpFile, "  \\newcommand{\\ZL%slr}[2]{\\item[R:] #2}\r\n", alt);
      fprintf(lpFile, "  \\newcommand{\\ZL%slf}[3]{\\item[#2.] #3}\r\n", alt);
      fprintf(lpFile, "  \\newcommand{\\ZL%slx}[3]{\\item[#2.] #3}\r\n", alt);
      fprintf(lpFile, "\r\n");
    }
  }
  if (!(s->uIOFlags & IO_FLAG_COMBINE)) {
    // print title and other texts
    if (s->output.mode == OUTPUT_MODE_LYRICS_NORMAL) {
      fprintf(lpFile, "  \\center{\\Huge{\\textbf{%s}}}\\\\\r\n", song->title);
      if ((strlen(song->artists.composer) > 0) || (strlen(song->artists.texter) > 0)) {
        fprintf(lpFile, "  \\raggedleft{\\large{");
        if (strcmp(song->artists.composer, song->artists.texter) == 0) {
          fprintf(lpFile, "%s: \\textbf{%s}", TEXT_ARTIST, song->artists.composer);
        }
        else {
          if (strlen(song->artists.composer) > 0) {
            fprintf(lpFile, "%s: \\textbf{%s}", TEXT_COMPOSER, song->artists.composer);
          }
          if ((strlen(song->artists.composer) > 0) && (strlen(song->artists.texter) > 0)) {
            fprintf(lpFile, "\\\\ ");
          }
          if (strlen(song->artists.texter) > 0) {
            fprintf(lpFile, "%s: \\textbf{%s}", TEXT_TEXTER, song->artists.texter);
          }
        }
        fprintf(lpFile, "}}\\\\\r\n");
      }
      fprintf(lpFile, "\r\n");
      // print lyrics
      if (song->lyrics.max_num > 0) {
        fprintf(lpFile, "  \\begin{enumerate}\r\n");
        for (unsigned int i = 0; i < MAX_LYRICS; ++i) {
          if (strlen(song->lyrics.lyrics[i].text)) {
            fprintf(lpFile, "    \\item[");
            if (set->print_lyric_numbers) {
              fprintf(lpFile, "%i.", i + 1);
            }
            fprintf(lpFile, "] %s\r\n", song->lyrics.lyrics[i].text);
          }
        }
        fprintf(lpFile, "  \\end{enumerate}\r\n");
      }
    }
    else if ((s->output.mode == OUTPUT_MODE_LYRICS_CMD) || (s->output.mode == OUTPUT_MODE_LYRICS_CMD_ALT)) {
      const char *alt = (s->output.mode == OUTPUT_MODE_LYRICS_CMD_ALT) ? "a" : "";
      fprintf(lpFile, "  \\ZL%st{%s}{%s}\r\n", alt, song->short_name, song->title);
      if ((strlen(song->artists.composer) > 0) || (strlen(song->artists.texter) > 0)) {
        fprintf(lpFile, "  \\ZL%si{%s}{", alt, song->short_name);
        if (strcmp(song->artists.composer, song->artists.texter) == 0) {
          fprintf(lpFile, "%s: \\textbf{%s}", TEXT_ARTIST, song->artists.composer);
        }
        else {
          if (strlen(song->artists.composer) > 0) {
            fprintf(lpFile, "%s: \\textbf{%s}", TEXT_COMPOSER, song->artists.composer);
          }
          if ((strlen(song->artists.composer) > 0) && (strlen(song->artists.texter) > 0)) {
            fprintf(lpFile, "\\\\ ");
          }
          if (strlen(song->artists.texter) > 0) {
            fprintf(lpFile, "%s: \\textbf{%s}", TEXT_TEXTER, song->artists.texter);
          }
        }
        fprintf(lpFile, "}");
        fprintf(lpFile, "{%s}{%s}\r\n", song->artists.composer, song->artists.texter);
      }
      fprintf(lpFile, "\r\n");
      // print lyrics
      if (song->lyrics.max_num > 0) {
        if (song->lyrics.max_num > 1) {
          fprintf(lpFile, "  \\begin{ZL%sl}{%s}\r\n", alt, song->short_name);
        }
        else {
          fprintf(lpFile, "  \\begin{ZL%slof}{%s}\r\n", alt, song->short_name);
        }
        for (unsigned int i = 0; i < 1; ++i) {
          if (strlen(song->lyrics.lyrics[i].text)) {
            fprintf(lpFile, "    \\ZL%sl%s{%s}{%i}{%s}\r\n", alt, i == 0 ? "f" : "x", song->short_name, i + 1, song->lyrics.lyrics[i].text);
          }
        }
        if (strlen(song->lyrics.refrain.text)) {
          fprintf(lpFile, "    \\ZL%slr{%s}{%s}\r\n", alt, song->short_name, song->lyrics.refrain.text);
        }
        for (unsigned int i = 1; i < MAX_LYRICS; ++i) {
          if (strlen(song->lyrics.lyrics[i].text)) {
            fprintf(lpFile, "    \\ZL%sl%s{%s}{%i}{%s}\r\n", alt, i == 0 ? "f" : "x", song->short_name, i + 1, song->lyrics.lyrics[i].text);
          }
        }
        if (song->lyrics.max_num > 1) {
          fprintf(lpFile, "  \\end{ZL%sl}\r\n", alt);
        }
        else {
          fprintf(lpFile, "  \\end{ZL%slof}\r\n", alt);
        }
      }
    }
  }
  else { // combine mode
    rc =  InsertInputFiles(lpFile, s, s->output.mode == OUTPUT_MODE_LYRICS_NORMAL ? "  \\newpage\r\n" : "");
  }
  if (!(s->uIOFlags & IO_FLAG_PART)) {
    fprintf(lpFile, "\\end{document}\r\n");
  }

  return(rc);
}

//*** note codes mode **********************************************************

int ExportNoteCode(FILE *lpFile, NOTE *lpNote)
{
  if ((lpFile == NULL) || (lpNote == NULL)) return(RC_ERR);
  if (lpNote->is_pause) {
    fprintf(lpFile, "P");
  }
  // length
  switch (lpNote->length & LENGTH_MASK) {
    case LENGTH_1: fprintf(lpFile, "1"); break;
    case LENGTH_2: fprintf(lpFile, "2"); break;
    case LENGTH_4: fprintf(lpFile, "4"); break;
    case LENGTH_8: fprintf(lpFile, "8"); break;
    case LENGTH_16: fprintf(lpFile, "16"); break;
    case LENGTH_32: fprintf(lpFile, "32"); break;
    case LENGTH_64: fprintf(lpFile, "64"); break;
    // case LENGTH_128: fprintf(lpFile, "128"); break;
    // case LENGTH_256: fprintf(lpFile, "256"); break;
    default: return(RC_ERR);
  }
  if (lpNote->length & LENGTH_PT) fprintf(lpFile, ".");
  // value
  unsigned char oct = 1;
  unsigned char val = lpNote->pitch - 60;
  while (val >= 12) {
    ++oct;
    val -= 12;
  }
  switch (val) {
    case 0: fprintf(lpFile, "c"); break;
    case 1: fprintf(lpFile, "+c"); break;
    case 2: fprintf(lpFile, "d"); break;
    case 3: fprintf(lpFile, "+d"); break;
    case 4: fprintf(lpFile, "e"); break;
    case 5: fprintf(lpFile, "f"); break;
    case 6: fprintf(lpFile, "+f"); break;
    case 7: fprintf(lpFile, "g"); break;
    case 8: fprintf(lpFile, "+g"); break;
    case 9: fprintf(lpFile, "a"); break;
    case 10: fprintf(lpFile, "-h"); break;
    case 11: fprintf(lpFile, "h"); break;
    default: return(RC_ERR);
  }
  fprintf(lpFile, "%u", oct);
  // chords
  for (unsigned int cidx = 0; cidx < lpNote->chord_count; ++cidx) {
    char chord = lpNote->chords[cidx];
    fprintf(lpFile, "A");
    fprintf(lpFile, "%u", chord & CHORD_VALUE_MASK);
    if (chord & CHORD_PT) fprintf(lpFile, ".");
    if (chord & CHORD_SLR) fprintf(lpFile, "_");
  }
  fprintf(lpFile, " ");
  return(RC_OK);
}

int ExportSeparatorCode(FILE *lpFile, SEPARATOR *lpSep)
{
  if ((lpFile == NULL) || (lpSep == NULL)) return(RC_ERR);
  fprintf(lpFile, "\r\nS");
  if (lpSep->size != 0) fprintf(lpFile, "%u", lpSep->size);
  switch (lpSep->style) {
    case SEP_STYLE_SPACE: break;
    case SEP_STYLE_DOTTED: fprintf(lpFile, "d%u", lpSep->line_pos); break;
    case SEP_STYLE_SOLID: fprintf(lpFile, "l%u", lpSep->line_pos); break;
    default: return(RC_ERR);
  }
  fprintf(lpFile, "\r\n");
  return(RC_OK);
}

int ExportNoteGroupCode(FILE *lpFile, NOTE_GROUP *lpGrp)
{
  if ((lpFile == NULL) || (lpGrp == NULL)) return(RC_ERR);
  int rc = RC_OK;

  if (lpGrp->type == GRP_TYPE_TIE) fprintf(lpFile, "( ");
  else fprintf(lpFile, "{ ");
  for (unsigned int i = 0; (RC_IS_OK(rc)) && (i < lpGrp->note_count); ++i) {
    if (RC_IS_OK(rc)) rc = ExportNoteCode(lpFile, &lpGrp->notes[i]);
  }
  if (lpGrp->type == GRP_TYPE_TIE) fprintf(lpFile, ") ");
  else fprintf(lpFile, "} ");

  return(rc);
}

int ExportNoteCodes(FILE *lpFile, SESSION *s)
{
  if ((lpFile == NULL) || (s == NULL)) return(RC_ERR);
  int rc = RC_OK;

  // print note codes
  for (unsigned int mi_idx = 0; (RC_IS_OK(rc)) && (mi_idx < s->song.melody.item_count); ++mi_idx) {
    MELODY_ITEM *item = &s->song.melody.items[mi_idx];
    switch (item->base.type) {
      case MI_TYPE_NOTE:
        rc = ExportNoteCode(lpFile, &item->note);
      break;
      case MI_TYPE_SEPARATOR:
        rc = ExportSeparatorCode(lpFile, &item->sep);
      break;
      case MI_TYPE_GROUP:
        rc = ExportNoteGroupCode(lpFile, &item->grp);
      break;
      default: return(RC_ERR);
    }
  }
  fprintf(lpFile, "\r\n");

  return(rc);
}

//*** PMX mode *****************************************************************


int ExportMTXNote(SESSION *session, FILE *lpFile, NOTE *lpNote)
{
  int rc = RC_OK;
  unsigned char pitch = lpNote->pitch;

  if (RC_IS_OK(rc)) rc = RetunePitch(session, &pitch);
  if (RC_IS_ERR(rc)) return(rc);

  if ((lpFile == NULL) || (lpNote == NULL)) return(RC_ERR);
  if (lpNote->is_pause) {
    fprintf(lpFile, "r");
  }
  else {
    switch (pitch % 12) {
      case 0: fprintf(lpFile, "c"); break;
      case 1: fprintf(lpFile, "c"); break;
      case 2: fprintf(lpFile, "d"); break;
      case 3: fprintf(lpFile, "d"); break;
      case 4: fprintf(lpFile, "e"); break;
      case 5: fprintf(lpFile, "f"); break;
      case 6: fprintf(lpFile, "f"); break;
      case 7: fprintf(lpFile, "g"); break;
      case 8: fprintf(lpFile, "g"); break;
      case 9: fprintf(lpFile, "a"); break;
      case 10: fprintf(lpFile, "b"); break;
      case 11: fprintf(lpFile, "b"); break;
      default: return(RC_ERR);
    }
  }
  // length
  switch (lpNote->length & LENGTH_MASK) {
    case LENGTH_1: fprintf(lpFile, "0"); break;
    case LENGTH_2: fprintf(lpFile, "2"); break;
    case LENGTH_4: fprintf(lpFile, "4"); break;
    case LENGTH_8: fprintf(lpFile, "8"); break;
    case LENGTH_16: fprintf(lpFile, "1"); break;
    case LENGTH_32: fprintf(lpFile, "3"); break;
    case LENGTH_64: fprintf(lpFile, "6"); break;
    // case LENGTH_128: fprintf(lpFile, ""); break;
    // case LENGTH_256: fprintf(lpFile, ""); break;
    default: return(RC_ERR);
  }
  if (lpNote->length & LENGTH_PT) fprintf(lpFile, "d");
  if (!lpNote->is_pause) {
    // MTX octave 0 starts from pitch 12
    if (pitch < 12) fprintf(lpFile, "0");
    else fprintf(lpFile, "%u", (pitch / 12) - 1);
  }
  if (!lpNote->is_pause) {
    switch (pitch % 12) {
      case 1: fprintf(lpFile, "s"); break;
      case 3: fprintf(lpFile, "s"); break;
      case 6: fprintf(lpFile, "s"); break;
      case 8: fprintf(lpFile, "s"); break;
      case 10: fprintf(lpFile, "f"); break;
    }
  }
  fprintf(lpFile, " ");
  return(RC_OK);
}

int ExportMTX(FILE *lpFile, SESSION *s)
{
  int rc = RC_OK;

  fprintf(lpFile, "Style: Singer\n");
  fprintf(lpFile, "%% Meter: \n");
  fprintf(lpFile, "%% Sharps: \n");
  fprintf(lpFile, "%% Flats: \n");
  fprintf(lpFile, "%% Systems: \n");
  // the next two lines do the same, but one works for older m-tx/pmx tools only, the other one for newer ones
  fprintf(lpFile, "PMX: w160m\n");      // set line width for older m-tx/pmx tools
  fprintf(lpFile, "Width: 160mm\n");    // set line width for newer m-tx/pmx tools
  fprintf(lpFile, "\n");
  if (strlen(s->song.title) > 0) fprintf(lpFile, "Title: %s\n", s->song.title);
  if (strlen(s->song.artists.composer) > 0) fprintf(lpFile, "Composer: %s\n", s->song.artists.composer);
  if (strlen(s->song.artists.texter) > 0) {
    if (strcmp(s->song.artists.composer, s->song.artists.texter) != 0) {
      fprintf(lpFile, "Poet: %s\n", s->song.artists.texter);
    }
  }
  fprintf(lpFile, "\n");

  for (unsigned int mi_idx = 0; (RC_IS_OK(rc)) && (mi_idx < s->song.melody.item_count); ++mi_idx) {
    MELODY_ITEM *item = &s->song.melody.items[mi_idx];
    switch (item->base.type) {
      case MI_TYPE_NOTE:
        rc = ExportMTXNote(s, lpFile, &item->note);
      break;
      case MI_TYPE_SEPARATOR:
        continue;
      break;
      case MI_TYPE_GROUP:
        if (item->grp.type == GRP_TYPE_TIE) fprintf(lpFile, "( ");
        for (unsigned int i = 0; (RC_IS_OK(rc)) && (i < item->grp.note_count); ++i) {
          rc = ExportMTXNote(s, lpFile, &item->grp.notes[i]);
        }
        if (item->grp.type == GRP_TYPE_TIE) fprintf(lpFile, ") ");
      break;
      default: return(RC_ERR);
    }
  }
  fprintf(lpFile, "\n");
  if (strlen(s->song.lyrics.lyrics[0].text) > 0) {
    fprintf(lpFile, "L: 1. %s", s->song.lyrics.lyrics[0].text);
    if (strlen(s->song.lyrics.refrain.text) > 0) {
      fprintf(lpFile, " R:\\quad{}%s", s->song.lyrics.refrain.text);
    }
    fprintf(lpFile, "\n");
  }
  return(RC_OK);
}

//*** export in MIDI format ****************************************************

#define MIDI_LEN_QUARTER  240
#define MIDI_LEN_WHOLE (4 * MIDI_LEN_QUARTER)

struct MIDI_CHUNK_HEADER {
  char id[4];
  uint32_t len;
};
#define MIDI_CHUNK_HEADER_LEN 8

struct MIDI_HEADER_DATA {
  uint16_t fmt;
  uint16_t trk_cnt;
  uint16_t div;
};
#define MIDI_HEADER_DATA_LEN 6

inline int WriteBufU32(unsigned char *data, unsigned int doffs, uint32_t u32)
{
  if (data == NULL) return(RC_ERR);
  data[doffs] = (u32 >> 24) & 0xFF;
  data[doffs + 1] = (u32 >> 16) & 0xFF;
  data[doffs + 2] = (u32 >> 8) & 0xFF;
  data[doffs + 3] = u32 & 0xFF;
  return(RC_OK);
}

inline int ReadBufU32(unsigned char *data, unsigned int doffs, uint32_t *u32)
{
  if ((data == NULL) || (u32 == NULL)) return(RC_ERR);
  *u32 = 0;
  *u32 |= data[doffs] << 24;
  *u32 |= data[doffs + 1] << 16;
  *u32 |= data[doffs + 2] << 8;
  *u32 |= data[doffs + 3];
  return(RC_OK);
}

inline int WriteBufU16(unsigned char *data, unsigned int doffs, uint16_t u16)
{
  if (data == NULL) return(RC_ERR);
  data[doffs] = (u16 >> 8) & 0xFF;
  data[doffs + 1] = u16 & 0xFF;
  return(RC_OK);
}

inline int ReadBufU16(unsigned char *data, unsigned int doffs, uint16_t *u16)
{
  if ((data == NULL) || (u16 == NULL)) return(RC_ERR);
  *u16 = 0;
  *u16 |= data[doffs] << 8;
  *u16 |= data[doffs + 1];
  return(RC_OK);
}

int WriteMIDIChunkHeader(unsigned char *data, unsigned int doffs, MIDI_CHUNK_HEADER *mchead)
{
  if ((data == NULL) || (mchead == NULL)) return(RC_ERR);
  int rc = RC_OK;
  memcpy(data + doffs, mchead->id, 4);
  if (RC_IS_OK(rc)) rc = WriteBufU32(data, doffs + 4, mchead->len);
  return(rc);
}

int ReadMIDIChunkHeader(unsigned char *data, unsigned int doffs, MIDI_CHUNK_HEADER *mchead)
{
  if ((data == NULL) || (mchead == NULL)) return(RC_ERR);
  int rc = RC_OK;
  memset(mchead, 0, sizeof(MIDI_CHUNK_HEADER));
  memcpy(mchead->id, data + doffs, 4);
  if (RC_IS_OK(rc)) rc = ReadBufU32(data, doffs + 4, &mchead->len);
  return(rc);
}

int WriteMIDIHeaderData(unsigned char *data, unsigned int doffs, MIDI_HEADER_DATA *mhdata)
{
  if ((data == NULL) || (mhdata == NULL)) return(RC_ERR);
  int rc = RC_OK;
  if (RC_IS_OK(rc)) rc = WriteBufU16(data, doffs, mhdata->fmt);
  if (RC_IS_OK(rc)) rc = WriteBufU16(data, doffs + 2, mhdata->trk_cnt);
  if (RC_IS_OK(rc)) rc = WriteBufU16(data, doffs + 4, mhdata->div);
  return(rc);
}

int ReadMIDIHeaderData(unsigned char *data, unsigned int doffs, MIDI_HEADER_DATA *mhdata)
{
  if ((data == NULL) || (mhdata == NULL)) return(RC_ERR);
  int rc = RC_OK;
  memset(mhdata, 0, sizeof(MIDI_HEADER_DATA));
  if (RC_IS_OK(rc)) rc = ReadBufU16(data, doffs, &mhdata->fmt);
  if (RC_IS_OK(rc)) rc = ReadBufU16(data, doffs + 2, &mhdata->trk_cnt);
  if (RC_IS_OK(rc)) rc = ReadBufU16(data, doffs + 4, &mhdata->div);
  return(rc);
}

int WriteMIDIDeltaTime(unsigned char *data, unsigned int *doffs, unsigned int delta_time)
{
  if ((data == NULL) || (doffs == NULL)) return(RC_ERR);
  int rc = RC_OK;

  // 1aaa aaaa 1bbb bbbb 1ccc cccc 0ddd dddd
  // -> max 28 bit
  //  <= 0x7F -> 1 byte
  if (delta_time <= 0x7F) {
    data[*doffs] = delta_time & 0x7F;
    ++*doffs;
  }
  //  <= 0x3FFF -> 2 byte
  else if (delta_time <= 0x3FFF) {
    data[*doffs] = 0x80 | ((delta_time >> 7) & 0x7F);
    ++*doffs;
    data[*doffs] = delta_time & 0x7F;
    ++*doffs;
  }
  //  <= 0x1FFFFF -> 3 byte
  //  <= 0xFFFFFFF  -> 4 byte
  else rc = RC_ERR;

  return(rc);
}

int WriteMIDITrackName(unsigned char *data, unsigned int *doffs, const char *track_name)
{
  if ((data == NULL) || (doffs == NULL) || (track_name == NULL)) return(RC_ERR);
  int rc = RC_OK;

  if (RC_IS_OK(rc)) rc = WriteMIDIDeltaTime(data, doffs, 0);
  data[*doffs] = 0xFF;
  ++*doffs;
  data[*doffs] = 0x03;
  ++*doffs;
  data[*doffs] = strlen(track_name) & 0xFF;
  ++*doffs;
  snprintf((char*)(data + *doffs), 255, "%s", track_name);
  *doffs += strlen(track_name) & 0xFF;

  return(rc);
}

inline int WriteMIDINoteCommand(SESSION *session, unsigned char *data, unsigned int *doffs, NOTE *note, bool note_on)
{
  int rc = RC_OK;
  unsigned char pitch = note->pitch;

  if (RC_IS_OK(rc)) rc = RetunePitch(session, &pitch);
  if (RC_IS_ERR(rc)) return(rc);

  if ((data == NULL) || (doffs == NULL) || (note == NULL)) return(RC_ERR);
  data[*doffs] = note_on ? 0x90 : 0x80;   // note on / note off
  ++*doffs;
  data[*doffs] = pitch;
  ++*doffs;
  data[*doffs] = 100;
  ++*doffs;
  return(RC_OK);
}

int GetMIDINoteLen(NOTE *note, unsigned int *len)
{
  *len = 0;
  switch (note->length & LENGTH_MASK) {
    case LENGTH_1:
      *len = MIDI_LEN_WHOLE;
    break;
    case LENGTH_2:
      *len = MIDI_LEN_WHOLE / 2;
    break;
    case LENGTH_4:
      *len = MIDI_LEN_WHOLE / 4;
    break;
    case LENGTH_8:
      *len = MIDI_LEN_WHOLE / 8;
    break;
    case LENGTH_16:
      *len = MIDI_LEN_WHOLE / 16;
    break;
    case LENGTH_32:
      *len = MIDI_LEN_WHOLE / 32;
    break;
    case LENGTH_64:
      *len = MIDI_LEN_WHOLE / 64;
    break;
    // case LENGTH_128:
    //   *len = MIDI_LEN_WHOLE / 128;
    // break;
    // case LENGTH_256:
    //   *len = MIDI_LEN_WHOLE / 256;
    // break;
    default: return(RC_ERR);
  }
  if (note->length & LENGTH_PT) *len += *len / 2;
  return(RC_OK);
}

int WriteMIDINote(SESSION *session, unsigned char *data, unsigned int *doffs, NOTE *note, unsigned int notelen, unsigned int *delta_time)
{
  if ((data == NULL) || (doffs == NULL) || (note == NULL) || (delta_time == NULL)) return(RC_ERR);
  int rc = RC_OK;
  unsigned int len = notelen;

  if (RC_IS_OK(rc) && (len == 0)) rc = GetMIDINoteLen(note, &len);

  if (note->is_pause) {
    *delta_time += len;
  }
  else {
    if (RC_IS_OK(rc)) rc = WriteMIDIDeltaTime(data, doffs, *delta_time);
    *delta_time = 0;
    if (RC_IS_OK(rc)) rc = WriteMIDINoteCommand(session, data, doffs, note, true);
    if (RC_IS_OK(rc)) rc = WriteMIDIDeltaTime(data, doffs, len);
    if (RC_IS_OK(rc)) rc = WriteMIDINoteCommand(session, data, doffs, note, false);
  }

  return(rc);
}

int WriteMIDIEndOfTrack(unsigned char *data, unsigned int *doffs, unsigned int delta_time)
{
  if ((data == NULL) || (doffs == NULL)) return(RC_ERR);
  int rc = RC_OK;
  if (RC_IS_OK(rc)) rc = WriteMIDIDeltaTime(data, doffs, delta_time);
  data[*doffs] = 0xFF;
  ++*doffs;
  data[*doffs] = 0x2F;
  ++*doffs;
  data[*doffs] = 0x00;
  ++*doffs;
  return(rc);
}

int WriteMIDITrack(SESSION *session, SONG *s, const char *track_name)
{
  if ((s == NULL) || (track_name == NULL)) return(RC_ERR);
  int rc = RC_OK;
  unsigned int delta_time = 0;
  memset(s->export_data, 0, MAXLEN_EXPORT_DATA_PER_SONG);
  s->export_data_len = 0;
  unsigned char *data = s->export_data + MIDI_CHUNK_HEADER_LEN;
  unsigned int doffs = 0;
  // encode track name
  if (RC_IS_OK(rc)) rc = WriteMIDITrackName(data, &doffs, track_name);
  // encode melody
  for (unsigned int mi_idx = 0; RC_IS_OK(rc) && (mi_idx < s->melody.item_count); ++mi_idx) {
    MELODY_ITEM *item = &s->melody.items[mi_idx];
    switch (item->base.type) {
      case MI_TYPE_NOTE:
        rc = WriteMIDINote(session, data, &doffs, &item->note, 0, &delta_time);
      break;
      case MI_TYPE_SEPARATOR:
        // nothing to do for separators
      break;
      case MI_TYPE_GROUP:
        if (item->grp.type == GRP_TYPE_NONE) {
          for (unsigned int i = 0; RC_IS_OK(rc) && (i < item->grp.note_count); ++i) {
            rc = WriteMIDINote(session, data, &doffs, &item->grp.notes[i], 0, &delta_time);
          }
        }
        else if (item->grp.type == GRP_TYPE_TIE) {
          printf("DBG: exporting tie group to MIDI\r\n");
          if (item->grp.note_count > 0) {
            unsigned int group_notelen = 0;
            for (unsigned int i = 0; RC_IS_OK(rc) && (i < item->grp.note_count); ++i) {
              unsigned int notelen = 0;
              if (RC_IS_OK(rc)) rc = GetMIDINoteLen(&item->grp.notes[i], &notelen);
              if (RC_IS_OK(rc)) group_notelen += notelen;
            }
            if (RC_IS_OK(rc)) rc = WriteMIDINote(session, data, &doffs, &item->grp.notes[0], group_notelen, &delta_time);
          }
        }
        else rc = RC_ERR;
      break;
    }
  }
  // end of track
  if (RC_IS_OK(rc)) rc = WriteMIDIEndOfTrack(data, &doffs, delta_time);
  // encode track header
  MIDI_CHUNK_HEADER mchead;
  memset(&mchead, 0, sizeof(MIDI_CHUNK_HEADER));
  memcpy(mchead.id, "MTrk", 4);
  mchead.len = doffs;
  if (RC_IS_OK(rc)) rc = WriteMIDIChunkHeader(s->export_data, 0, &mchead);
  s->export_data_len = MIDI_CHUNK_HEADER_LEN + doffs;

  return(rc);
}

int ExportMIDIHeader(FILE *lpFile, unsigned int track_count)
{
  if (lpFile == NULL) return(RC_ERR);
  int rc = RC_OK;
  MIDI_CHUNK_HEADER mchead;
  MIDI_HEADER_DATA mhdata;
  unsigned char buf[32];
  memset(&mchead, 0, sizeof(MIDI_CHUNK_HEADER));
  memcpy(mchead.id, "MThd", 4);
  mchead.len = MIDI_HEADER_DATA_LEN;
  memset(&mhdata, 0, sizeof(MIDI_HEADER_DATA));
  mhdata.fmt = 1;
  mhdata.trk_cnt = track_count;
  mhdata.div = MIDI_LEN_QUARTER;
  if (RC_IS_OK(rc)) rc = WriteMIDIChunkHeader(buf, 0, &mchead);
  if (RC_IS_OK(rc)) rc = WriteMIDIHeaderData(buf, MIDI_CHUNK_HEADER_LEN, &mhdata);
  fwrite(buf, 1, MIDI_CHUNK_HEADER_LEN + MIDI_HEADER_DATA_LEN, lpFile);
  return(rc);
}

int ExportMIDI(FILE *lpFile, SESSION *s)
{
  if (lpFile == NULL) return(RC_ERR);
  int rc = RC_OK;
  if (!(s->uIOFlags & IO_FLAG_COMBINE)) {
    if (RC_IS_OK(rc)) rc = ExportMIDIHeader(lpFile, 1);
    char track_name[128] = { 0 };
    if (s->input.first_input_file_idx >= 0) {
      char *fname = s->input.files[s->input.first_input_file_idx].lpFileName;
      char *pos = strrchr(fname, '/');
      if (pos == NULL) pos = strrchr(fname, '\\');
      if (pos != NULL) ++pos;
      else pos = fname;
      snprintf(track_name, 128, pos);
      pos = strrchr(track_name, '.');
      if (pos != NULL) *pos = '\0';
    }
    else if (strlen(s->song.short_name) != 0) {
      snprintf(track_name, 128, "%s", s->song.short_name);
    }
    else {
      sprintf(track_name, "PSZL sample track");
    }
    if (RC_IS_OK(rc)) rc = WriteMIDITrack(s, &s->song, track_name);
    if (RC_IS_OK(rc)) fwrite(s->song.export_data, 1, s->song.export_data_len, lpFile);
  }
  else {  // combine mode
    unsigned int track_count = 0;
    FILE *lpInputFiles[MAX_INPUT_FILES];
    unsigned char data[MAXLEN_EXPORT_DATA_PER_SONG];
    unsigned int dlen;
    memset(lpInputFiles, 0, sizeof(FILE*) * MAX_INPUT_FILES);
    for (unsigned int i = 0; RC_IS_OK(rc) && (i < s->input.uFileCount); ++i) {
      if (s->input.files[i].is_settings_file) continue;
      lpInputFiles[i] = fopen(s->input.files[i].lpFileName, "rb");
      if (lpInputFiles[i] != NULL) {
        dlen = fread(data, 1, MIDI_CHUNK_HEADER_LEN + MIDI_HEADER_DATA_LEN, lpInputFiles[i]);
        if (dlen == MIDI_CHUNK_HEADER_LEN + MIDI_HEADER_DATA_LEN) {
          MIDI_CHUNK_HEADER mchead;
          MIDI_HEADER_DATA mhdata;
          memset(&mchead, 0, sizeof(MIDI_CHUNK_HEADER));
          memset(&mhdata, 0, sizeof(MIDI_HEADER_DATA));
          if (RC_IS_OK(rc)) rc = ReadMIDIChunkHeader(data, 0, &mchead);
          if (RC_IS_OK(rc)) {
            if (memcmp(mchead.id, "MThd", 4) != 0) rc = RC_ERR;
            else if (mchead.len != MIDI_HEADER_DATA_LEN) rc = RC_ERR;
          }
          if (RC_IS_OK(rc)) rc = ReadMIDIHeaderData(data, MIDI_CHUNK_HEADER_LEN, &mhdata);
          if (RC_IS_OK(rc)) {
            if ((mhdata.fmt != 0) && (mhdata.fmt != 1)) rc = RC_ERR;
          }
          if (RC_IS_OK(rc)) {
            track_count += mhdata.trk_cnt;
            if (s->uLogMode >= LOG_MODE_VERBOSE) {
              fprintf(stdout, "DBG: found %u tracks in input file '%s'\r\n", mhdata.trk_cnt, s->input.files[i].lpFileName);
            }
          }
        }
        else {
          rc = RC_ERR;
        }
        if (RC_IS_ERR(rc)) {
          fprintf(stderr, "ERR: input file '%s' does not seem to be a valid MIDI file\r\n", s->input.files[i].lpFileName);
          fclose(lpInputFiles[i]);
        }
      }
      else {
        fprintf(stderr, "ERR: input file '%s' not found\r\n", s->input.files[i].lpFileName);
        rc = RC_ERR;
      }
    }
    if (RC_IS_OK(rc)) rc = ExportMIDIHeader(lpFile, track_count);
    // fprintf(stdout, "DBG: combining MIDI from %u input files\r\n", s->input.uFileCount);
    for (unsigned int i = 0; RC_IS_OK(rc) && (i < s->input.uFileCount); ++i) {
      if (lpInputFiles[i] == NULL) continue;
      // fprintf(stdout, "DBG: opened input file '%s'\r\n", s->input.lpFileNames[i]);
      while ((dlen = fread(data, 1, MAXLEN_EXPORT_DATA_PER_SONG, lpInputFiles[i])) > 0) {
        // fprintf(stdout, "DBG: read %u byte from input file '%s'\r\n", dlen, s->input.lpFileNames[i]);
        fwrite(data, 1, dlen, lpFile);
      }
    }
    for (unsigned int i = 0; i < s->input.uFileCount; ++i) {
      if (lpInputFiles[i] != NULL) fclose(lpInputFiles[i]);
    }
  }

  return(rc);
}


//*** user function ************************************************************

int Export(FILE *lpFile, SESSION *s)
{
  switch (s->output.mode) {
    case OUTPUT_MODE_ZITHER:
      fprintf(lpFile, "%% generated by PS ZitherLayout version %s\r\n", VERSION);
      return(ExportFullSong(lpFile, s));
    break;
    case OUTPUT_MODE_LYRICS_NORMAL:
    case OUTPUT_MODE_LYRICS_CMD:
    case OUTPUT_MODE_LYRICS_CMD_ALT:
      fprintf(lpFile, "%% generated by PS ZitherLayout version %s\r\n", VERSION);
      return(ExportLyrics(lpFile, s));
    break;
    case OUTPUT_MODE_NOTE_CODES:
      fprintf(lpFile, "; generated by PS ZitherLayout version %s\r\n", VERSION);
      return(ExportNoteCodes(lpFile, s));
    break;
    case OUTPUT_MODE_MTX:
      fprintf(lpFile, "%% generated by PS ZitherLayout version %s\n", VERSION);
      return(ExportMTX(lpFile, s));
    break;
    case OUTPUT_MODE_MIDI:
      return(ExportMIDI(lpFile, s));
    break;
  }
  printf("ERR: output format not yet implemented\r\n");
  return(RC_ERR);
}