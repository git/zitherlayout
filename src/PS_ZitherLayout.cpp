/*
  This file is part of PS ZitherLayout.

  PS ZitherLayout is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  PS ZitherLayout is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with PS ZitherLayout.  If not, see <http://www.gnu.org/licenses/>.


  Diese Datei ist Teil von PS ZitherLayout.

  PS ZitherLayout ist Freie Software: Sie können es unter den Bedingungen
  der GNU General Public License, wie von der Free Software Foundation,
  Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren
  veröffentlichten Version, weiterverbreiten und/oder modifizieren.

  PS ZitherLayout wird in der Hoffnung, dass es nützlich sein wird, aber
  OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
  Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
  Siehe die GNU General Public License für weitere Details.

  Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
  Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
*/

/*
**  © 2011-2023 Carsten Pfeffer (PiperSoft)
**
**  @project: PS ZitherLayout
**  @file: PS_ZitherLayout.cpp
**  @brief main module of PS ZitherLayout
**
**  @date: 2011-11-03
**  @author: Carsten Pfeffer (nee Gaede)
**           Carsten Gaede
*/

#include "pszl_version.h"
#include "pszl_definitions.h"

#define RC_ERR_BASE RC_ERR_BASE_PSZL

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include "pszl_fields.h"
#include "pszl_input.h"
#include "pszl_layout.h"
#include "pszl_output.h"

#define DEFAULT_SETTINGS_FILENAME ".PiperSoft/ZitherLayout/default-settings"  // located in home directory

//*** dump routines ************************************************************

int DumpNote(NOTE *lpNote)
{
  int iChord;
  printf("DBG: item[%03i]: %s row=%2i, len=%2i%1s, val=%2i, dir=%i, line_pos=%i, chord_count=%2i, chord_pos=%i\r\n", lpNote->base.idx, lpNote->is_pause ? "pause" : "note", lpNote->pos.lrow.value, lpNote->length & LENGTH_MASK, (lpNote->length & LENGTH_PT) ? "p" : " ", lpNote->pitch, lpNote->dir, lpNote->line_pos_in, lpNote->chord_count, lpNote->chord_pos);
  if (lpNote->chord_count > 0) {
    printf("DBG:             chords:");
    for (iChord = 0; iChord < lpNote->chord_count; ++iChord) {
      printf(" [%i]=%i", iChord, lpNote->chords[iChord]);
    }
    printf("\r\n");
  }
  return(RC_OK);
}

int DumpSeparator(SEPARATOR *lpSep)
{
  printf("DBG: item[%03i]: separator row=%2i, size=%2i, style=%2u, pos=%2u\r\n", lpSep->base.idx, lpSep->pos.lrow.value, lpSep->size, lpSep->style, lpSep->line_pos);
  return(RC_OK);
}

int DumpGroupNote(NOTE *lpNote)
{
  int iChord;
  printf("DBG:   item[%03i]: %s len=%2i%1s, chord_count=%2i, chord_pos=%i", lpNote->base.idx, lpNote->is_pause ? "pause" : "note", lpNote->length & LENGTH_MASK, (lpNote->length & LENGTH_PT) ? "p" : " ", lpNote->chord_count, lpNote->chord_pos);
  if (lpNote->chord_count > 0) {
    printf(", chords=");
    for (iChord = 0; iChord < lpNote->chord_count; ++iChord) {
      printf("%i", lpNote->chords[iChord]);
    }
  }
  printf("\r\n");
  return(RC_OK);
}

int DumpGroup(NOTE_GROUP *grp)
{
  char stype[32] = {0};
  if (grp->type != GRP_TYPE_NONE) {
    switch(grp->type) {
      case GRP_TYPE_TIE:
        snprintf(stype, 32, "tie ");
      break;
    }
  }
  if (grp->note_count == 0) {
    printf("DBG: item[%03i]: %sgroup (emtpy)\r\n", grp->base.idx, stype);
    return(RC_OK);
  }
  NOTE *lpNote = &grp->notes[0];
  printf("DBG: item[%03i]: %sgroup row=%2i, val=%2i, dir=%i, line_pos=%i\r\n", grp->base.idx, stype, lpNote->pos.lrow.value, lpNote->pitch, lpNote->dir, lpNote->line_pos_in);
  for (unsigned int i = 0; i < grp->note_count; ++i) {
    DumpGroupNote(&grp->notes[i]);
  }
  return(RC_OK);
}

int DumpMelodyItem(MELODY_ITEM *item)
{
  if (item == NULL) return(RC_ERR);
  switch (item->base.type) {
    case MI_TYPE_NOTE: return(DumpNote(&item->note));
    case MI_TYPE_SEPARATOR: return(DumpSeparator(&item->sep));
    case MI_TYPE_GROUP: return(DumpGroup(&item->grp));
    default:
      printf("DBG: item[%03i]: type %u - %s\r\n", item->base.idx, item->base.type, MI_TYPE__NAME(item->base.type));
    break;
  }
  return(RC_OK);
}

int DumpSong(SONG *song)
{
  for(unsigned int mi_idx = 0; mi_idx < song->melody.item_count; ++mi_idx) {
    DumpMelodyItem(&(song->melody.items[mi_idx]));
  }
  printf("DBG: rows needed: %i\r\n", song->rows_needed);
  return(RC_OK);
}

int DumpMarker(MARKER *lpMrk)
{
  char posstr[128];
  PrintPosition(posstr, 128, &lpMrk->pos);
  printf("DBG: marker[%02i] is at %s\r\n", lpMrk->id, posstr);
  return(RC_OK);
}

int DumpMarkers(SESSION *s)
{
  for (unsigned int midx = 0; midx < MAX_MARKERS; ++midx) {
    MARKER *mrk = &s->markers[midx];
    if (!IsPositionDefined(&mrk->pos)) continue;
    DumpMarker(mrk);
  }
  return(RC_OK);
}

#define DUMP(x) \
  printf("DBG: %s=%i\r\n", #x, s->set.x)
int DumpLayoutSettings(SESSION *s)
{
  DUMP(dim.page.height);
  DUMP(dim.page.width);
  DUMP(dim.page.borders.top);
  DUMP(dim.page.borders.bottom);
  DUMP(dim.page.borders.left);
  DUMP(dim.page.borders.right);
  DUMP(dim.picture.height);
  DUMP(dim.picture.width);
  DUMP(dim.picture.notes.width);
  DUMP(dim.picture.notes.offset);
  DUMP(dim.picture.notes.borders.top);
  DUMP(dim.picture.notes.borders.bottom);
  DUMP(dim.picture.notes.borders.left);
  DUMP(dim.picture.notes.borders.right);
  DUMP(dim.picture.info.width);
  DUMP(dim.picture.info.borders.top);
  DUMP(dim.picture.info.borders.bottom);
  DUMP(dim.picture.row.height);
  DUMP(dim.picture.col.width);
  DUMP(dim.notes.width);
  DUMP(dim.notes.width_dotted);
  DUMP(count.cols);
  DUMP(count.rows);

  return(RC_OK);
}

//*** main routine *************************************************************

int Run(SESSION *session, int argc, char **argv)
{
  int rc = RC_OK;

  FILE *fIn = (FILE*)NULL;
  FILE *fOut = stdout;
  int i;

  char *lpHome;
  char lpDefaultSettingsFileName[PATH_MAX];

  //*** input ***

  // read command line arguments
  if (ReadCommandLineParameters(argc, argv, session) < 0) {
    return(RC_ERR);
  }
  if (session->printHelp) {
    fprintf(stdout, "usage: PS_ZitherLayout [-q|-v] [-p|-c] [-s <setfile>]* [-S <set>]* [-i <infile>*]* [-O <outmode>] [-o <outfile>]\r\n");
    fprintf(stdout, "see readme for more information\r\n");
    return(RC_OK);
  }

  // print welcome message
  if (session->uLogMode >= LOG_MODE_NORMAL) {
    fprintf(stdout, "--- welcome to PiperSoft ZitherLayout (version %s) ---\r\n", VERSION);
#ifdef PSZL_DEVELOP
    fprintf(stdout, "--- build %s %s ---\r\n", __DATE__, __TIME__);
#endif  // develop
  }

  if (!(session->uIOFlags & IO_FLAG_COMBINE)) {
    // set default values
    SetDefaultInputOptions(session);
  }
  SetDefaultLayoutOptions(session);

  // try to read default settings file
  if ((lpHome = getenv("HOME")) != NULL) {
    sprintf(lpDefaultSettingsFileName, "%s/%s", lpHome, DEFAULT_SETTINGS_FILENAME);
    if ((fIn = fopen(lpDefaultSettingsFileName, "r")) != NULL) {
      if (session->uLogMode >= LOG_MODE_VERBOSE) {
        fprintf(stdout, "DBG: reading default settings from '%s'\r\n", lpDefaultSettingsFileName);
      }
      if (ReadInputFile(fIn, (const char*)lpDefaultSettingsFileName, session) < 0) {
        fprintf(stderr, "ERR: invalid default settings file '%s'\r\n", lpDefaultSettingsFileName);
        rc = RC_ERR;
      }
      fclose(fIn);
      if (RC_IS_ERR(rc)) {
        return(rc);
      }
    }
  }

  // read settings (and input) files
  // read input files only, if not running in combine mode
  for (i = 0; i < (int)session->input.uFileCount; ++i) {
    if ((session->uIOFlags & IO_FLAG_COMBINE) && !session->input.files[i].is_settings_file) continue;
    const char *file_type = (!session->input.files[i].is_settings_file) ? "input" : "settings";
    if ((fIn = fopen(session->input.files[i].lpFileName, "r")) == NULL) {
      fprintf(stderr, "ERR: could not open %s file '%s'\r\n", file_type, session->input.files[i].lpFileName);
      return(RC_ERR);
    }
    if (session->uLogMode >= LOG_MODE_VERBOSE) {
      fprintf(stdout, "DBG: reading %s file '%s'\r\n", file_type, session->input.files[i].lpFileName);
    }
    if (ReadInputFile(fIn, session->input.files[i].lpFileName, session) < 0) {
      fprintf(stderr, "ERR: invalid %s file '%s'\r\n", file_type, session->input.files[i].lpFileName);
      rc = RC_ERR;
    }
    fclose(fIn);
    if (RC_IS_ERR(rc)) {
      return(rc);
    }
  }

  if (!(session->uIOFlags & IO_FLAG_COMBINE)) {
    // read stdin if no input file was specified
    if (session->input.bReadStdIn) {
      if (ReadInputFile(stdin, (const char*)NULL, session) < 0) return(RC_ERR);
    }

    // apply settins from command line
    if (RC_IS_OK(rc)) rc = ApplyCommandLineSettings(session);

    // transpose
    if (RC_IS_OK(rc)) rc = Transpose(session);

    // check pitch range
    if (RC_IS_OK(rc)) rc = CheckAndFixPitchRange(session);

    // check maximun lyric number
    if (RC_IS_OK(rc)) {
      for (unsigned int l = 0; l < MAX_LYRICS; ++l) {
        if (strlen(session->song.lyrics.lyrics[l].text) > 0) if (session->song.lyrics.max_num <= l) session->song.lyrics.max_num = l + 1;
      }
    }

    if ((session->output.mode == OUTPUT_MODE_ZITHER) || (session->output.mode == OUTPUT_MODE_NOTE_CODES) || (session->output.mode == OUTPUT_MODE_MTX)) {
      //*** layout ***
      if (session->uLogMode >= LOG_MODE_VERBOSE) {
        DumpMarkers(session); // dump (unresolved) markers
      }
      if (session->output.mode == OUTPUT_MODE_ZITHER) {
        if (RC_IS_OK(rc)) rc = GenerateLayout(session);
      }
      if (session->uLogMode >= LOG_MODE_NORMAL) {
        fprintf(stdout, "INF: found %i melody items\r\n", session->song.melody.item_count);
      }
      if (session->uLogMode >= LOG_MODE_VERBOSE) {
        DumpSong(&session->song);
        DumpMarkers(session); // dump markers again, now resolved
        DumpLayoutSettings(session);
      }
    }
    else if (session->output.mode == OUTPUT_MODE_MIDI) {
      DumpSong(&session->song);
    }
  }

  //*** output ***

  // create output file
  if (session->output.lpFileName != NULL) {
    if ((fOut = fopen(session->output.lpFileName, "wb")) == NULL) {
      fprintf(stderr, "ERR: could not create output file '%s'\r\n", session->output.lpFileName);
      return(RC_ERR);
    }
  }

  if (RC_IS_OK(rc)) rc = Export(fOut, session);

  // close output file
  if ((fOut != NULL) && (fOut != stdout)) {
    fclose(fOut);
  }

  if (RC_IS_ERR(rc)) {
    fprintf(stdout, "--- PS ZitherLayout failed with error %i ---\r\n", rc);
  }
  // print bye bye message
  if (session->uLogMode >= LOG_MODE_NORMAL) {
    fprintf(stdout, "--- bye ---\r\n");
  }

  return(rc);
}

int main(int argc, char **argv)
{
  int rc = RC_OK;

  SESSION session;
  FILE *fIn = (FILE*)NULL;
  FILE *fOut = stdout;
  int i;

  char *lpHome;
  char lpDefaultSettingsFileName[PATH_MAX];

  // clear session
  memset(&session, 0, sizeof(SESSION));
  session.input.first_input_file_idx = -1;
  for (unsigned int i = 0; i < MAX_MELODY_ITEMS; ++i) session.song.melody.items[i].base.idx = i + 1;
  for (unsigned int i = 0; i < MAX_MARKERS; ++i) session.markers[i].id = i + 1;
  for (unsigned int i = 0; i < MAX_RETUNED_NOTES; ++i) session.set.retuned.notes[i].old_pitch = PITCH_NONE;
  for (unsigned int i = 0; i < MAX_MARKED_PITCHES; ++i) session.set.pitch_marks.marks[i].pitch = PITCH_NONE;
  InitFields(&session);
#ifdef PSZL_DEVELOP
  rc = TestFields(&session);
  if (!RC_IS_OK(rc)) {
    fprintf(stderr, "ERR: testing fields failed with error %i\r\n", rc);
    return(EXIT_FAILURE);
  }
#endif  // develop
  // set some defaults
  session.output.mode = OUTPUT_MODE_ZITHER;

  rc = Run(&session, argc, argv);

  // cleanup session
  FreeFields(&session);

  return(RC_IS_ERR(rc) ? EXIT_FAILURE : EXIT_SUCCESS);
}
