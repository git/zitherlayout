/*
  This file is part of PS ZitherLayout.

  PS ZitherLayout is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  PS ZitherLayout is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with PS ZitherLayout.  If not, see <http://www.gnu.org/licenses/>.


  Diese Datei ist Teil von PS ZitherLayout.

  PS ZitherLayout ist Freie Software: Sie können es unter den Bedingungen
  der GNU General Public License, wie von der Free Software Foundation,
  Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren
  veröffentlichten Version, weiterverbreiten und/oder modifizieren.

  PS ZitherLayout wird in der Hoffnung, dass es nützlich sein wird, aber
  OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
  Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
  Siehe die GNU General Public License für weitere Details.

  Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
  Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
*/

/*
**  © 2011-2023 Carsten Pfeffer (PiperSoft)
**
**  @project: PS ZitherLayout
**  @file: pszl_layout.cpp
**  @brief implementation of layout module
**
**  @date: 2011-11-14
**  @author: Carsten Pfeffer (nee Gaede)
**           Carsten Gaede
*/


#include "pszl_layout.h"
#include "pszl_version.h"

#define RC_ERR_BASE RC_ERR_BASE_LAYOUT

#include <stdio.h>

#include "pszl_fields.h"


// definition of directions
#define DIR_NONE  0x00
#define DIR_STAY  0x01
#define DIR_UP    0x02
#define DIR_DOWN  0x03
#define DIR_TURN  0x10
#define DIR_UTURN (DIR_UP | DIR_TURN)
#define DIR_DTURN (DIR_DOWN | DIR_TURN)

// list of chord positions (0: invalid, impossible)
//   first index is line_pos_in (lp)
//   second index is line_pos_in of next note (nlp)
unsigned char ACCRPOS[6][6] = {
// lp \nlp: 0,      L,      TL,     T,      TR,     R
//      to: 0,      R,      BR,     B,      BL,     L
  /*0*/   { RPOS_L, RPOS_L, RPOS_L, RPOS_L, RPOS_R, RPOS_R},
  /*L*/   { RPOS_R, RPOS_B, RPOS_B, RPOS_R, 0,      0},
  /*TL*/  { RPOS_R, RPOS_B, RPOS_B, RPOS_R, RPOS_R, 0},
  /*T*/   { RPOS_L, RPOS_L, RPOS_L, RPOS_L, RPOS_R, RPOS_R},
  /*TR*/  { RPOS_L, 0,      RPOS_L, RPOS_L, RPOS_B, RPOS_B},
  /*R*/   { RPOS_L, 0,      0,      RPOS_L, RPOS_B, RPOS_B}
};


//*** setting default dimensions ***********************************************

/*            norm  smal  tiny
note height   7010  5608  4474
note width    2204  1763  1406
note w. dotd  3439  2751  2195
row height    4000  3000  2500
*/
int SetDimensions(SESSION *s, unsigned char compression)
{
  /*  try to compress by 0.1mm row height, from max row height to 30 use normal notes
   * from 30 to 26 use small notes
   * from 25 to xx use tiny notes
   * */
  s->set.dim.picture.row.height = DIM_DEF_MAX_ROW_HEIGHT - compression;
  if (compression <= MAX_COMPRESSION_NORMAL_SIZE) {
    s->set.dim.notes.width        = 22;
    s->set.dim.notes.width_dotted = 35;
  }
  else if (compression <= MAX_COMPRESSION_SMALL_SIZE) {
    s->set.dim.notes.width        = 18;
    s->set.dim.notes.width_dotted = 28;
  }
  else {
    s->set.dim.notes.width        = 14;
    s->set.dim.notes.width_dotted = 22;
  }
  s->set.dim.notes.dist_grouped = s->set.dim.notes.width_dotted;
  s->set.dim.picture.notes.borders.top = s->set.dim.picture.row.height / 2;
  s->set.dim.picture.notes.borders.bottom = s->set.dim.picture.row.height / 2;

  return(RC_OK);
}

int SetDefaultLayoutOptions(SESSION *s)
{
  // default page dimensions and borders
  s->set.dim.page.height          = DIM_DEF_PAGE_HEIGHT;
  s->set.dim.page.width           = DIM_DEF_PAGE_WIDTH;
  s->set.dim.page.borders.top     = DIM_DEF_PAGE_BORDER;
  s->set.dim.page.borders.bottom  = DIM_DEF_PAGE_BORDER;
  s->set.dim.page.borders.left    = DIM_DEF_PAGE_BORDER;
  s->set.dim.page.borders.right   = DIM_DEF_PAGE_BORDER;
  // default note borders, row and column sizes
  s->set.dim.picture.notes.borders.right  = DIM_DEF_BORDER_RIGHT;
  s->set.dim.picture.col.width = DIM_DEF_COL_WIDTH;

  s->set.dim.picture.info.c1_dist = DIM_DEF_INFO_C1_DIST;
  s->set.dim.picture.info.title.border = DIM_DEF_TITLE_BORDER;
  s->set.dim.picture.info.title.sub_title_dist = DIM_DEF_SUB_TITLE_DIST;

  s->set.dim.chords.width = DIM_DEF_ACCORD_WIDTH;
  s->set.dim.chords.c1_dist = DIM_DEF_ACCORD_C1_DIST;
  s->set.dim.chords.dist = DIM_DEF_ACCORD_DIST;

  s->set.dim.notes.dy = -10;
  s->set.dim.notes.chord.dist = DIM_DEF_ACC_DIST;
  s->set.dim.lyrics.number_dist = DIM_DEF_LYRIC_NUMBER_DIST;

  s->set.dim.cut_edge.width = DIM_DEF_CUT_WIDTH;
  s->set.dim.cut_edge.height = DIM_DEF_CUT_HEIGHT;

  s->set.count.cols = DEF_NUM_STRINGS;
  s->set.count.chords = DEF_MAX_CHORD;

  s->set.notes_position = NOTES_POS_CENTER;
  s->set.compression.min = DEF_MIN_COMPRESSION;
  s->set.compression.max = DEF_MAX_COMPRESSION;

  s->set.dim.picture.info.borders.top = DIM_DEF_MAX_ROW_HEIGHT / 2;
  s->set.dim.picture.info.borders.bottom = DIM_DEF_MAX_ROW_HEIGHT / 2;

  return(RC_OK);
}


//*** check and fix pitch range ***********************************************

int MarkUsedPitch(SESSION *session, unsigned char pitch)
{
  int rc = RC_OK;

  for (unsigned int r = 0; r < MAX_RETUNED_NOTES; ++r) {
    RETUNED_NOTE *rn = &session->set.retuned.notes[r];
    if (rn->old_pitch != pitch) continue;
    rn->in_use = true;
    break;
  }

  return(rc);
}

int CheckAndFixPitchRange(SESSION *s)
{
  unsigned char min_pitch = 0;
  unsigned char max_pitch = MAX_PITCH;
  bool overflow = false;
  bool underflow = false;

  switch (s->output.mode) {
    case OUTPUT_MODE_ZITHER:
    case OUTPUT_MODE_NOTE_CODES:
      min_pitch = 60;                                 // from c1 .. (in PS ZL notation, c.4 in MTX notation)
      max_pitch = min_pitch + s->set.count.cols - 1;  // .. to c3   (default in PS ZL notation, c.6 in MTX notation)
    break;
    case OUTPUT_MODE_MTX:
      min_pitch = 12;         // from c.0 .. (in MTX notation)
      max_pitch = MAX_PITCH;  // .. to g.9   (in MTX notation)
    break;
    case OUTPUT_MODE_MIDI:
      min_pitch = 0;
      max_pitch = MAX_PITCH;
    break;
    case OUTPUT_MODE_LYRICS_NORMAL:
    case OUTPUT_MODE_LYRICS_CMD:
    case OUTPUT_MODE_LYRICS_CMD_ALT:
      // no need to check pitch range for lyrics mode
      return(RC_OK);
    default: return(RC_ERR);
  }

  for (unsigned int mi_idx = 0; mi_idx < s->song.melody.item_count; ++mi_idx) {
    MELODY_ITEM *item = &s->song.melody.items[mi_idx];
    NOTE *note = (NOTE*)NULL;
    switch (item->base.type) {
      case MI_TYPE_NOTE:
        note = &item->note;
        while (note->pitch > max_pitch) {
          overflow = true;
          note->pitch -= 12;  // go down one octave
        }
        while (note->pitch < min_pitch) {
          underflow = true;
          note->pitch += 12;  // go up one octave
        }
        if (!note->is_pause) MarkUsedPitch(s, note->pitch);
      break;
      case MI_TYPE_SEPARATOR:
        // nothing to do
      break;
      case MI_TYPE_GROUP:
        if (item->grp.note_count == 0) continue;
        note = &item->grp.notes[0];
        while (note->pitch > max_pitch) {
          overflow = true;
          note->pitch -= 12;  // go down one octave
        }
        while (note->pitch < min_pitch) {
          underflow = true;
          note->pitch += 12;  // go up one octave
        }
        MarkUsedPitch(s, note->pitch);
      break;
      default: return(RC_ERR);
    }
  }
  if (underflow) fprintf(stderr, "WRN: underflow occured while checking pitch range\r\n");
  if (overflow) fprintf(stderr, "WRN: overflow occured while checking pitch range\r\n");

  return(RC_OK);
}


//*** calculate logical and relative positions *********************************

int CalculateDirections(SONG *song)
{
  for (unsigned int mi_idx = 0; mi_idx < song->melody.item_count; ++mi_idx) {
    MELODY_ITEM *item = &song->melody.items[mi_idx];
    NOTE *note = (NOTE*)NULL;
    unsigned char pitch = PITCH_NONE, prev_pitch = PITCH_NONE, next_pitch = PITCH_NONE;
    unsigned char *dir = (unsigned char*)NULL;
    switch (item->base.type) {
      case MI_TYPE_NOTE:
        note = &item->note;
      break;
      case MI_TYPE_SEPARATOR:
        continue;
      break;
      case MI_TYPE_GROUP:
        if (item->grp.note_count == 0) return(RC_ERR);
        note = &item->grp.notes[0];
      break;
      default: return(RC_ERR);
    }
    if (note == NULL) continue;
    pitch = note->pitch;
    dir = &note->dir;
    if (mi_idx > 0) {
      MELODY_ITEM *prev_item = &song->melody.items[mi_idx - 1];
      switch (prev_item->base.type) {
        case MI_TYPE_NOTE:
          prev_pitch = prev_item->note.pitch;
        break;
        case MI_TYPE_SEPARATOR:
          prev_pitch = PITCH_NONE;
        break;
        case MI_TYPE_GROUP:
          if (prev_item->grp.note_count == 0) return(RC_ERR);
          prev_pitch = prev_item->grp.notes[0].pitch;
        break;
        default: return(RC_ERR);
      }
    }
    if (mi_idx < (song->melody.item_count - 1)) {
      MELODY_ITEM *next_item = &song->melody.items[mi_idx + 1];
      switch (next_item->base.type) {
        case MI_TYPE_NOTE:
          next_pitch = next_item->note.pitch;
        break;
        case MI_TYPE_SEPARATOR:
          next_pitch = PITCH_NONE;
        break;
        case MI_TYPE_GROUP:
          if (next_item->grp.note_count == 0) return(RC_ERR);
          next_pitch = next_item->grp.notes[0].pitch;
        break;
        default: return(RC_ERR);
      }
    }
    if (dir == NULL) continue;
    if (pitch == PITCH_NONE) *dir = DIR_NONE;
    else if (prev_pitch == PITCH_NONE) {
      // first note doesn't have direction
      *dir = DIR_STAY;
    }
    else if (prev_pitch == pitch) {
      // same pitch again
      *dir = DIR_STAY;
    }
    else if (prev_pitch < pitch) {
      if ((next_pitch == PITCH_NONE) || (pitch <= next_pitch)) {
        // going up
        *dir = DIR_UP;
      }
      else {
        // turn around, come from lower, going back to lower
        *dir = DIR_UP | DIR_TURN;
      }
    }
    else {  // prev_val > val
      if ((next_pitch == PITCH_NONE) || (pitch >= next_pitch)) {
        // going down
        *dir = DIR_DOWN;
      }
      else {
        // turn around, come from higher, going back to higher
        *dir = DIR_DOWN | DIR_TURN;
      }
    }
  }

  return(RC_OK);
}

int CalculateRelativeChordPositions(SONG *song)
{
  if (song == NULL) return(RC_ERR);
  for (unsigned int mi_idx = 0; mi_idx < song->melody.item_count; ++mi_idx) {
    MELODY_ITEM *item = &song->melody.items[mi_idx];
    NOTE *note = (NOTE*)NULL;
    unsigned char line_pos_in = RPOS_NONE, next_line_pos_in = RPOS_NONE;
    switch (item->base.type) {
      case MI_TYPE_NOTE:
        note = &item->note;
        if (note->chord_count == 0) continue;
        line_pos_in = note->line_pos_in;
        if (mi_idx < (song->melody.item_count - 1)) {
          MELODY_ITEM *next_item = &song->melody.items[mi_idx + 1];
          switch (next_item->base.type) {
            case MI_TYPE_NOTE:
              next_line_pos_in = next_item->note.line_pos_in;
            break;
          }
        }
        note->chord_pos = ACCRPOS[line_pos_in][next_line_pos_in];
      break;
      case MI_TYPE_SEPARATOR:
        // no chords
      break;
      case MI_TYPE_GROUP:
        for (unsigned int i = 0; i < item->grp.note_count; ++i) {
          item->grp.notes[i].chord_pos = RPOS_BOTTOM;
        }
      break;
      default: return(RC_ERR);
    }
  }

  return(RC_OK);
}

int CalculateNotePositions(SONG *song)
{
  if (song == NULL) return(RC_ERR);
  unsigned char prev_dir = DIR_NONE;
  unsigned short last_row = 0;
  for (unsigned int mi_idx = 0; mi_idx < song->melody.item_count; ++mi_idx) {
    MELODY_ITEM *item = &song->melody.items[mi_idx];
    NOTE *note = (NOTE*)NULL;
    SEPARATOR *sep = (SEPARATOR*)NULL;
    switch (item->base.type) {
      case MI_TYPE_NOTE:
        note = &item->note;
      break;
      case MI_TYPE_SEPARATOR:
        sep = &item->sep;
        sep->pos.lrow.value = last_row + sep->line_pos;
        last_row += sep->size;
        prev_dir = DIR_NONE;
      break;
      case MI_TYPE_GROUP:
        if (item->grp.note_count == 0) return(RC_ERR);
        note = &item->grp.notes[0];
      break;
      default: return(RC_ERR);
    }
    if (note != NULL) {
      note->pos.lcol.value = note->pitch != PITCH_NONE ? note->pitch - 60 : 0;
      if (prev_dir == DIR_NONE) {   // beginning of (a part of) the song
        note->pos.lrow.value = last_row + 2; // leave space for start needle
        note->line_pos_in = RPOS_NONE;
      }
      else if (note->dir == DIR_STAY) {
        note->pos.lrow.value = last_row + 2;
        note->line_pos_in = RPOS_TOP;
      }
      else if ((note->dir & DIR_TURN) || (prev_dir & DIR_TURN)) {
        note->pos.lrow.value = last_row + 1;
        if ((note->dir == DIR_UTURN) || (prev_dir == DIR_DTURN)) {
          note->line_pos_in = RPOS_TL;
        }
        else {
          note->line_pos_in = RPOS_TR;
        }
      }
      else {
        note->pos.lrow.value = last_row;
        if (note->dir == DIR_UP) {
          note->line_pos_in = RPOS_LEFT;
        }
        else {
          note->line_pos_in = RPOS_RIGHT;
        }
      }
      last_row = note->pos.lrow.value;
      prev_dir = note->dir;
    }
  }
  song->rows_needed = last_row + 2;   // leave space for last chord

  return(RC_OK);
}

int CalculateRetuneLinePositions(SESSION *session)
{
  for (unsigned int r = 0; r < MAX_RETUNED_NOTES; ++r) {
    RETUNED_NOTE *rn = &session->set.retuned.notes[r];
    if (rn->old_pitch == PITCH_NONE) continue;
    if (!rn->in_use) continue;
    rn->pos.lrow.value = 0;
    rn->pos.lcol.value = rn->old_pitch - 60;
  }
  return(RC_OK);
}

int CalculatePitchMarkLinePositions(SESSION *session)
{
  for (unsigned int m = 0; m < MAX_MARKED_PITCHES; ++m) {
    PITCH_MARK *pm = &session->set.pitch_marks.marks[m];
    if (pm->pitch == PITCH_NONE) continue;
    pm->pos.lrow.value = 0;
    pm->pos.lcol.value = pm->pitch - 60;
  }
  return(RC_OK);
}


//*** calculate layout parameters **********************************************

int CalculateFixLayoutParameters(SESSION *s)
{
  DIMENSIONS *dim = &s->set.dim;
  dim->picture.height = dim->page.height - dim->page.borders.top - dim->page.borders.bottom;
  dim->picture.width = dim->page.width - dim->page.borders.left - dim->page.borders.right;
  if (dim->picture.notes.borders.left == 0) {   // protect manual settings
    dim->picture.notes.borders.left = dim->picture.col.width / 2;
  }
  dim->picture.notes.width = dim->picture.notes.borders.left + dim->picture.notes.borders.right;
  dim->picture.notes.width += (s->set.count.cols - 1) * dim->picture.col.width;

  dim->picture.info.width = dim->picture.width - dim->picture.notes.width;
  dim->picture.notes.offset = dim->picture.info.width;

  SetDimensions(s, 0);
  s->set.dim.picture.row.c0.height = s->set.dim.picture.row.height;
  s->set.dim.notes.c0.width = s->set.dim.notes.width;
  s->set.dim.notes.c0.width_dotted = s->set.dim.notes.width_dotted;
  s->set.dim.picture.notes.borders.c0.top = s->set.dim.picture.notes.borders.top;
  s->set.dim.picture.notes.borders.c0.bottom = s->set.dim.picture.notes.borders.bottom;
  s->set.count.c0.rows = (s->set.dim.picture.height / s->set.dim.picture.row.c0.height) + 1 - 1;  // one for top / bottom border

  return(RC_OK);
}

int CalculateDynamicLayoutParameters(SESSION *s)
{
  for (unsigned int i = s->set.compression.min; i <= s->set.compression.max; ++i) {
    s->set.compression.current = i;
    SetDimensions(s, i);
    s->set.count.rows = (s->set.dim.picture.height / s->set.dim.picture.row.height) + 1 - 1;  // one for top / bottom border
    if (s->song.rows_needed <= s->set.count.rows) {
      // notes fit on the page
      break;
    }
    else if (i < s->set.compression.max) {
      if (s->uLogMode >= LOG_MODE_NORMAL) {
        printf("INF: switching to compression %i\r\n", i + 1);
      }
    }
  }
  if (s->set.count.rows < s->song.rows_needed) {
    fprintf(stderr, "WRN: not all notes will fit on the page\r\n");
    return(RC_WARN);
  }
  else if (s->song.rows_needed < s->set.count.rows) { // center notes
    int i = 0;
    switch(s->set.notes_position) {
      case NOTES_POS_TOP:
      break;
      case NOTES_POS_CENTER:
        i = (s->set.count.rows - s->song.rows_needed) / 2;
      break;
      case NOTES_POS_BOTTOM:
        i = s->set.count.rows - s->song.rows_needed;
      break;
      default: return(RC_ERR);
    }
    if (i > 0) {
      if (s->uLogMode >= LOG_MODE_VERBOSE) {
        printf("DBG: positioning notes, skipping %i rows\r\n", i);
      }
      for (unsigned int mi_idx = 0; mi_idx < s->song.melody.item_count; ++mi_idx) {
        MELODY_ITEM *item = &s->song.melody.items[mi_idx];
        switch (item->base.type) {
          case MI_TYPE_NOTE:
            item->note.pos.lrow.value += i;
          break;
          case MI_TYPE_SEPARATOR:
            item->sep.pos.lrow.value += i;
          break;
          case MI_TYPE_GROUP:
            if (item->grp.note_count == 0) return(RC_ERR);
            item->grp.notes[0].pos.lrow.value += i;
          break;
          default: return(RC_ERR);
        }
      }
    }
  }

  return(RC_OK);
}

int CalculateDynamicDimensions(SESSION *s)
{
  s->set.dim.notes.dx = -(s->set.dim.notes.width / 2);
  s->set.dim.notes.start_needle_size = s->set.dim.notes.width / 2;
  s->set.dim.notes.circle_size = (5 * s->set.dim.notes.width) / 2;

  s->set.dim.notes.chord.dx_l = -s->set.dim.notes.chord.dist + s->set.dim.notes.dx;
  s->set.dim.notes.chord.dx_r = s->set.dim.notes.chord.dist + s->set.dim.notes.width_dotted + s->set.dim.notes.dx;
  s->set.dim.notes.chord.dy_b = -s->set.dim.notes.chord.dist + s->set.dim.notes.dy;

  s->set.dim.picture.info.title.width = s->set.dim.picture.height - s->set.dim.picture.info.borders.top - s->set.dim.picture.info.borders.bottom - (2 * s->set.dim.picture.info.title.border);

  return(RC_OK);
}


//*** calculate real positions *************************************************

int CalculateRealPosition(SESSION *s, POSITION *pos, unsigned int uFormat)
{
  unsigned int rel_x, rel_y;
  switch (uFormat & POSFMT_MASK) {
    case POSFMT_NONE:
    case POSFMT_CR:
      rel_x = pos->lcol.value * s->set.dim.picture.col.width;
      rel_y = pos->lrow.value * s->set.dim.picture.row.height;
    break;
    case POSFMT_CR_ABS:
      rel_x = pos->lcol.value * s->set.dim.picture.col.width;
      rel_y = pos->lrow.value * s->set.dim.picture.row.c0.height;
    break;
    case POSFMT_XY:
      rel_x = pos->lcol.value * 10;  // by default col is in mm, dim is in 1/10th of mm (see fine modifier below)
      rel_y = pos->lrow.value * 10;
    break;
    default:
#ifdef PSZL_DEVELOP
      fprintf(stdout, "DBG: unhandled positioning format %u\r\n", uFormat);
#endif
      return(CalculateRealPosition(s, pos, POSFMT_CR));
    break;
  }
  if (uFormat & POSMOD_FINE) {
    rel_x = (rel_x + 5) / 10;
    rel_y = (rel_y + 5) / 10;
  }
  pos->x = s->set.dim.picture.notes.offset + s->set.dim.picture.notes.borders.left + rel_x;
  switch(uFormat & POSFMT_MASK) {
    case POSFMT_NONE:
    case POSFMT_CR:
      pos->y = s->set.dim.picture.height - s->set.dim.picture.notes.borders.top - rel_y;
    break;
    case POSFMT_CR_ABS:
    case POSFMT_XY:
      pos->y = s->set.dim.picture.height - s->set.dim.picture.notes.borders.c0.top - rel_y;
    break;
  }

  return(RC_OK);
}

int CalculateRealPositions(SESSION *s)
{
  // calculate position from line and value
  for (unsigned int mi_idx = 0; mi_idx < s->song.melody.item_count; ++mi_idx) {
    MELODY_ITEM *item = &s->song.melody.items[mi_idx];
    switch (item->base.type) {
      case MI_TYPE_NOTE:
        CalculateRealPosition(s, &item->note.pos, 0);
      break;
      case MI_TYPE_SEPARATOR:
        CalculateRealPosition(s, &item->sep.pos, 0);
      break;
      case MI_TYPE_GROUP:
        if (item->grp.note_count == 0) return(RC_ERR);
        CalculateRealPosition(s, &item->grp.notes[0].pos, 0);
      break;
      default: return(RC_ERR);
    }
  }
  unsigned char prev_pitch = PITCH_NONE;
  for (unsigned int mi_idx = 0; mi_idx < s->song.melody.item_count; ++mi_idx) {
    MELODY_ITEM *item = &s->song.melody.items[mi_idx];
    NOTE *note = (NOTE*)NULL;
    switch (item->base.type) {
      case MI_TYPE_NOTE:
        note = &item->note;
      break;
      case MI_TYPE_SEPARATOR:
        prev_pitch = PITCH_NONE;
      break;
      case MI_TYPE_GROUP:
        if (item->grp.note_count == 0) return(RC_ERR);
        note = &item->grp.notes[0];
      break;
      default: return(RC_ERR);
    }
    if (note != NULL) {
      if (prev_pitch == PITCH_NONE) {
        note->start_needle_pos.x = note->pos.x;
        note->start_needle_pos.y = note->pos.y + (2 * s->set.dim.picture.row.height);
      }
      prev_pitch = note->pitch;
    }
  }
  for (unsigned int i = 0; i < MAX_LYRICS; ++i) {
    CalculateRealPosition(s, &s->song.lyrics.lyrics[i].pos, s->song.lyrics.pos_fmt);
  }
  CalculateRealPosition(s, &s->song.lyrics.refrain.pos, s->song.lyrics.pos_fmt);
  for (unsigned int i = 0; i < MAX_ADDITIONS; ++i) {
    CalculateRealPosition(s, &s->song.add.adds[i].pos, s->song.add.pos_fmt);
  }
  for (unsigned int r = 0; r < MAX_RETUNED_NOTES; ++r) {
    CalculateRealPosition(s, &s->set.retuned.notes[r].pos, 0);
  }
  for (unsigned int m = 0; m < MAX_MARKED_PITCHES; ++m) {
    CalculateRealPosition(s, &s->set.pitch_marks.marks[m].pos, 0);
  }

  return(RC_OK);
}

/*
**  number of grouped notes
**        relative position of notes
**              X position of non-grouped note
**                        relative columns
**        : : : : : : :
**    1   : : : X : : :   0
**    2   : : X : X : :   -1 1
**    3   : X : X : X :   -2 0 2
**    4   X : X : X : X   -3 -1 1 3
*/
int CalculateGroupNotePositions(SESSION *s)
{
  if (s == NULL) return(RC_ERR);
  int rc = RC_OK;

  for (unsigned int mi_idx = 0; (RC_IS_OK(rc)) && (mi_idx < s->song.melody.item_count); ++mi_idx) {
    MELODY_ITEM *item = &s->song.melody.items[mi_idx];
    if (item->base.type != MI_TYPE_GROUP) continue;
    NOTE_GROUP *grp = &item->grp;
    if (grp->note_count == 0) return(RC_ERR);
    int rel_col = 1 - grp->note_count;
    for (unsigned int n_idx = 0; (RC_IS_OK(rc)) && (n_idx < grp->note_count); ++n_idx) {
      grp->note_pos_x[n_idx] = grp->notes[0].pos.x + (rel_col * (int)s->set.dim.notes.dist_grouped / 2);
      rel_col += 2;
    }
  }

  return(rc);
}

//*** marker resolution ***

int ResolveMarker_MIRef(SESSION *session, MARKER *marker, bool *touched_markers)
{
  if (touched_markers[marker->id - 1]) {
    fprintf(stderr, "ERR: loop detected while resolving marker [%u]\r\n", marker->id);
    return(RC_ERR);
  }
  touched_markers[marker->id - 1] = true;
  if (!IsPositionDefined(&marker->pos)) return(RC_OK);

  int rc = RC_OK;

  if (marker->pos.ref_mi_idx != 0) {
    if (marker->pos.ref_mi_idx > session->song.melody.item_count) return(RC_ERR); // handled before, but check
    MELODY_ITEM *ref_mi = &session->song.melody.items[marker->pos.ref_mi_idx - 1];
    switch (ref_mi->base.type) {
      case MI_TYPE_NOTE:
        marker->pos.lcol.value += ref_mi->note.pos.lcol.value;
        marker->pos.lrow.value += ref_mi->note.pos.lrow.value;
      break;
      case MI_TYPE_SEPARATOR:
        // col is not defined by separator
        marker->pos.lrow.value += ref_mi->sep.pos.lrow.value;
      break;
      case MI_TYPE_GROUP:
        if (ref_mi->grp.note_count == 0) return(RC_ERR);  // handled before, but check
        marker->pos.lcol.value += ref_mi->grp.notes[0].pos.lcol.value;
        marker->pos.lrow.value += ref_mi->grp.notes[0].pos.lrow.value;
      break;
    }
    marker->pos.ref_mi_idx = 0; // resolved, remove melody item reference
  }

  return(rc);
}

int ResolveMarker_Col(SESSION *session, MARKER *marker, bool *touched_markers)
{
  if (touched_markers[marker->id - 1]) {
    fprintf(stderr, "ERR: loop detected while resolving marker [%u] column\r\n", marker->id);
    return(RC_ERR);
  }
  touched_markers[marker->id - 1] = true;
  if (!IsPositionDefined(&marker->pos)) return(RC_OK);

  int rc = RC_OK;

  if (marker->pos.lcol.ref_mrk_id != 0) {
    if (marker->pos.lcol.ref_mrk_id > MAX_MARKERS) return(RC_ERR);
    MARKER *ref_marker = &session->markers[marker->pos.lcol.ref_mrk_id - 1];
    if (RC_IS_OK(rc)) rc = ResolveMarker_Col(session, ref_marker, touched_markers);
    marker->pos.lcol.value += ref_marker->pos.lcol.value;
    marker->pos.lcol.ref_mrk_id = 0; // resolved, remove marker reference
  }

  return(rc);
}

int ResolveMarker_Row(SESSION *session, MARKER *marker, bool *touched_markers)
{
  if (touched_markers[marker->id - 1]) {
    fprintf(stderr, "ERR: loop detected while resolving marker [%u] row\r\n", marker->id);
    return(RC_ERR);
  }
  touched_markers[marker->id - 1] = true;
  if (!IsPositionDefined(&marker->pos)) return(RC_OK);

  int rc = RC_OK;

  if (marker->pos.lrow.ref_mrk_id != 0) {
    if (marker->pos.lrow.ref_mrk_id > MAX_MARKERS) return(RC_ERR);
    MARKER *ref_marker = &session->markers[marker->pos.lrow.ref_mrk_id - 1];
    if (RC_IS_OK(rc)) rc = ResolveMarker_Row(session, ref_marker, touched_markers);
    marker->pos.lrow.value += ref_marker->pos.lrow.value;
    marker->pos.lrow.ref_mrk_id = 0; // resolved, remove marker reference
  }

  return(rc);
}

int ResolvePosition(SESSION *session, POSITION *pos)
{
  int rc = RC_OK;
  if (pos->ref_mi_idx > 0) return(RC_ERR);  // not yet implemented
  else {
    if (pos->lcol.ref_mrk_id > 0) {
      if (pos->lcol.ref_mrk_id > MAX_MARKERS) return(RC_ERR);
      MARKER *ref_marker = &session->markers[pos->lcol.ref_mrk_id - 1];
      if (ref_marker->pos.ref_mi_idx > 0) return(RC_ERR); // unresolved marker
      if (ref_marker->pos.lcol.ref_mrk_id > 0) return(RC_ERR); // unresolved marker
      pos->lcol.value += ref_marker->pos.lcol.value;
      pos->lcol.ref_mrk_id = 0; // resolved, remove marker reference
    }
    if (pos->lrow.ref_mrk_id > 0) {
      if (pos->lrow.ref_mrk_id > MAX_MARKERS) return(RC_ERR);
      MARKER *ref_marker = &session->markers[pos->lrow.ref_mrk_id - 1];
      if (ref_marker->pos.ref_mi_idx > 0) return(RC_ERR); // unresolved marker
      if (ref_marker->pos.lrow.ref_mrk_id > 0) return(RC_ERR); // unresolved marker
      pos->lrow.value += ref_marker->pos.lrow.value;
      pos->lrow.ref_mrk_id = 0; // resolved, remove marker reference
    }
  }
  return(rc);
}

int ResolveMarkerPositions(SESSION *session)
{
  int rc = RC_OK;
  bool touched_markers[MAX_MARKERS] = {0};

  for (unsigned int m = 0; (RC_IS_OK(rc) && (m < MAX_MARKERS)); ++m) {
    MARKER *marker = &session->markers[m];
    if (!IsMarkerDefined(marker)) continue;
    if (RC_IS_OK(rc)) rc = ResolveMarker_MIRef(session, marker, touched_markers);
    for (unsigned int tm = 0; tm < MAX_MARKERS; ++tm) touched_markers[tm] = false;
    if (RC_IS_OK(rc)) rc = ResolveMarker_Col(session, marker, touched_markers);
    for (unsigned int tm = 0; tm < MAX_MARKERS; ++tm) touched_markers[tm] = false;
    if (RC_IS_OK(rc)) rc = ResolveMarker_Row(session, marker, touched_markers);
  }
  for (unsigned int l = 0; (RC_IS_OK(rc) && (l < MAX_LYRICS)); ++l) {
    if (RC_IS_OK(rc)) rc = ResolvePosition(session, &session->song.lyrics.lyrics[l].pos);
  }
  for (unsigned int at = 0; (RC_IS_OK(rc) && (at < MAX_ADDITIONS)); ++at) {
    if (RC_IS_OK(rc)) rc = ResolvePosition(session, &session->song.add.adds[at].pos);
  }

  return(rc);
}


//*** generate complete layout *************************************************

int GenerateLayout(SESSION *s)
{
  int rc = RC_OK;

  if (RC_IS_OK(rc)) rc = CalculateDirections(&s->song);
  if (RC_IS_OK(rc)) rc = CalculateNotePositions(&s->song);
  if (RC_IS_OK(rc)) rc = CalculateRelativeChordPositions(&s->song);
  if (RC_IS_OK(rc)) rc = CalculateRetuneLinePositions(s);
  if (RC_IS_OK(rc)) rc = CalculatePitchMarkLinePositions(s);

  if (RC_IS_OK(rc)) rc = CalculateFixLayoutParameters(s);
  if (RC_IS_OK(rc)) rc = CalculateDynamicLayoutParameters(s);
  if (RC_IS_OK(rc)) rc = CalculateDynamicDimensions(s);

  if (RC_IS_OK(rc)) rc = ResolveMarkerPositions(s);

  if (RC_IS_OK(rc)) rc = CalculateRealPositions(s);
  if (RC_IS_OK(rc)) rc = CalculateGroupNotePositions(s);

  return(rc);
}
