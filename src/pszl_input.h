/*
  This file is part of PS ZitherLayout.

  PS ZitherLayout is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  PS ZitherLayout is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with PS ZitherLayout.  If not, see <http://www.gnu.org/licenses/>.


  Diese Datei ist Teil von PS ZitherLayout.

  PS ZitherLayout ist Freie Software: Sie können es unter den Bedingungen
  der GNU General Public License, wie von der Free Software Foundation,
  Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren
  veröffentlichten Version, weiterverbreiten und/oder modifizieren.

  PS ZitherLayout wird in der Hoffnung, dass es nützlich sein wird, aber
  OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
  Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
  Siehe die GNU General Public License für weitere Details.

  Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
  Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
*/

/*
**  © 2011-2023 Carsten Pfeffer (PiperSoft)
**
**  @project: PS ZitherLayout
**  @file: pszl_input.h
**  @brief definition of input module
**
**  @date: 2011-11-13
**  @author: Carsten Pfeffer (nee Gaede)
**           Carsten Gaede
*/
#pragma once


#include <stdio.h>

#include "pszl_definitions.h"


#define STR_IS(str, cmp) (strcmp(str, cmp) == 0)
#define STR_IS_ONE_OF(str, cmp1, cmp2) (STR_IS(str, cmp1) || STR_IS(str, cmp2))
#define STR_IS_ONE_OF_THREE(str, cmp1, cmp2, cmp3) (STR_IS(str, cmp1) || STR_IS(str, cmp2) || STR_IS(str, cmp3))
#define STR_STARTS_WITH(str, pre) (strncmp(str, pre, strlen(pre)) == 0)
#define STR_STARTS_WITH_ONE_OF(str, pre1, pre2) (STR_STARTS_WITH(str, pre1) || STR_STARTS_WITH(str, pre2))

#define OPERATOR_NONE   0
#define OPERATOR_SET    1
#define OPERATOR_ADD    2
#define OPERATOR_SUB    3
//#define OPERATOR_MUL    4
//#define OPERATOR_DIV    5
//#define OPERATOR_MOD    6


unsigned char GetLength(int iLength);

int ParsePitch(SESSION *session, const char *lpInput, bool bAllowSuffix, unsigned char *lpPitch, unsigned char *lpOctave, unsigned int *lpValueLength);

int SetDefaultInputOptions(SESSION *s);
int ReadCommandLineParameters(int argc, char **argv, SESSION *s);
int ReadInputFile(FILE *lpFile, const char *lpFileName, SESSION *s);
int ApplyCommandLineSettings(SESSION *s);
int Transpose(SESSION *s);
