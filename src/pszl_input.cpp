/*
  This file is part of PS ZitherLayout.

  PS ZitherLayout is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  PS ZitherLayout is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with PS ZitherLayout.  If not, see <http://www.gnu.org/licenses/>.


  Diese Datei ist Teil von PS ZitherLayout.

  PS ZitherLayout ist Freie Software: Sie können es unter den Bedingungen
  der GNU General Public License, wie von der Free Software Foundation,
  Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren
  veröffentlichten Version, weiterverbreiten und/oder modifizieren.

  PS ZitherLayout wird in der Hoffnung, dass es nützlich sein wird, aber
  OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
  Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
  Siehe die GNU General Public License für weitere Details.

  Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
  Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
*/

/*
**  © 2011-2023 Carsten Pfeffer (PiperSoft)
**
**  @project: PS ZitherLayout
**  @file: pszl_input.cpp
**  @brief implementation of input module
**
**  @date: 2011-11-13
**  @author: Carsten Pfeffer (nee Gaede)
**           Carsten Gaede
*/


#include "pszl_input.h"
#include "pszl_version.h"

#define RC_ERR_BASE RC_ERR_BASE_INPUT

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pszl_fields.h"


#define INPUT_TYPE_NONE     0
#define INPUT_TYPE_FIELD    1   // field value, command or song info
#define INPUT_TYPE_FIELD_LE 2   // field value, ending at end of line
#define INPUT_TYPE_MELODY   3   // melody input: notes, pauses, separators, groups



// definition of states
#define STATE_INIT      0
// note or pause
#define STATE_LENGTH    1
#define STATE_VALUE     2
#define STATE_OCTAVE    3
#define STATE_CHORD     4
// separator
#define STATE_SEP_SIZE  1
#define STATE_SEP_STYLE 2
#define STATE_SEP_POS   3
// marker
#define STATE_MRK_ID    1
// common
#define STATE_END       9
#define STATE_FLAG_KEEP_POSITION  0x80

// pitches from c1 to c2 in PSZL notation and MTX/MIDI note value
// PSZL c1 +c1 d1 +d1 e1 f1 +f1 g1 +g1 a1 +a1 h1 c2
// MIDI 60  61 62  63 64 65  66 67  68 69  70 71 72
//                         a   b   c   d   e   f   g   h
unsigned char PITCHES[] = {69, 70, 60, 62, 64, 65, 67, 71};

struct INPUT_ITEMS {
  char *line_copy;
  char *items[MAX_INPUT_LENGTH];
  unsigned int item_count;
};



//*** initialize default settings **********************************************

int SetDefaultInputOptions(SESSION *s)
{
  s->set.defaults.length = LENGTH_4;
  s->set.defaults.octave = 1;
  for (unsigned int i = 0; i < MAX_LYRICS; ++i) {
    s->song.lyrics.lyrics[i].visible = true;
  }

  return(RC_OK);
}


//*** generic read methods *****************************************************

#define CHECK_LENGTH(x) \
  case x:\
    return(LENGTH_##x);\
  break;
unsigned char GetLength(int iLength)
{
  switch(iLength) {
    CHECK_LENGTH(1);
    CHECK_LENGTH(2);
    CHECK_LENGTH(4);
    CHECK_LENGTH(8);
    CHECK_LENGTH(16);
    CHECK_LENGTH(32);
    CHECK_LENGTH(64);
    //CHECK_LENGTH(128);
    //CHECK_LENGTH(256);
    default:
      return(LENGTH_INVALID);
    break;
  }
}

#define CHECK_LENGTH_REV(x) \
  case LENGTH_##x: \
    return(x); \
  break;
int GetLengthRev(unsigned char bLength)
{
  switch (bLength) {
    CHECK_LENGTH_REV(1);
    CHECK_LENGTH_REV(2);
    CHECK_LENGTH_REV(4);
    CHECK_LENGTH_REV(8);
    CHECK_LENGTH_REV(16);
    CHECK_LENGTH_REV(32);
    CHECK_LENGTH_REV(64);
    //CHECK_LENGTH_REV(128);
    //CHECK_LENGTH_REV(256);
    default:
      return(RC_ERR);
    break;
  }
}


//*** read command line ********************************************************

option LONG_OPTIONS[] = {
  {"help", no_argument, 0, 'h'},                // print help and exit
  {"quiet", no_argument, 0, 'q'},
  {"verbose", no_argument, 0, 'v'},
  {"part", no_argument, 0, 'p'},                // set part mode
  {"combine", no_argument, 0, 'c'},             // set combine mode
  {"settings-file", required_argument, 0, 's'}, // settings file; accepts multiple files
  {"set", required_argument, 0, 'S'},           // settings from command line
  {"input-file", required_argument, 0, 'i'},    // input file, if none given, use stdin; accepts multiple files
  {"output-mode", required_argument, 0, 'O'},   // output mode, use 'zither' of not given
  {"output-file", required_argument, 0, 'o'},   // output file, if none given, use stdout
  {0, 0, 0, 0}
};
#define OPTION_STRING "hqvpcs:S:i:O:o:"

int ReadCommandLineParameters(int argc, char **argv, SESSION *s)
{
  bool reading_input_files = false; // reading '-i' (and not '-s') arguments
  int arg;

  s->input.bReadStdIn = true;
#ifndef PSZL_DEVELOP
  s->uLogMode = LOG_MODE_NORMAL;
#else
  s->uLogMode = LOG_MODE_VERBOSE;
#endif

  while ((arg = getopt_long(argc, argv, OPTION_STRING, LONG_OPTIONS, NULL)) >= 0) {
//fprintf(stderr, "DBG: reading option %i ('%c')\r\n", arg, arg);
    switch(arg) {
      case 'h':
        s->printHelp = true;
        return(RC_OK);
      break;
      case '?':
        fprintf(stderr, "ERR: see readme for known options\r\n");
        return(RC_ERR);
      break;
      case 'q':
#ifndef PSZL_DEVELOP
        s->uLogMode = LOG_MODE_QUIET;
#endif  // develop
      break;
      case 'v':
#ifndef PSZL_DEVELOP
        s->uLogMode = LOG_MODE_VERBOSE;
#endif  // develop
      break;
      case 'p':
        s->uIOFlags |= IO_FLAG_PART;
      break;
      case 'c':
        s->uIOFlags |= IO_FLAG_COMBINE;
      break;
      case 'i':
        s->input.bReadStdIn = false;
        reading_input_files = true;
      //break; continue, 'i' is same as 's', but will skip stdin later
      case 's':
        if (optarg != NULL) {
          if (s->input.uFileCount >= MAX_INPUT_FILES) {
            fprintf(stderr, "ERR: too many settings / input files\r\n");
            return(RC_ERR);
          }
//fprintf(stderr, "DBG: input file = '%s'\r\n", optarg);
          s->input.files[s->input.uFileCount].lpFileName = optarg;
          s->input.files[s->input.uFileCount].is_settings_file = !reading_input_files;
          if (reading_input_files && (s->input.first_input_file_idx < 0)) s->input.first_input_file_idx = s->input.uFileCount;
          ++s->input.uFileCount;
        }
        if (reading_input_files) {
          while ((optind < argc) && (*argv[optind] != '-')) {
//fprintf(stderr, "DBG: input file '%s'\r\n", argv[optind]);
            if (s->input.uFileCount >= MAX_INPUT_FILES) {
              fprintf(stderr, "ERR: too many settings / input files\r\n");
              return(RC_ERR);
            }
            s->input.files[s->input.uFileCount].lpFileName = argv[optind];
            s->input.files[s->input.uFileCount].is_settings_file = !reading_input_files;
            ++s->input.uFileCount;
            ++optind;
          }
        }
      break;
      case 'S':
        if (s->set.cmd_line.count >= MAX_CL_SETTINGS) {
          fprintf(stderr, "ERR: too many command line settings\r\n");
          return(RC_ERR);
        }
        s->set.cmd_line.items[s->set.cmd_line.count] = optarg;
        ++s->set.cmd_line.count;
      break;
      case 'o':
        if (s->output.lpFileName != NULL) {
          fprintf(stderr, "ERR: more than one output file given\r\n");
          return(RC_ERR);
        }
        s->output.lpFileName = optarg;
      break;
      case 'O':
        if (STR_IS_ONE_OF(optarg, "z", "zither")) {
          s->output.mode = OUTPUT_MODE_ZITHER;
        }
        else if (STR_IS_ONE_OF(optarg, "l", "lyrics")) {
          s->output.mode = OUTPUT_MODE_LYRICS_NORMAL;
        }
        else if (STR_IS_ONE_OF(optarg, "lc", "lyrics-cmd")) {
          s->output.mode = OUTPUT_MODE_LYRICS_CMD;
        }
        else if (STR_IS_ONE_OF(optarg, "lca", "lyrics-cmd-alt")) {
          s->output.mode = OUTPUT_MODE_LYRICS_CMD_ALT;
        }
        else if (STR_IS_ONE_OF(optarg, "nc", "note-codes")) {
          s->output.mode = OUTPUT_MODE_NOTE_CODES;
        }
        else if (STR_IS(optarg, "mtx")) {
          s->output.mode = OUTPUT_MODE_MTX;
        }
        else if (STR_IS(optarg, "midi")) {
          s->output.mode = OUTPUT_MODE_MIDI;
        }
        else {
          fprintf(stderr, "ERR: invalid output mode\r\n");
          return(RC_ERR);
        }
      break;
    }
  }
//  fprintf(stderr, "DBG: reading command line options finished\r\n");

  return(RC_OK);
}


//*** read input files *********************************************************

int ReadFieldValue(const char *lpFieldName, const char *lpRawValue, unsigned char uOperator, const char *lpCurrentFileName, SESSION *s)
{
  int rc = RC_OK;
  FIELD_VALUE_REF fvref;
  FIELD_VALUE fval;

  memset(&fvref, 0, sizeof(FIELD_VALUE_REF));
  memset(&fval, 0, sizeof(FIELD_VALUE));

  rc = GetFieldValueRef(s, lpFieldName, &fvref);
  if (RC_IS_ERR(rc)) {
    fprintf(stderr, "ERR: unknown field '%s'\r\n", lpFieldName);
    fprintf(stderr, "ERR: last error is %i\r\n", rc);
    return(rc);
  }
  // fprintf(stdout, "DBG: '%s' is field ID %u", lpFieldName, fvref.id);
  // if (fvref.index != 0) {
  //   fprintf(stdout, ", index %8", fvref.index);
  // }
  // fprintf(stdout, ", type %u", fvref.value_type);
  // fprintf(stdout, "\r\n");

  rc = ParseFieldValue(s, &fvref, lpRawValue, &fval);
  if (RC_IS_ERR(rc)) {
    fprintf(stderr, "ERR: invalid field '%s' value '%s'\r\n", lpFieldName, lpRawValue);
    fprintf(stderr, "ERR: last error is %i\r\n", rc);
    return(rc);
  }
  else if (rc == RC_WARN) {
    fprintf(stderr, "WRN: ignoring invalid field '%s' value '%s'\r\n", lpFieldName, lpRawValue);
    fprintf(stderr, "WRN: last warning code is %i\r\n", rc);
    return(rc);
  }
  // switch (fval.type) {
  //   case FVT_STR:
  //     // fprintf(stdout, "DBG: field value is '%s'\r\n", fval.sval);
  //   break;
  //   case FVT_INT:
  //   case FVT_NOTEPOS:
  //   case FVT_NOTELEN:
  //   case FVT_POSFMT:
  //   case FVT_STY:
  //     // fprintf(stdout, "DBG: field value is %i\r\n", fval.ival);
  //   break;
  //   case FVT_BOOL:
  //     // fprintf(stdout, "DBG: field value is %s\r\n", fval.bval ? "true" : "false");
  //   break;
  //   case FVT_POS:
  //     // fprintf(stdout, "DBG: field value is %i,%i\r\n", fval.pval.col, fval.pval.row);
  //   break;
  //   default:
  //     fprintf(stderr, "WRN: unhandled value type %u\r\n", fval.type);
  //   break;
  // }

  //*** special fields *********************************************************
  // in|input-file: input file name
  if (fvref.id == FID_IN) {
    FILE *fIn = (FILE*)NULL;
    char file_name[MAX_INPUT_LENGTH + 1];
    if (lpCurrentFileName != NULL) {
      char path[MAX_INPUT_LENGTH + 1];
      snprintf(path, MAX_INPUT_LENGTH + 1, lpCurrentFileName);
      char *pos = strrchr(path, '/');
      if (pos == NULL) {
        pos = strrchr(path, '\\');
      }
      if (pos != NULL) {
        *pos = '\0';
        snprintf(file_name, MAX_INPUT_LENGTH + 1, "%s/%s", path, fval.sval);
      }
      else {
        snprintf(file_name, MAX_INPUT_LENGTH + 1, "./%s", fval.sval);
      }
    }
    else {
      snprintf(file_name, MAX_INPUT_LENGTH + 1, "./%s", fval.sval);
    }
    if ((fIn = fopen(file_name, "r")) == NULL) {
      fprintf(stderr, "ERR: could not open input file '%s'\r\n", file_name);
      rc = RC_ERR;
    }
    if (s->uLogMode >= LOG_MODE_VERBOSE) {
      fprintf(stdout, "DBG: reading input file '%s'\r\n", file_name);
    }
    if (ReadInputFile(fIn, (const char*)file_name, s) < 0) {
      fprintf(stderr, "ERR: invalid input file '%s'\r\n", file_name);
      rc = RC_ERR;
    }
    if (fIn != NULL) {
      fclose(fIn);
      fIn = NULL;
    }
    return(rc);
  }

  rc = SetFieldValue(s, &fvref, &fval, uOperator);
  if (RC_IS_ERR(rc)) {
    fprintf(stderr, "ERR: setting field '%s' value '%s' failed\r\n", lpFieldName, lpRawValue);
    fprintf(stderr, "ERR: last error is %i\r\n", rc);
    return(rc);
  }

  return(rc);
}

int ReadFieldValue(const char *lpInput, const char *lpCurrentFileName, SESSION *s)
{
  char cmd[MAX_INPUT_LENGTH + 1];
  char val[MAX_INPUT_LENGTH + 1];
  strncpy(cmd, lpInput, MAX_INPUT_LENGTH);
  char *eq_pos = strchr(cmd, '=');
  unsigned char op = OPERATOR_SET;
  FIELD_VALUE fval;
  memset(&fval, 0, sizeof(FIELD_VALUE));

  if (eq_pos != NULL) {
    *eq_pos = '\0';
    strncpy(val, eq_pos + 1, MAX_INPUT_LENGTH);
    --eq_pos;
    switch (*eq_pos) {
      case '+':
        op = OPERATOR_ADD;
        *eq_pos = '\0';
      break;
      case '-':
        op = OPERATOR_SUB;
        *eq_pos = '\0';
      break;
      /*case '*':
        op = OPERATOR_MUL;
        *eq_pos = '\0';
      break;
      case '/':
        op = OPERATOR_DIV;
        *eq_pos = '\0';
      break;
      case '%':
        op = OPERATOR_MOD;
        *eq_pos = '\0';
      break;*/
      default:
        op = OPERATOR_SET;
      break;
    }
  }
  else {
    val[0] = '\0';
  }

  return(ReadFieldValue(cmd, val, op, lpCurrentFileName, s));
}

int ReadFieldValues(INPUT_ITEMS *lpItems, const char *lpCurrentFileName, SESSION *s)
{
  if ((lpItems == NULL) || (s == NULL)) return(RC_ERR);
  int rc = RC_OK;

  for (unsigned int i = 0; (RC_IS_OK(rc)) && (i < lpItems->item_count); ++i) {
    rc = ReadFieldValue(lpItems->items[i], lpCurrentFileName, s);
  }
  return(rc);
}

int ParsePitch(SESSION *session, const char *lpInput, bool bAllowSuffix, unsigned char *lpPitch, unsigned char *lpOctave, unsigned int *lpValueLength)
{
  *lpPitch = PITCH_NONE;
  int rc = RC_OK;
  unsigned char pitch = 0;
  unsigned char octave = 0;
  const char *p = lpInput;

  if (RC_IS_OK(rc)) if (*p == '-') { pitch -= 1; ++p; } else if (*p == '+') { pitch += 1; ++p; }
  if (RC_IS_OK(rc)) if ((*p < 'a') || (*p > 'h')) rc = RC_ERR;
  if (RC_IS_OK(rc)) pitch += PITCHES[*p - 'a'];
  if (RC_IS_OK(rc)) ++p;
  if (RC_IS_OK(rc)) if ((*p >= '0') && (*p <= '9')) { octave = *p - '0'; ++p; }
  if (RC_IS_OK(rc)) if (!bAllowSuffix) if (*p != '\0') rc = RC_ERR;
  if (RC_IS_OK(rc)) pitch += (12 * (octave - 1));
  if (RC_IS_OK(rc)) *lpPitch = pitch;
  if (RC_IS_OK(rc)) if (lpOctave != NULL) *lpOctave = octave;
  if (RC_IS_OK(rc)) if (lpValueLength != NULL) *lpValueLength = (p - lpInput);

  return(rc);
}

int ReadNote(char *lpInput, SETTINGS *set, NOTE *lpNote)
{
  int iState = 0, len = 0, oct = 0, i = 0;
  int chords[MAX_CHORDS_PER_NOTE];
  int chord_count = 0;
  char *lpPos = lpInput;
  INPUT_DEFAULTS *def = &set->defaults;

  lpNote->length = LENGTH_NONE;
  lpNote->pitch = PITCH_NONE;
  iState = STATE_LENGTH | STATE_FLAG_KEEP_POSITION;
  len = oct = chord_count = 0;
  memset(chords, 0, sizeof(int) * MAX_CHORDS_PER_NOTE);
  iState = STATE_LENGTH;
  while (*lpPos != '\0') {
    switch(iState) {
      case STATE_LENGTH:
        if ((*lpPos >= '0') && (*lpPos <= '9')) {
          len *= 10;
          len += (*lpPos - '0');
        }
        else if (*lpPos == '.') {
          lpNote->length |= LENGTH_PT;
        }
        /*else if (*lpPos == ':') {
          lpNote->length |= LENGTH_DPT;
        }*/
        else {
          iState = STATE_VALUE | STATE_FLAG_KEEP_POSITION;
          lpNote->pitch = 0;
        }
      break;
      case STATE_VALUE:
        if (*lpPos == '-') {
          lpNote->pitch -= 1;
        }
        else if (*lpPos == '+') {
          lpNote->pitch += 1;
        }
        else if ((*lpPos >= 'a') && (*lpPos <= 'h')) {
          lpNote->pitch += PITCHES[*lpPos - 'a'];
          iState = STATE_OCTAVE;
        }
        // else if (*lpPos == 'p') { // pause
        //   lpNote->value = VALUE_PAUSE;
        //   iState = STATE_END;
        // }
        else {
          iState = STATE_OCTAVE | STATE_FLAG_KEEP_POSITION;
        }
      break;
      case STATE_OCTAVE:
        if ((*lpPos >= '0') && (*lpPos <= '9')) {
          oct *= 10;
          oct += (*lpPos - '0');
        }
        else if ((*lpPos == 'A') || (*lpPos == 'C')) {
          iState = STATE_CHORD;
          chord_count = 1;
        }
        else {
          iState = STATE_END | STATE_FLAG_KEEP_POSITION;
        }
      break;
      case STATE_CHORD:
        if ((*lpPos >= '0') && (*lpPos <= '9')) {
          chords[chord_count - 1] *= 10;
          chords[chord_count - 1] += (*lpPos - '0');
        }
        else if (*lpPos == '.') {
          lpNote->chords[chord_count - 1] |= CHORD_PT;
        }
        else if (*lpPos == '_') {
          lpNote->chords[chord_count - 1] |= CHORD_SLR;
        }
        else if ((*lpPos == 'A') || (*lpPos == 'C')) {
          if (chord_count < MAX_CHORDS_PER_NOTE) {
            iState = STATE_CHORD;
            ++chord_count;
          }
          else {
            fprintf(stderr, "ERR: too many chords, input: %s\r\n", lpInput);
            return(RC_ERR);
          }
        }
        else {
          iState = STATE_END | STATE_FLAG_KEEP_POSITION;
        }
      break;
    }
    if (!(iState & STATE_FLAG_KEEP_POSITION)) {
      ++lpPos;
    }
    iState &= ~STATE_FLAG_KEEP_POSITION;
  }
  // apply length
  if (len == 0) {
    if (def->length == 0) {
      fprintf(stderr, "ERR: no length given, input: %s\r\n", lpInput);
      return(RC_ERR);
    }
    //printf("DBG: using default length (%i), input: %s\r\n", set->def.len, lpLine);
    lpNote->length |= def->length;
  }
  else {
    if ((len = GetLength(len)) == LENGTH_INVALID) {
      fprintf(stderr, "ERR: invalid length, input: %s\r\n", lpInput);
      return(RC_ERR);
    }
    lpNote->length |= len;
  }
  // apply octave
  if (oct == 0) {
    if ((def->octave == 0)/* && (def->octave_start == 0)*/) {
      fprintf(stderr, "ERR: no octave given, input: %s\r\n", lpInput);
      return(RC_ERR);
    }
    if (def->octave != 0) {
      //printf("DBG: using default octave (%i), input: %s\r\n", set->def.oct, lpLine);
      oct = def->octave;
    }
  }
  lpNote->pitch += (12 * (oct - 1));
  // apply chord
  if (chord_count > 0) {
    for (i = 0; i < chord_count; ++i) {
      if ((chords[i] >= 1) && (chords[i] <= (int)set->count.chords)) {
        lpNote->chords[i] |= chords[i];
      }
      else {
        fprintf(stderr, "WRN: ignoring invalid chord, input: %s\r\n", lpInput);
      }
    }
    lpNote->chord_count = chord_count;
  }
  // printf("DBG: input line '%s', len=%i, val=%i\r\n", lpLine, lpNote->length, lpNote->pitch);

  return(RC_OK);
}

int ReadSeparator(char *lpInput, SETTINGS *set, SEPARATOR *lpSep)
{
  int iState = 0;
  char *lpPos = lpInput;

  iState = STATE_SEP_SIZE;
  while (*lpPos != '\0') {
    switch(iState) {
      case STATE_SEP_SIZE:
        if ((*lpPos >= '0') && (*lpPos <= '9')) {
          lpSep->size *= 10;
          lpSep->size += (*lpPos - '0');
        }
        else {
          iState = STATE_SEP_STYLE | STATE_FLAG_KEEP_POSITION;
        }
      break;
      case STATE_SEP_STYLE:
        if (*lpPos == 's') {  // space
          lpSep->style = SEP_STYLE_SPACE;
          iState = STATE_END;
        }
        else if (*lpPos == 'd') { // dotted line
          lpSep->style = SEP_STYLE_DOTTED;
          iState = STATE_SEP_POS;
        }
        else if (*lpPos == 'l') { // (solid) line
          lpSep->style = SEP_STYLE_SOLID;
          iState = STATE_SEP_POS;
        }
        else {
          iState = STATE_END | STATE_FLAG_KEEP_POSITION;
        }
      break;
      case STATE_SEP_POS:
        if ((*lpPos >= '0') && (*lpPos <= '9')) {
          lpSep->line_pos *= 10;
          lpSep->line_pos += (*lpPos - '0');
        }
        else {
          iState = STATE_END | STATE_FLAG_KEEP_POSITION;
        }
      break;
    }
    if (!(iState & STATE_FLAG_KEEP_POSITION)) {
      ++lpPos;
    }
    iState &= ~STATE_FLAG_KEEP_POSITION;
  }
  // set separator defaults
  if (lpSep->size == 0) lpSep->size = 1;
  if (lpSep->style != SEP_STYLE_SPACE) {
    if (lpSep->size < 2) lpSep->size = 2;
    if (lpSep->line_pos == 0) lpSep->line_pos = 1;
    else if (lpSep->line_pos >= lpSep->size) lpSep->line_pos = lpSep->size - 1;
  }
  // printf("DBG: input line '%s', len=%i, val=%i\r\n", lpLine, lpNote->length, lpNote->pitch);

  return(RC_OK);
}

int ReadMelodyItem(char *lpInput, SETTINGS *set, MELODY_ITEM *lpItem)
{
  if ((lpInput == NULL) || (set == NULL) || (lpItem == NULL)) return(RC_ERR);

  // printf("DBG: reading item[%03i] from input '%s'\r\n", lpItem->base.idx, lpInput);
  switch (*lpInput) {
    case 'N':
      lpItem->base.type = MI_TYPE_NOTE;
      return(ReadNote(lpInput + 1, set, &lpItem->note));
    break;
    case 'P':
      lpItem->base.type = MI_TYPE_NOTE;
      lpItem->note.is_pause = true;
      return(ReadNote(lpInput + 1, set, &lpItem->note));
    break;
    case 'S':
      lpItem->base.type = MI_TYPE_SEPARATOR;
      return(ReadSeparator(lpInput + 1, set, &lpItem->sep));
    break;
    case 'M':
      // handled before
      return(RC_ERR);
    break;
    default:
      lpItem->base.type = MI_TYPE_NOTE;
      return(ReadNote(lpInput, set, &lpItem->note));
    break;
  }

  return(RC_OK);
}

int ReadNoteGroup(INPUT_ITEMS *lpItems, SETTINGS *set, NOTE_GROUP *grp)
{
  int rc = RC_OK;
  for (unsigned int i = 0; (RC_IS_OK(rc)) && (i < lpItems->item_count); ++i) {
    if (grp->note_count >= MAX_NOTES_PER_GROUP) {
      printf("ERR: too many notes per group\r\n");
      return(RC_ERR);
    }
    NOTE *note = &grp->notes[grp->note_count];
    note->base.idx = grp->note_count + 1;
    switch (lpItems->items[i][0]) {
      case 'N':
        if (RC_IS_OK(rc)) rc = ReadNote(lpItems->items[i] + 1, set, note);
        if (RC_IS_OK(rc)) ++grp->note_count;
      break;
      case 'P':
        if (RC_IS_OK(rc)) note->is_pause = true;
        if (RC_IS_OK(rc)) rc = ReadNote(lpItems->items[i] + 1, set, note);
        if (RC_IS_OK(rc)) ++grp->note_count;
      break;
      default:
        if (RC_IS_OK(rc)) rc = ReadNote(lpItems->items[i], set, note);
        if (RC_IS_OK(rc)) ++grp->note_count;
      break;
    }
  }
  if (RC_IS_OK(rc)) {
    if (grp->note_count < 2) {
      printf("ERR: too few notes in group\r\n");
      return(RC_ERR);
    }
    int pitch = grp->notes[0].pitch;
    if (pitch == PITCH_NONE) return(RC_ERR);
    for (unsigned int i = 0; i < grp->note_count; ++i) {
      if (grp->notes[i].pitch != pitch) {
        if (grp->notes[i].is_pause) grp->notes[i].pitch = pitch;
        else { printf("ERR: pitch mismatch inside group\r\n"); return(RC_ERR); }
      }
      if (grp->notes[i].chord_count > 1) {
        printf("ERR: too many chords at grouped note\r\n");
        return(RC_ERR);
      }
    }
  }
  return(rc);
}

int ReadMarker(char *lpInput, SESSION *s)
{
  int iState = 0;
  unsigned int mrk_id = 0;
  char *lpPos = lpInput;

  if (*lpPos != 'M') return(RC_ERR);
  ++lpPos;
  iState = STATE_MRK_ID;
  while (*lpPos != '\0') {
    switch(iState) {
      case STATE_MRK_ID:
        if ((*lpPos >= '0') && (*lpPos <= '9')) {
          mrk_id *= 10;
          mrk_id += (*lpPos - '0');
        }
        else {
          iState = STATE_END;
        }
      break;
    }
    ++lpPos;
  }
  // set marker defaults
  // check marker values
  if ((mrk_id == 0) || (mrk_id > MAX_MARKERS)) {
    printf("ERR: invalid marker ID %u", mrk_id);
    return(RC_ERR);
  }
  MARKER *mrk = &s->markers[mrk_id - 1];
  if (IsMarkerDefined(mrk)) {
    fprintf(stdout, "WRN: re-defining marker ID %u", mrk_id);
  }
  memset(&mrk->pos, 0, sizeof(POSITION));
  mrk->pos.ref_mi_idx = s->song.melody.item_count + 1;
  printf("DBG: marker[%03u] references melody item [%03i]\r\n", mrk->id, mrk->pos.ref_mi_idx);
  // printf("DBG: input line '%s', len=%i, val=%i\r\n", lpLine, lpNote->length, lpNote->pitch);

  return(RC_OK);
}

int ReadMelodyItems(INPUT_ITEMS *lpItems, MELODY *melody, SESSION *s)
{
  if ((lpItems == NULL) || (melody == NULL) || (s == NULL)) return(RC_ERR);
  int rc = RC_OK;
  bool in_group = false;
  unsigned char group_type = GRP_TYPE_NONE;
  INPUT_ITEMS grp_items;

  // memset(&grp_items, 0, sizeof(INPUT_ITEMS));
  for (unsigned int i = 0; (RC_IS_OK(rc)) && (i < lpItems->item_count); ++i) {
    if (melody->item_count >= MAX_MELODY_ITEMS) {
      printf("ERR: too many melody items\r\n");
      rc = RC_ERR;
      break;
    }
    MELODY_ITEM *mitem = &melody->items[melody->item_count];
    if (strcmp(lpItems->items[i], "{") == 0) {
      if (in_group) {
        printf("ERR: group nesting is not allowed\r\n");
        rc = RC_ERR;
        break;
      }
      memset(&grp_items, 0, sizeof(INPUT_ITEMS));
      in_group = true;
    }
    else if (strcmp(lpItems->items[i], "}") == 0) {
      if (!in_group) {
        printf("ERR: group end without group start\r\n");
        rc = RC_ERR;
        break;
      }
      else if (group_type != GRP_TYPE_NONE) {
        printf("ERR: mismatching group start and group end\r\n");
        rc = RC_ERR;
        break;
      }
      mitem->base.type = MI_TYPE_GROUP;
      mitem->grp.type = group_type;
      if (RC_IS_OK(rc)) rc = ReadNoteGroup(&grp_items, &s->set, &mitem->grp);
      if (RC_IS_OK(rc)) ++melody->item_count;
      in_group = false;
      group_type = GRP_TYPE_NONE;
    }
    else if (strcmp(lpItems->items[i], "(") == 0) {
      if (in_group) {
        printf("ERR: group nesting is not allowed\r\n");
        rc = RC_ERR;
        break;
      }
      memset(&grp_items, 0, sizeof(INPUT_ITEMS));
      in_group = true;
      group_type = GRP_TYPE_TIE;
    }
    else if (strcmp(lpItems->items[i], ")") == 0) {
      if (!in_group) {
        printf("ERR: group end without group start\r\n");
        rc = RC_ERR;
        break;
      }
      else if (group_type != GRP_TYPE_TIE) {
        printf("ERR: mismatching group start and group end\r\n");
        rc = RC_ERR;
        break;
      }
      mitem->base.type = MI_TYPE_GROUP;
      mitem->grp.type = group_type;
      if (RC_IS_OK(rc)) rc = ReadNoteGroup(&grp_items, &s->set, &mitem->grp);
      if (RC_IS_OK(rc)) ++melody->item_count;
      in_group = false;
      group_type = GRP_TYPE_NONE;
    }
    else if (in_group) {
      grp_items.items[grp_items.item_count] = lpItems->items[i];
      ++grp_items.item_count;
    }
    else if (lpItems->items[i][0] == 'M') {
      if (RC_IS_OK(rc)) rc = ReadMarker(lpItems->items[i], s);
    }
    else {
      if (RC_IS_OK(rc)) rc = ReadMelodyItem(lpItems->items[i], &s->set, mitem);
      if (RC_IS_OK(rc)) ++melody->item_count;
    }
  }
  if (RC_IS_OK(rc)) {
    for (unsigned int mrk_idx = 0; mrk_idx < MAX_MARKERS; ++mrk_idx) {
      MARKER *mrk = &s->markers[mrk_idx];
      if (!IsMarkerDefined(mrk)) continue;
      if (mrk->pos.ref_mi_idx == 0) continue;
      if (mrk->pos.ref_mi_idx > s->song.melody.item_count) {
        printf("ERR: marker ID %u not referencing note or similar!", mrk->id);
        if (RC_IS_OK(rc)) rc = RC_ERR;
      }
      else {
        MELODY_ITEM *mi = &s->song.melody.items[mrk->pos.ref_mi_idx - 1];
        switch (mi->base.type) {
          case MI_TYPE_NOTE:
            // nothing to check
          break;
          case MI_TYPE_SEPARATOR:
            // nothing to check
          break;
          case MI_TYPE_GROUP:
            if (mi->grp.note_count == 0) {
              printf("ERR: marker ID %u referencing empty note group!", mrk->id);
              if (RC_IS_OK(rc)) rc = RC_ERR;
            }
          break;
        }
      }
    }
    if (RC_IS_ERR(rc)) return(rc);
  }

  return(rc);
}


//*** reading input file ***

int ClearInputItems(INPUT_ITEMS *lpItems)
{
  if (lpItems == NULL) return(RC_ERR);
  if (lpItems->line_copy != NULL) free(lpItems->line_copy);
  memset(lpItems, 0, sizeof(INPUT_ITEMS));
  return(RC_OK);
}

int ParseInputItems(const char *lpLine, INPUT_ITEMS *lpItems, SESSION *s)
{
  if ((lpLine == NULL) || (lpItems == NULL)) return(RC_ERR);
  ClearInputItems(lpItems);
  lpItems->line_copy = (char*)malloc(strlen(lpLine) + 1);
  memset(lpItems->line_copy, 0, strlen(lpLine) + 1);
  strcpy(lpItems->line_copy, lpLine);
  char *pos = lpItems->line_copy;
  bool in_item = false;
  while (*pos != '\0') {
    switch (*pos) {
      case ' ':
      case '\t':
        in_item = false;
        *pos = '\0';
        ++pos;
      break;
      // break at beginning of comment
      case ';':
        *pos = '\0';
        // stay here to stop reading input
      break;
      default:
        if (!in_item) {
          lpItems->items[lpItems->item_count] = pos;
          ++lpItems->item_count;
        }
        in_item = true;
        ++pos;
      break;
    }
  }
  if (s->uLogMode >= LOG_MODE_VERBOSE) {
    fprintf(stdout, "DBG: parsed %u input items in line '%s'\r\n", lpItems->item_count, lpLine);
  }
  return(RC_OK);
}

int ReadInputFile(FILE *lpFile, const char *lpFileName, SESSION *s)
{
  int rc = RC_OK;
  unsigned int line = 0;

  while (RC_IS_OK(rc)) {
    char lpLine[MAX_INPUT_LENGTH];
    char *pos;
    INPUT_ITEMS iitems;
    memset(lpLine, '\0', MAX_INPUT_LENGTH);
    memset(&iitems, 0, sizeof(INPUT_ITEMS));
    if (fgets(lpLine, MAX_INPUT_LENGTH, lpFile) == NULL) break;
    ++line;
    while ((lpLine[strlen(lpLine)-1] == '\r') || (lpLine[strlen(lpLine)-1] == '\n')) {
      lpLine[strlen(lpLine)-1] = '\0';
    }
    //printf("DBG: input line '%s'\r\n", line);
    pos = lpLine;
    while ((*pos == ' ') || (*pos == '\t')) {
      ++pos;
    }
    if (*pos == ';') continue;  // ignore comment line
    if (*pos == '#') {
      char *text = strchr(pos, ' ');
      if (text != NULL) { *text = '\0'; ++text; }
      if (strcmp(pos, "#warning") == 0) {
        if (text != NULL) {
          fprintf(stdout, "%s:%u: warning: %s\r\n", lpFileName, line, text);
        }
        else {
          fprintf(stdout, "%s:%u: warning in input file\r\n", lpFileName, line);
        }
      }
      else if (strcmp(pos, "#error") == 0) {
        if (text != NULL) {
          fprintf(stderr, "%s:%u: error: %s\r\n", lpFileName, line, text);
        }
        else {
          fprintf(stderr, "%s:%u: error in input file\r\n", lpFileName, line);
        }
        return(RC_ERR);
      }
      else {
        return(RC_ERR);
      }
      continue; // do not further process this line
    }
    if (*pos != '@') {  // remove comments, ';' only allowed (as part of values) in @ lines
      char *pos_semi = strchr(pos, ';');
      if (pos_semi != NULL) *pos_semi = '\0';
    }
    if (*pos == '\0') continue; // ignore empty line
    if (STR_IS(lpLine, "\\end")) return(RC_OK);
    switch(*pos) {
      case '\\':
        ++pos;
        if (RC_IS_OK(rc)) rc = ParseInputItems(pos, &iitems, s);
        if (RC_IS_OK(rc)) rc = ReadFieldValues(&iitems, lpFileName, s);
      break;
      case '@':
        ++pos;
        if (s->uLogMode >= LOG_MODE_VERBOSE) {
          fprintf(stdout, "DBG: reading field value from line '%s'\r\n", pos);
        }
        if (RC_IS_OK(rc)) rc = ReadFieldValue(pos, lpFileName, s);
      break;
      default:
        if (RC_IS_OK(rc)) rc = ParseInputItems(pos, &iitems, s);
        if (RC_IS_OK(rc)) rc = ReadMelodyItems(&iitems, &s->song.melody, s);
      break;
    }
    ClearInputItems(&iitems);
  }

  return(rc);
}


//*** apply command line settings **********************************************

int ApplyCommandLineSettings(SESSION *s)
{
  for (unsigned int i = 0; i < s->set.cmd_line.count; ++i) {
    ReadFieldValue(s->set.cmd_line.items[i], (const char*)NULL, s);
  }

  return(RC_OK);
}


//*** transpose ****************************************************************

int Transpose(SESSION *s)
{
  if (s->song.transpose != 0) {
    if (s->uLogMode != LOG_MODE_QUIET) {
      fprintf(stdout, "INF: transposing song by %i half steps\r\n", s->song.transpose);
    }
    bool overflow = false;
    bool underflow = false;
    bool removeChords = (s->song.transpose % 12) != 0;
    bool removedChords = false;
    for (unsigned int mi_idx = 0; mi_idx < s->song.melody.item_count; ++mi_idx) {
      MELODY_ITEM *item = &s->song.melody.items[mi_idx];
      NOTE *note = (NOTE*)NULL;
      switch (item->base.type) {
        case MI_TYPE_NOTE:
          note = &item->note;
        break;
        case MI_TYPE_SEPARATOR:
          continue;
        break;
        case MI_TYPE_GROUP:
          if (item->grp.note_count == 0) return(RC_ERR);
          note = &item->grp.notes[0];
        break;
        default: return(RC_ERR);
      }
      if (note == NULL) continue;
      if (note->chord_count > 0) removedChords |= removeChords;
      if (removeChords) {
        switch (item->base.type) {
          case MI_TYPE_NOTE:
            note->chord_count = 0;
          break;
          case MI_TYPE_SEPARATOR:
            // nothing to do
          break;
          case MI_TYPE_GROUP:
            for (unsigned int i = 0; i < item->grp.note_count; ++i) item->grp.notes[i].chord_count = 0;
          break;
          default: return(RC_ERR);
        }
      }
      int new_pitch = note->pitch;
      new_pitch += s->song.transpose;
      while (new_pitch > MAX_PITCH) {
        // fprintf(stderr, "DBG: overflow on note[%u]: %i (%u + %i) > %u\r\n", nidx, new_value, note->value, s->song.transpose, s->set.count.cols);
        overflow = true;
        new_pitch -= 12;  // go down one octave
      }
      while (new_pitch < 0) {
        // fprintf(stderr, "DBG: underflow on note[%u]: %i (%u + %i) <= 0\r\n", nidx, new_value, note->value, s->song.transpose);
        underflow = true;
        new_pitch += 12;  // go up one octave
      }
      switch (item->base.type) {
        case MI_TYPE_NOTE:
          note->pitch = new_pitch;
        break;
        case MI_TYPE_SEPARATOR:
          // nothing to do
        break;
        case MI_TYPE_GROUP:
          for (unsigned int i = 0; i < item->grp.note_count; ++i) item->grp.notes[i].pitch = new_pitch;
        break;
        default: return(RC_ERR);
      }
    }
    if (removedChords) fprintf(stderr, "WRN: removed chords while transposing song\r\n");
    if (underflow) fprintf(stderr, "WRN: underflow occured while transposing song\r\n");
    if (overflow) fprintf(stderr, "WRN: overflow occured while transposing song\r\n");
    s->song.transpose = 0;
  }
  return(RC_OK);
}