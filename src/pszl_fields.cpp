/*
  This file is part of PS ZitherLayout.

  PS ZitherLayout is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  PS ZitherLayout is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with PS ZitherLayout.  If not, see <http://www.gnu.org/licenses/>.


  Diese Datei ist Teil von PS ZitherLayout.

  PS ZitherLayout ist Freie Software: Sie können es unter den Bedingungen
  der GNU General Public License, wie von der Free Software Foundation,
  Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren
  veröffentlichten Version, weiterverbreiten und/oder modifizieren.

  PS ZitherLayout wird in der Hoffnung, dass es nützlich sein wird, aber
  OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
  Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
  Siehe die GNU General Public License für weitere Details.

  Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
  Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
*/

/*
**  © 2011-2023 Carsten Pfeffer (PiperSoft)
**
**  @project: PS ZitherLayout
**  @file: pszl_fields.cpp
**  @brief implementation of field handling module
**
**  @date: 2022-11-29
**  @author: Carsten Pfeffer (nee Gaede)
*/


#include "pszl_fields.h"

#define RC_ERR_BASE RC_ERR_BASE_FIELDS

#include <ctype.h>
#include <string.h>
#include <stdlib.h>

#include "pszl_input.h"

typedef int (*FVGetter) (struct SESSION *session, struct FIELD_VALUE_REF *lpFVRef, struct FIELD_VALUE *lpValue);
typedef int (*FVSetter) (struct SESSION *session, struct FIELD_VALUE_REF *lpFVRef, struct FIELD_VALUE *lpValue, unsigned int uOperator);

struct FIELD {
  unsigned int id;              // field ID, @see FID_
  const char *short_name;
  bool short_name_ignore_case;  // ignore case when checking for short name match
  const char *long_name;
  unsigned int max_name_idx;
  unsigned char value_type;
  FVGetter getter;
  FVSetter setter;
  void *pvalue;
  union {
    struct {
      int max_length;
    } _str;
    struct {
      int min_value;
      int max_value;
    } _int;
    struct {
      int max_index;
    } _imask;
    struct {
      int max_items;
    } _p2pmap;
    struct {
      int max_items;
    } _plist;
  };
};

struct FIELD_STORE {
  struct {
    POSITION *m[MAX_MARKERS];   // m<x> | marker-<x>
    char *l[MAX_LYRICS];        // l<x> | lyrics-<x>
    POSITION *lp[MAX_LYRICS];   // lp<x> | lyrics-position-<x>
    bool *lv[MAX_LYRICS];       // lv<x> | lyrics-visible-<x>
  } tmp;
  FIELD fields[MAX_FIELDS];
};


struct NAMED_VALUE {
  unsigned int type;      // field value type, @see FVT_..
  const char *short_name;
  const char *long_name;
  unsigned int value;
} named_values[] = {
  // layout style
  { FVT_STY, "m", "melody", LAYOUT_STYLE_MELODY },
  { FVT_STY, "b", "bass", LAYOUT_STYLE_BASS },
  // positioning format
  { FVT_POSFMT, "cr", "col-row", POSFMT_CR },
  { FVT_POSFMT, "crf", "col-row-fine", POSFMT_CR | POSMOD_FINE },
  { FVT_POSFMT, "cra", "col-row-absolute", POSFMT_CR_ABS },
  { FVT_POSFMT, "craf", "col-row-absolute-fine", POSFMT_CR_ABS | POSMOD_FINE },
  { FVT_POSFMT, "xy", 0, POSFMT_XY },
  { FVT_POSFMT, "xyf", "xy-fine", POSFMT_XY | POSMOD_FINE },
  // notes positiong
  { FVT_NOTEPOS, "t", "top", NOTES_POS_TOP },
  { FVT_NOTEPOS, "c", "center", NOTES_POS_CENTER },
  { FVT_NOTEPOS, "b", "bottom", NOTES_POS_BOTTOM },
  // highlight mode
  { FVT_HLMODE, "n", "none", HLM_NONE },
  { FVT_HLMODE, "c", "color", HLM_COLOR },
  { FVT_HLMODE, "l", "line", HLM_LINE },
  { FVT_HLMODE, "cl", "color-and-line", HLM_COLOR | HLM_LINE },
  //
  { 0 }
};



int ParseIntValue(SESSION *session, const char *lpInput, bool bAllowSuffix, int *lpValue, unsigned int *lpValueLength);


//*** get field and field value refence ***

int strcmp_ic(const char* s1, const char* s2, bool ignore_case)
{
  if (!ignore_case) return(strcmp(s1, s2));
  if (strlen(s1) != strlen(s2)) return -1;
  for (unsigned int i = 0; i < strlen(s1); ++i) {
    if (toupper(s1[i]) != toupper(s2[i])) return(s1[i] - s2[i]);
  }
  return(0);
}

int strncmp_ic(const char* s1, const char* s2, unsigned int cmp_len, bool ignore_case)
{
  if (!ignore_case) return(strncmp(s1, s2, cmp_len));
  for (unsigned int i = 0; i < cmp_len; ++i) {
    if (toupper(s1[i]) != toupper(s2[i])) return(s1[i] - s2[i]);
    if ((s1[i] == '\0') && (s2[i] == '\0')) return(0);
  }
  return(0);
}

#define STR_IS_IC(str, cmp, ic) (strcmp_ic(str, cmp, ic) == 0)
#define STR_STARTS_WITH_IC(str, pre, ic) (strncmp_ic(str, pre, strlen(pre), ic) == 0)

int GetField(SESSION *session, const char *lpFieldName, FIELD **lpField, unsigned int *lpFieldIndex)
{
  *lpField = (FIELD*)NULL;
  *lpFieldIndex = 0;
  for (unsigned int i = 0; i < MAX_FIELDS; ++i) {
    FIELD *tmpf = &session->field_store->fields[i];
    if (tmpf->max_name_idx == 0) {
      if (tmpf->short_name != NULL) if (STR_IS_IC(lpFieldName, tmpf->short_name, tmpf->short_name_ignore_case)) { *lpField = tmpf; return(RC_OK); }
      if (tmpf->long_name != NULL) if (STR_IS(lpFieldName, tmpf->long_name)) { *lpField = tmpf; return(RC_OK); }
    }
    else {
      bool idx_ok = false;
      int idx = 0;
      if (tmpf->short_name != NULL) if (STR_STARTS_WITH_IC(lpFieldName, tmpf->short_name, tmpf->short_name_ignore_case)) {
        idx_ok = (ParseIntValue(session, lpFieldName + strlen(tmpf->short_name), false, &idx, NULL) == RC_OK);
      }
      if (tmpf->long_name != NULL) if (STR_STARTS_WITH(lpFieldName, tmpf->long_name)) {
        idx_ok = lpFieldName[strlen(tmpf->long_name)] == '-';
        idx_ok &= (ParseIntValue(session, lpFieldName + strlen(tmpf->long_name) + 1, false, &idx, NULL) == RC_OK);
      }
      if (idx_ok) {
        if ((idx < 1) || (idx > tmpf->max_name_idx)) idx_ok = false;
      }
      if (idx_ok) {
        *lpField = tmpf;
        *lpFieldIndex = (unsigned int)idx;
        return(RC_OK);
      }
    }
  }
  return(RC_ERR);
}

int GetFieldValueRef(SESSION *session, const char *lpFieldName, FIELD_VALUE_REF *lpFVRef)
{
  FIELD *lpField = (FIELD*)NULL;
  unsigned int uFieldIndex = 0;
  memset(lpFVRef, 0, sizeof(FIELD_VALUE_REF));
  int rc = GetField(session, lpFieldName, &lpField, &uFieldIndex);
  if (RC_IS_ERR(rc)) return(rc);
  lpFVRef->id = lpField->id;
  lpFVRef->index = uFieldIndex;
  lpFVRef->value_type = lpField->value_type;
  return(RC_OK);
}


//*** parse field value ***
//
//  #######     ####    #######    ######   ########
//  ##    ##   ##  ##   ##    ##  ##    ##  ##
//  ##    ##  ##    ##  ##    ##  ##        ##
//  #######   ##    ##  #######    ######   #######
//  ##        ########  ##  ##          ##  ##
//  ##        ##    ##  ##   ##   ##    ##  ##
//  ##        ##    ##  ##    ##   ######   ########
//

int ResolveFieldRef(SESSION *session, const char *lpInput, bool bAllowSuffix, FIELD_VALUE_REF *lpFVRef, FIELD_VALUE *lpFValue, unsigned int *lpFieldRefLength)
{
  if ((*lpInput != '@') && (*lpInput != '^')) return(RC_ERR);
  int rc = RC_OK;
  memset(lpFVRef, 0, sizeof(FIELD_VALUE_REF));
  memset(lpFValue, 0, sizeof(FIELD_VALUE));
  if (lpFieldRefLength != NULL) *lpFieldRefLength = 0;
  char copy[MAX_INPUT_LENGTH];
  strncpy(copy, lpInput, MAX_INPUT_LENGTH);
  char *p = copy;
  char *sep = strchr(p, ',');
  if (bAllowSuffix && (sep != NULL)) *sep = '\0';
  ++p;  // skip @ or ^ at the beginning
  if (RC_IS_OK(rc)) rc = GetFieldValueRef(session, p, lpFVRef);
  if (RC_IS_OK(rc)) rc = GetFieldValue(session, lpFVRef, lpFValue);
  if (RC_IS_OK(rc)) if (lpFieldRefLength != NULL) *lpFieldRefLength = strlen(copy);
  return(rc);
}

int ParseFieldNamedValue(SESSION *session, FIELD_VALUE_REF *lpFVRef, const char *lpInput, FIELD_VALUE *lpFValue)
{
  bool found_value_type = false;
  for (unsigned int nvi = 0; named_values[nvi].type != 0; ++nvi) {
    NAMED_VALUE *nv = &named_values[nvi];
    if (nv->type != lpFVRef->value_type) continue;
    found_value_type = true;
    if (nv->short_name != NULL) if (STR_IS(lpInput, nv->short_name)) {
      lpFValue->type = lpFVRef->value_type;
      lpFValue->ival = nv->value;
      return(RC_OK);
    }
    if (nv->long_name != NULL) if (STR_IS(lpInput, nv->long_name)) {
      lpFValue->type = lpFVRef->value_type;
      lpFValue->ival = nv->value;
      return(RC_OK);
    }
  }
  if (found_value_type) return(RC_ERR);
  return(RC_WARN);
}

int ParseStrValue(SESSION *session, const char *lpInput, bool bAllowFRef, char *lpValue, unsigned int uMaxLength)
{
  int rc = RC_OK;
  if (*lpInput == '^') {
    FIELD_VALUE_REF fvref = {0};
    FIELD_VALUE fval = {0};
    if (RC_IS_OK(rc)) rc = ResolveFieldRef(session, lpInput, false, &fvref, &fval, 0);
    if (RC_IS_OK(rc)) {
      if ((fval.type == FVT_STR) || (fval.type == FVT_POS_STR)) {
        strncpy(lpValue, fval.sval, MAX_INPUT_LENGTH);
      }
      else rc = RC_ERR;
    }
  }
  else {
    strncpy(lpValue, lpInput, MAX_INPUT_LENGTH);
  }
  return(rc);
}

int ParseFieldStrValue(SESSION *session, const char *lpInput, FIELD_VALUE *lpFValue)
{
  int rc = RC_OK;
  memset(lpFValue, 0, sizeof(FIELD_VALUE));
  if (RC_IS_OK(rc)) rc = ParseStrValue(session, lpInput, true, lpFValue->sval, MAX_INPUT_LENGTH);
  if (RC_IS_OK(rc)) lpFValue->type = FVT_STR;
  return(rc);
}

int ParseIntValue(SESSION *session, const char *lpInput, bool bAllowSuffix, int *lpValue, unsigned int *lpValueLength)
{
  bool neg = false;
  const char *p = lpInput;
  int v = 0;
  *lpValue = -1;
  if (lpValueLength != NULL) *lpValueLength = 0;
  if ((*p != '-') && (*p != '+') && ((*p < '0') || (*p > '9'))) return(RC_ERR);
  if (*p == '-') {
    neg = true;
    ++p;
  }
  else if (*p == '+') {
    ++p;
  }
  while ((*p >= '0') && (*p <= '9')) {
    v *= 10;
    v += (*p - '0');
    ++p;
  }
  if (neg) v *= -1;
  if (!bAllowSuffix) if (*p != '\0') return(RC_ERR);
  *lpValue = v;
  if (lpValueLength != NULL) *lpValueLength = (p - lpInput);
  return(RC_OK);
}

int ParsePosValue(SESSION *session, const char *lpInput, bool bAllowFRefs, bool bAllowSuffix, POSITION *lpValue, unsigned int *lpValueLength)
{
  int rc = RC_OK;
  const char *p = lpInput;
  unsigned int vlen = 0;
  if (RC_IS_OK(rc)) {
    if (*p == '^') {
      FIELD_VALUE_REF fvref = {0};
      FIELD_VALUE fval = {0};
      if (RC_IS_OK(rc)) rc = ResolveFieldRef(session, p, true, &fvref, &fval, &vlen);
      if (RC_IS_OK(rc)) {
        if (fvref.id == FID_Mx) {
          lpValue->lcol.ref_mrk_id = fvref.index;
        }
        else if ((fval.type == FVT_POS) || (fval.type == FVT_POS_STR)) {
          memcpy(&lpValue->lcol, &fval.pval.lcol, sizeof(POSITION_FIELD));
        }
        else rc = RC_ERR;
      }
      if (RC_IS_OK(rc)) p += vlen;
    }
    else {
      if (RC_IS_OK(rc)) rc = ParseIntValue(session, p, true, (int*)&lpValue->lcol.value, &vlen);
      if (RC_IS_OK(rc)) p += vlen;
    }
  }
  if (RC_IS_OK(rc)) if (*p != ',') rc = RC_ERR;
  if (RC_IS_OK(rc)) ++p;
  if (RC_IS_OK(rc)) {
    if (*p == '^') {
      FIELD_VALUE_REF fvref = {0};
      FIELD_VALUE fval = {0};
      if (RC_IS_OK(rc)) rc = ResolveFieldRef(session, p, true, &fvref, &fval, &vlen);
      if (RC_IS_OK(rc)) {
        if (fvref.id == FID_Mx) {
          lpValue->lrow.ref_mrk_id = fvref.index;
        }
        else if ((fval.type == FVT_POS) || (fval.type == FVT_POS_STR)) {
          memcpy(&lpValue->lrow, &fval.pval.lrow, sizeof(POSITION_FIELD));
        }
        else rc = RC_ERR;
      }
      if (RC_IS_OK(rc)) p += vlen;
    }
    else {
      if (RC_IS_OK(rc)) rc = ParseIntValue(session, p, bAllowSuffix, (int*)&lpValue->lrow.value, &vlen);
      if (RC_IS_OK(rc)) p += vlen;
    }
  }
  if (RC_IS_OK(rc)) if (!bAllowSuffix) if (*p != '\0') rc = RC_ERR;
  if (RC_IS_OK(rc)) if (lpValueLength != NULL) *lpValueLength = (p - lpInput);
  return(rc);
}

int ParseFieldIntValue(SESSION *session, const char *lpInput, FIELD_VALUE *lpFValue)
{
  int rc = RC_OK;
  memset(lpFValue, 0, sizeof(FIELD_VALUE));
  rc = ParseIntValue(session, lpInput, false, &lpFValue->ival, 0);
  if (RC_IS_OK(rc)) lpFValue->type = FVT_INT;
  return(rc);
}

int ParseFieldBoolValue(SESSION *session, const char *lpInput, FIELD_VALUE *lpFValue)
{
  int rc = RC_OK;
  rc = ParseIntValue(session, lpInput, false, &lpFValue->ival, 0);
  if (RC_IS_OK(rc)) lpFValue->bval = (lpFValue->ival > 0);
  else if (STR_IS_ONE_OF_THREE(lpInput, "true", "True", "TRUE")) {
    lpFValue->bval = true;
    rc = RC_OK;
  }
  else if (STR_IS_ONE_OF_THREE(lpInput, "false", "False", "FALSE")) {
    lpFValue->bval = false;
    rc = RC_OK;
  }
  else rc = RC_ERR;
  if (RC_IS_OK(rc)) lpFValue->type = FVT_BOOL;
  return(rc);
}

int ParseFieldPosValue(SESSION *session, const char *lpInput, FIELD_VALUE *lpFValue)
{
  int rc = RC_OK;
  rc = ParsePosValue(session, lpInput, true, false, &lpFValue->pval, 0);
  if (RC_IS_OK(rc)) lpFValue->type = FVT_POS;
  return(rc);
}

int ParseFieldPosStrValue(SESSION *session, const char *lpInput, FIELD_VALUE *lpFValue)
{
  int rc = RC_OK;
  const char *p = lpInput;
  unsigned int vlen = 0;
  if (RC_IS_OK(rc)) rc = ParsePosValue(session, p, true, true, &lpFValue->pval, &vlen);
  if (RC_IS_OK(rc)) p += vlen;
  if (RC_IS_OK(rc)) if (*p != ',') rc = RC_ERR;
  if (RC_IS_OK(rc)) ++p;
  if (RC_IS_OK(rc)) rc = ParseStrValue(session, p, true, lpFValue->sval, MAX_INPUT_LENGTH);
  if (RC_IS_OK(rc)) lpFValue->type = FVT_POS_STR;
  return(rc);
}

int ParseFieldNoteLenValue(SESSION *session, const char *lpInput, FIELD_VALUE *lpFValue)
{
  int rc = RC_OK;
  rc = ParseIntValue(session, lpInput, false, &lpFValue->ival, 0);
  if (RC_IS_OK(rc)) {
    lpFValue->ival = GetLength(lpFValue->ival);
    if (lpFValue->ival == LENGTH_INVALID) rc = RC_ERR;
  }
  if (RC_IS_OK(rc)) lpFValue->type = FVT_NOTELEN;
  return(rc);
}

int ParseFieldIMaskValue(SESSION *session, FIELD_VALUE_REF *lpFVRef, const char *lpInput, FIELD_VALUE *lpFValue)
{
  if ((lpFVRef->id == 0) || (lpFVRef->id >= MAX_FIELDS)) return(RC_ERR);
  FIELD *field = &session->field_store->fields[lpFVRef->id];
  if (field->_imask.max_index > 32) return(RC_ERR);
  int rc = RC_OK;
  const char *p = lpInput;
  int ival = 0;
  unsigned int vlen = 0;
  while (RC_IS_OK(rc)) {
    if (RC_IS_OK(rc)) rc = ParseIntValue(session, p, true, &ival, &vlen);
    if (RC_IS_OK(rc)) if ((ival <= 0) || (ival > field->_imask.max_index)) rc = RC_ERR;
    if (RC_IS_OK(rc)) lpFValue->ilval |= 0x01 << (ival - 1);
    if (RC_IS_OK(rc)) p += vlen;
    if (RC_IS_OK(rc)) if (*p == '\0') break;
    if (RC_IS_OK(rc)) if (*p != ',') rc = RC_ERR;
    if (RC_IS_OK(rc)) ++p;
  }
  if (RC_IS_OK(rc)) lpFValue->type = FVT_IMASK;
  return(rc);
}

int ParseP2PValue(SESSION *session, const char *lpInput, bool bAllowSuffix, FIELD_P2PMAP_ITEM *lpValue, unsigned int *lpValueLength)
{
  int rc = RC_OK;
  const char *p = lpInput;
  unsigned int vlen = 0;
  lpValue->key = PITCH_NONE;
  lpValue->value = PITCH_NONE;
  if (RC_IS_OK(rc)) rc = ParsePitch(session, p, true, &lpValue->key, 0, &vlen);
  if (RC_IS_OK(rc)) p += vlen;
  if (RC_IS_OK(rc)) if (*p == ':') {
    if (RC_IS_OK(rc)) ++p;
    if (RC_IS_OK(rc)) {
      if (STR_STARTS_WITH(p, "none")) {
        lpValue->value = PITCH_NONE;
        if (RC_IS_OK(rc)) p += 4;
      }
      else {
        if (RC_IS_OK(rc)) rc = ParsePitch(session, p, bAllowSuffix, &lpValue->value, 0, &vlen);
        if (RC_IS_OK(rc)) p += vlen;
      }
    }
  }
  if (RC_IS_OK(rc)) if (!bAllowSuffix) if (*p != '\0') rc = RC_ERR;
  if (RC_IS_OK(rc)) if (lpValueLength != NULL) *lpValueLength = (p - lpInput);
  return(rc);
}

int ParseFieldP2PMapValue(SESSION *session, FIELD_VALUE_REF *lpFVRef, const char *lpInput, FIELD_VALUE *lpFValue)
{
  if ((lpFVRef->id == 0) || (lpFVRef->id >= MAX_FIELDS)) return(RC_ERR);
  FIELD *field = &session->field_store->fields[lpFVRef->id];
  if (field->_p2pmap.max_items > MAXLEN_P2PMAP) return(RC_ERR);
  int rc = RC_OK;
  const char *p = lpInput;
  FIELD_P2PMAP_ITEM item = {0};
  unsigned int vlen = 0;
  while (RC_IS_OK(rc)) {
    if (RC_IS_OK(rc)) rc = ParseP2PValue(session, p, true, &item, &vlen);
    if (RC_IS_OK(rc)) if (lpFValue->p2pmval.item_count >= field->_p2pmap.max_items) {
      fprintf(stderr, "ERR: too many '@%s' entries in '%s'\r\n", field->long_name, lpInput);
      rc = RC_ERR;
    }
    if (RC_IS_OK(rc)) {
      FIELD_P2PMAP_ITEM *mi = &lpFValue->p2pmval.items[lpFValue->p2pmval.item_count];
      mi->key = item.key;
      mi->value = item.value;
      ++lpFValue->p2pmval.item_count;
    }
    if (RC_IS_OK(rc)) p += vlen;
    if (RC_IS_OK(rc)) if (*p == '\0') break;
    if (RC_IS_OK(rc)) if (*p != ',') rc = RC_ERR;
    if (RC_IS_OK(rc)) ++p;
  }
  if (RC_IS_OK(rc)) lpFValue->type = FVT_P2PMAP;
  return(rc);
}

int ParseFieldPListValue(SESSION *session, FIELD_VALUE_REF *lpFVRef, const char *lpInput, FIELD_VALUE *lpFValue)
{
  if ((lpFVRef->id == 0) || (lpFVRef->id >= MAX_FIELDS)) return(RC_ERR);
  FIELD *field = &session->field_store->fields[lpFVRef->id];
  if (field->_plist.max_items > MAXLEN_PLIST) return(RC_ERR);
  int rc = RC_OK;
  const char *p = lpInput;
  FIELD_PLIST_ITEM item = {0};
  unsigned int vlen = 0;
  while (RC_IS_OK(rc)) {
    if (RC_IS_OK(rc)) rc = ParsePitch(session, p, true, &item.value, 0, &vlen);
    if (RC_IS_OK(rc)) if (lpFValue->plval.item_count >= field->_plist.max_items) {
      fprintf(stderr, "ERR: too many '@%s' entries in '%s'\r\n", field->long_name, lpInput);
      rc = RC_ERR;
    }
    if (RC_IS_OK(rc)) {
      FIELD_PLIST_ITEM *li = &lpFValue->plval.items[lpFValue->plval.item_count];
      li->value = item.value;
      ++lpFValue->plval.item_count;
    }
    if (RC_IS_OK(rc)) p += vlen;
    if (RC_IS_OK(rc)) if (*p == '\0') break;
    if (RC_IS_OK(rc)) if (*p != ',') rc = RC_ERR;
    if (RC_IS_OK(rc)) ++p;
  }
  if (RC_IS_OK(rc)) lpFValue->type = FVT_PLIST;
  return(rc);
}

int ParseFieldValue(SESSION *session, FIELD_VALUE_REF *lpFVRef, const char *lpInput, FIELD_VALUE *lpFValue)
{
  int rc = RC_OK;
  if (lpInput[0] == '@') {
    FIELD_VALUE_REF refFVRef;
    memset(&refFVRef, 0, sizeof(FIELD_VALUE_REF));
    if (RC_IS_OK(rc)) rc = ResolveFieldRef(session, lpInput, false, &refFVRef, lpFValue, 0);
    if (RC_IS_OK(rc)) {
      bool is_marker_ref = true;
      is_marker_ref &= lpFVRef->value_type == FVT_POS;
      is_marker_ref &= refFVRef.id == FID_Mx;
      is_marker_ref &= (lpFVRef->id != refFVRef.id) || (lpFVRef->index != refFVRef.index);
      if (is_marker_ref) {
        memset(lpFValue, 0, sizeof(FIELD_VALUE));
        lpFValue->pval.lcol.ref_mrk_id = refFVRef.index;
        lpFValue->pval.lrow.ref_mrk_id = refFVRef.index;
        lpFValue->type = FVT_POS;
      }
    }
  }
  else {
    switch(lpFVRef->value_type) {
      case FVT_STR:
        rc = ParseFieldStrValue(session, lpInput, lpFValue);
      break;
      case FVT_INT:
        rc = ParseFieldIntValue(session, lpInput, lpFValue);
      break;
      case FVT_BOOL:
        rc = ParseFieldBoolValue(session, lpInput, lpFValue);
      break;
      case FVT_POS:
        rc = ParseFieldPosValue(session, lpInput, lpFValue);
      break;
      case FVT_POS_STR:
        rc = ParseFieldPosStrValue(session, lpInput, lpFValue);
      break;
      case FVT_NOTEPOS:
      case FVT_POSFMT:
      case FVT_STY:
      case FVT_HLMODE:
        rc = ParseFieldNamedValue(session, lpFVRef, lpInput, lpFValue);
      break;
      case FVT_NOTELEN:
        rc = ParseFieldNoteLenValue(session, lpInput, lpFValue);
      break;
      case FVT_IMASK:
        rc = ParseFieldIMaskValue(session, lpFVRef, lpInput, lpFValue);
      break;
      case FVT_P2PMAP:
        rc = ParseFieldP2PMapValue(session, lpFVRef, lpInput, lpFValue);
      break;
      case FVT_PLIST:
        rc = ParseFieldPListValue(session, lpFVRef, lpInput, lpFValue);
      break;
      default:
        rc = RC_ERR;
      break;
    }
  }
  return(rc);
}


//*** get field value ***
//
//   ######   ########  ########            ##    ##    ####    ##
//  ##    ##  ##           ##               ##    ##   ##  ##   ##
//  ##        ##           ##               ##    ##  ##    ##  ##
//  ##        #######      ##               ##    ##  ##    ##  ##
//  ##   ###  ##           ##                ##  ##   ########  ##
//  ##    ##  ##           ##                 ####    ##    ##  ##
//   ######   ########     ##                  ##     ##    ##  ########
//

bool IsPositionFieldDefined(POSITION_FIELD *pfield)
{
  if (pfield->ref_mrk_id > 0) return(true);
  if (pfield->value != 0) return(true);
  return(false);
}

bool IsPositionDefined(POSITION *pos)
{
  if (pos->ref_mi_idx > 0) return(true);
  if (IsPositionFieldDefined(&pos->lcol)) return(true);
  if (IsPositionFieldDefined(&pos->lrow)) return(true);
  return(false);
}

bool IsMarkerDefined(MARKER *mrk)
{
  return(IsPositionDefined(&mrk->pos));
}

int PrintPosition(char *str, unsigned int maxlen, POSITION *pos)
{
  char tmp[MAX_INPUT_LENGTH] = {0};
  char offs[MAX_INPUT_LENGTH] = {0};
  sprintf(offs, "(%i, %i)", pos->lcol.value, pos->lrow.value);
  if (pos->ref_mi_idx != 0) {
    snprintf(tmp, MAX_INPUT_LENGTH, "@ref mel[%03u]", pos->ref_mi_idx);
    if ((pos->lcol.value != 0) || (pos->lrow.value != 0)) {
      strcat(tmp, " with offs ");
      strcat(tmp, offs);
    }
  }
  else if ((pos->lcol.ref_mrk_id != 0) || (pos->lrow.ref_mrk_id != 0)) {
    if (pos->lcol.ref_mrk_id == pos->lrow.ref_mrk_id) {
      snprintf(tmp, MAX_INPUT_LENGTH, "@ref mrk[%02u]", pos->lcol.ref_mrk_id);
    }
    else {
      snprintf(tmp, MAX_INPUT_LENGTH, "(@ref mrk[%02u], @ref mrk[%02u])", pos->lcol.ref_mrk_id, pos->lrow.ref_mrk_id);

    }
    if ((pos->lcol.value != 0) || (pos->lrow.value != 0)) {
      strcat(tmp, " with offs ");
      strcat(tmp, offs);
    }
  }
  else {
    strcat(tmp, offs);
  }
  snprintf(str, maxlen, "%s", tmp);
  return(RC_OK);
}

int GetFieldValue(SESSION *session, FIELD_VALUE_REF *lpFVRef, FIELD_VALUE *lpValue)
{
  if (lpFVRef->id >= MAX_FIELDS) return(RC_ERR);
  int rc = RC_OK;
  FIELD *field = &session->field_store->fields[lpFVRef->id];
  if (field->max_name_idx == 0) if (lpFVRef->index != 0) return(RC_ERR);
  if (field->max_name_idx > 0) if ((lpFVRef->index == 0) || (lpFVRef->index > field->max_name_idx)) return(RC_ERR);
  if (field->getter != NULL) rc = field->getter(session, lpFVRef, lpValue);
  else if (field->pvalue != NULL) {
    rc = RC_OK;
    void *pvalue = NULL;
    if (field->max_name_idx > 0) {
      if (lpFVRef->index == 0) rc = RC_ERR;
      if (RC_IS_OK(rc)) pvalue = ((void**)field->pvalue)[lpFVRef->index - 1];
    }
    else {
      pvalue = field->pvalue;
    }
    if (pvalue != NULL) {
      switch (field->value_type) {
        case FVT_STR:
          snprintf(lpValue->sval, MAX_INPUT_LENGTH, "%s", (char*)pvalue);
          if (RC_IS_OK(rc) && (session->uLogMode >= LOG_MODE_VERBOSE)) {
            if (field->max_name_idx == 0) fprintf(stdout, "DBG: got field '%s' value '%s'\r\n", field->short_name, lpValue->sval);
            else fprintf(stdout, "DBG: got field '%s'[%u] value '%s'\r\n", field->short_name, lpFVRef->index, lpValue->sval);
          }
        break;
        case FVT_INT:
          lpValue->ival = *(int*)pvalue;
          if (RC_IS_OK(rc) && (session->uLogMode >= LOG_MODE_VERBOSE)) {
            if (field->max_name_idx == 0) fprintf(stdout, "DBG: got field '%s' value %i\r\n", field->short_name, lpValue->ival);
            else fprintf(stdout, "DBG: got field '%s'[%u] value %i\r\n", field->short_name, lpFVRef->index, lpValue->ival);
          }
        break;
        case FVT_BOOL:
          lpValue->bval = *(bool*)pvalue;
          if (RC_IS_OK(rc) && (session->uLogMode >= LOG_MODE_VERBOSE)) {
            if (field->max_name_idx == 0) fprintf(stdout, "DBG: got field '%s' value %s\r\n", field->short_name, lpValue->bval ? "true" : "false");
            else fprintf(stdout, "DBG: got field '%s'[%u] value %s\r\n", field->short_name, lpFVRef->index, lpValue->bval ? "true" : "false");
          }
        break;
        case FVT_POS:
          memcpy(&lpValue->pval, pvalue, sizeof(POSITION));
          if (RC_IS_OK(rc) && (session->uLogMode >= LOG_MODE_VERBOSE)) {
            char posstr[128];
            PrintPosition(posstr, 128, &lpValue->pval);
            if (field->max_name_idx == 0) fprintf(stdout, "DBG: got field '%s' value %s\r\n", field->short_name, posstr);
            else fprintf(stdout, "DBG: got field '%s'[%u] value %s\r\n", field->short_name, lpFVRef->index, posstr);
          }
        break;
        case FVT_POSFMT:
        case FVT_NOTEPOS:
        case FVT_NOTELEN:
        case FVT_STY:
        case FVT_HLMODE:
          lpValue->ival = *(unsigned char*)pvalue;
          if (RC_IS_OK(rc) && (session->uLogMode >= LOG_MODE_VERBOSE)) {
            if (field->max_name_idx == 0) fprintf(stdout, "DBG: got field '%s' value %i\r\n", field->short_name, lpValue->ival);
            else fprintf(stdout, "DBG: got field '%s'[%u] value %i\r\n", field->short_name, lpFVRef->index, lpValue->ival);
          }
        break;
        default:
          rc = RC_ERR;
        break;
      }
    }
    else {
      rc = RC_ERR;
    }
    if (RC_IS_OK(rc)) lpValue->type = field->value_type;
  }
  else rc = RC_ERR;
  return(rc);
}

int GetPageBorders(SESSION *session, FIELD_VALUE_REF *lpFVRef, FIELD_VALUE *lpValue)
{
  if (lpFVRef->id != FID_PB) return(RC_ERR);
  unsigned int pbt, pbb, pbl, pbr;
  pbt = session->set.dim.page.borders.top;
  pbb = session->set.dim.page.borders.bottom;
  pbl = session->set.dim.page.borders.left;
  pbr = session->set.dim.page.borders.right;
  if ((pbt != pbb) || (pbt != pbl) || (pbt != pbr)) return(RC_ERR);
  lpValue->ival = pbt;
  lpValue->type = FVT_INT;
  return(RC_OK);
}

int GetArtist(SESSION *session, FIELD_VALUE_REF *lpFVRef, FIELD_VALUE *lpValue)
{
  if (lpFVRef->id != FID_A) return(RC_ERR);
  char *c, *te;
  c = session->song.artists.composer;
  te = session->song.artists.texter;
  if (strcmp(c, te) != 0) return(RC_ERR);
  snprintf(lpValue->sval, MAX_INPUT_LENGTH + 1, "%s", c);
  lpValue->type = FVT_STR;
  return(RC_OK);
}

int GetAdditionalText(SESSION *session, FIELD_VALUE_REF *lpFVRef, FIELD_VALUE *lpValue)
{
  if (lpFVRef->id != FID_ATx) return(RC_ERR);
  if ((lpFVRef->index <= 0) || (lpFVRef->index > MAX_ADDITIONS)) return(RC_ERR);
  ADDITION *add = &session->song.add.adds[lpFVRef->index - 1];
  memcpy(&lpValue->pval, &add->pos, sizeof(POSITION));
  snprintf(lpValue->sval, MAX_INPUT_LENGTH + 1, "%s", add->text);
  lpValue->type = FVT_POS_STR;
  if (/*RC_IS_OK(rc) &&*/ (session->uLogMode >= LOG_MODE_VERBOSE)) {
    char posstr[128];
    PrintPosition(posstr, 128, &add->pos);
    fprintf(stdout, "DBG: got field 'at'[%u] value %s, '%s'\r\n", lpFVRef->index, posstr, add->text);
  }
  return(RC_OK);
}

int GetFixCompression(SESSION *session, FIELD_VALUE_REF *lpFVRef, FIELD_VALUE *lpValue)
{
  if (lpFVRef->id != FID_FC) return(RC_ERR);
  unsigned int lc,hc;
  lc = session->set.compression.min;
  hc = session->set.compression.max;
  if (lc != hc) return(RC_ERR);
  lpValue->ival = lc;
  lpValue->type = FVT_INT;
  return(RC_OK);
}

int GetLyricsVisibility(SESSION *session, FIELD_VALUE_REF *lpFVRef, FIELD_VALUE *lpValue)
{
  if (lpFVRef->id != FID_LV) return(RC_ERR);
  lpValue->ilval = 0;
  for (unsigned int l = 0; l < MAX_LYRICS; ++l) {
    if (session->song.lyrics.lyrics[l].visible) {
      lpValue->ilval |= (0x01 << l);
    }
  }
  lpValue->type = FVT_IMASK;
  return(RC_OK);
}

int GetRetune(SESSION *session, FIELD_VALUE_REF *lpFVRef, FIELD_VALUE *lpValue)
{
  if (lpFVRef->id != FID_R) return(RC_ERR);
  memset(&lpValue->p2pmval, 0, sizeof(FIELD_P2PMAP_VALUE));
  for (unsigned int r = 0; r < MAX_RETUNED_NOTES; ++r) {
    RETUNED_NOTE *rn = &session->set.retuned.notes[r];
    if (rn->old_pitch != PITCH_NONE) {
      if (lpValue->p2pmval.item_count >= MAXLEN_P2PMAP) return(RC_ERR); // error: too many int-int map entries
      FIELD_P2PMAP_ITEM *mi = &lpValue->p2pmval.items[lpValue->p2pmval.item_count];
      mi->key = rn->old_pitch;
      mi->value = rn->new_pitch;
      ++lpValue->p2pmval.item_count;
    }
  }
  lpValue->type = FVT_P2PMAP;

  if (session->uLogMode >= LOG_MODE_VERBOSE) {
    fprintf(stdout, "DBG: got field 'r' value ");
    bool first = true;
    for (unsigned int i = 0; i < lpValue->p2pmval.item_count; ++i) {
      FIELD_P2PMAP_ITEM *mi = &lpValue->p2pmval.items[i];
      if (!first) fprintf(stdout, ",");
      fprintf(stdout, "%u:%u", mi->key, mi->value);
      first = false;
    }
    if (first) fprintf(stdout, "-none-");
    fprintf(stdout, "\r\n");
  }

  return(RC_OK);
}

int GetPitchMarks(SESSION *session, FIELD_VALUE_REF *lpFVRef, FIELD_VALUE *lpValue)
{
  if (lpFVRef->id != FID_PM) return(RC_ERR);
  memset(&lpValue->plval, 0, sizeof(FIELD_PLIST_VALUE));
  for (unsigned int m = 0; m < MAX_MARKED_PITCHES; ++m) {
    PITCH_MARK *pm = &session->set.pitch_marks.marks[m];
    if (pm->pitch != PITCH_NONE) {
      if (lpValue->plval.item_count >= MAXLEN_PLIST) return(RC_ERR); // error: too many int list entries
      FIELD_PLIST_ITEM *li = &lpValue->plval.items[lpValue->plval.item_count];
      li->value = pm->pitch;
      ++lpValue->plval.item_count;
    }
  }
  lpValue->type = FVT_PLIST;

  if (session->uLogMode >= LOG_MODE_VERBOSE) {
    fprintf(stdout, "DBG: got field 'pm' value ");
    bool first = true;
    for (unsigned int i = 0; i < lpValue->p2pmval.item_count; ++i) {
      FIELD_PLIST_ITEM *li = &lpValue->plval.items[i];
      if (!first) fprintf(stdout, ",");
      fprintf(stdout, "%u", li->value);
      first = false;
    }
    if (first) fprintf(stdout, "-none-");
    fprintf(stdout, "\r\n");
  }

  return(RC_OK);
}

//*** set field value ***
//
//   ######   ########  ########            ##    ##    ####    ##
//  ##    ##  ##           ##               ##    ##   ##  ##   ##
//  ##        ##           ##               ##    ##  ##    ##  ##
//   ######   #######      ##               ##    ##  ##    ##  ##
//        ##  ##           ##                ##  ##   ########  ##
//  ##    ##  ##           ##                 ####    ##    ##  ##
//   ######   ########     ##                  ##     ##    ##  ########
//

int SetStrValue(char *lpFieldValue, unsigned int uMaxLength, char *lpValue, unsigned int uOperator)
{
  char tmp[MAX_INPUT_LENGTH + 1];
  tmp[0] = '\0';
  switch (uOperator) {
    case OPERATOR_SET:
    break;
    case OPERATOR_ADD:
      strcpy(tmp, lpFieldValue);
    break;
    default:
      fprintf(stderr, "ERR: string operator %u is not yet supported\r\n", uOperator);
      return(RC_ERR);
    //break;
  }
  snprintf(lpFieldValue, uMaxLength + 1, "%s%s", tmp, lpValue);
  return(RC_OK);
}

int SetIntValue(int *lpFieldValue, int iMinValue, int iMaxValue, int iValue, unsigned char uOperator)
{
  int iNewValue = *lpFieldValue;
  switch (uOperator) {
    case OPERATOR_SET:
      iNewValue = iValue;
    break;
    case OPERATOR_ADD:
      iNewValue += iValue;
    break;
    case OPERATOR_SUB:
      iNewValue -= iValue;
    break;
/*    case OPERATOR_MUL:
      uNewValue *= iValue;
    break;
    case OPERATOR_DIV:
      uNewValue /= iValue;
    break;
    case OPERATOR_MOD:
      uNewValue %= iValue;
    break;*/
    default:
      fprintf(stderr, "ERR: integer operator %u is not yet supported\r\n", uOperator);
      return(RC_ERR);
    //break;
  }
  if ((iMinValue <= iMaxValue) && ((iMinValue != 0) || (iMaxValue != 0))) {
    if (iNewValue < iMinValue) {
      iNewValue = iMinValue;
    }
    else if (iNewValue > iMaxValue) {
      iNewValue = iMaxValue;
    }
  }
  *lpFieldValue = iNewValue;
  return(RC_OK);
}

int SetBoolValue(bool *lpFieldValue, bool bValue, unsigned char uOperator)
{
  bool bNewValue = *lpFieldValue;
  switch (uOperator) {
    case OPERATOR_SET:
      bNewValue = bValue;
    break;
    // case OPERATOR_ADD:
    //   iNewValue += iValue;
    // break;
    // case OPERATOR_SUB:
    //   iNewValue -= iValue;
    // break;
/*    case OPERATOR_MUL:
      uNewValue *= iValue;
    break;
    case OPERATOR_DIV:
      uNewValue /= iValue;
    break;
    case OPERATOR_MOD:
      uNewValue %= iValue;
    break;*/
    default:
      fprintf(stderr, "ERR: boolean operator %u is not yet supported\r\n", uOperator);
      return(RC_ERR);
    //break;
  }
  *lpFieldValue = bNewValue;
  return(RC_OK);
}

int SetPosValue(POSITION *lpFieldValue, POSITION *lpValue, unsigned char uOperator)
{
  if (uOperator != OPERATOR_SET) {
    if (lpValue->ref_mi_idx > 0) return(RC_ERR);
    if (lpValue->lcol.ref_mrk_id > 0) return(RC_ERR);
    if (lpValue->lrow.ref_mrk_id > 0) return(RC_ERR);
  }
  POSITION pNewValue = { 0 };
  memcpy(&pNewValue, lpFieldValue, sizeof(POSITION));
  switch (uOperator) {
    case OPERATOR_SET:
      memcpy(&pNewValue, lpValue, sizeof(POSITION));
    break;
    case OPERATOR_ADD:
      pNewValue.lcol.value += lpValue->lcol.value;
      pNewValue.lrow.value += lpValue->lrow.value;
    break;
    case OPERATOR_SUB:
      pNewValue.lcol.value -= lpValue->lcol.value;
      pNewValue.lrow.value -= lpValue->lrow.value;
    break;
/*    case OPERATOR_MUL:
      pNewValue.col *= lpValue->col;
      pNewValue.row *= lpValue->row;
    break;
    case OPERATOR_DIV:
      pNewValue.col /= lpValue->col;
      pNewValue.row /= lpValue->row;
    break;
    case OPERATOR_MOD:
      pNewValue.col %= lpValue->col;
      pNewValue.row %= lpValue->row;
    break;*/
    default:
      fprintf(stderr, "ERR: position operator %u is not yet supported\r\n", uOperator);
      return(RC_ERR);
    //break;
  }
  memcpy(lpFieldValue, &pNewValue, sizeof(POSITION));
  return(RC_OK);
}

int SetNamedValue(unsigned char *lpFieldValue, int iValue, unsigned char uOperator)
{
  unsigned char bNewValue = *lpFieldValue;
  switch (uOperator) {
    case OPERATOR_SET:
      bNewValue = (unsigned char)iValue;
    break;
    // case OPERATOR_ADD:
    //   bNewValue += bValue;
    // break;
    // case OPERATOR_SUB:
    //   bNewValue -= bValue;
    // break;
/*    case OPERATOR_MUL:
      uNewValue *= iValue;
    break;
    case OPERATOR_DIV:
      uNewValue /= iValue;
    break;
    case OPERATOR_MOD:
      uNewValue %= iValue;
    break;*/
    default:
      fprintf(stderr, "ERR: named value operator %u is not yet supported\r\n", uOperator);
      return(RC_ERR);
    //break;
  }
  *lpFieldValue = bNewValue;
  return(RC_OK);
}

int SetFieldValue(SESSION *session, FIELD_VALUE_REF *lpFVRef, FIELD_VALUE *lpValue, unsigned int uOperator)
{
  if (lpFVRef->id >= MAX_FIELDS) return(RC_ERR);
  int rc = RC_OK;
  FIELD *field = &session->field_store->fields[lpFVRef->id];
  if (field->value_type != lpValue->type) return(RC_ERR);
  if (field->max_name_idx == 0) if (lpFVRef->index != 0) return(RC_ERR);
  if (field->max_name_idx > 0) if ((lpFVRef->index == 0) || (lpFVRef->index > field->max_name_idx)) return(RC_ERR);
  if (field->setter != NULL) rc = field->setter(session, lpFVRef, lpValue, uOperator);
  else if (field->pvalue != NULL) {
    rc = RC_OK;
    void *pvalue = NULL;
    if (field->max_name_idx > 0) {
      if (lpFVRef->index == 0) rc = RC_ERR;
      if (RC_IS_OK(rc)) pvalue = ((void**)field->pvalue)[lpFVRef->index - 1];
    }
    else {
      pvalue = field->pvalue;
    }
    if (pvalue != NULL) {
      switch (field->value_type) {
        case FVT_STR:
          rc = SetStrValue((char*)pvalue, field->_str.max_length, lpValue->sval, uOperator);
          if (RC_IS_OK(rc) && (session->uLogMode >= LOG_MODE_VERBOSE)) {
            if (field->max_name_idx == 0) fprintf(stdout, "DBG: set field '%s' to '%s'\r\n", field->short_name, (char*)pvalue);
            else fprintf(stdout, "DBG: set field '%s'[%u] to '%s'\r\n", field->short_name, lpFVRef->index, (char*)pvalue);
          }
        break;
        case FVT_INT:
          rc = SetIntValue((int*)pvalue, field->_int.min_value, field->_int.max_value, lpValue->ival, uOperator);
          if (RC_IS_OK(rc) && (session->uLogMode >= LOG_MODE_VERBOSE)) {
            if (field->max_name_idx == 0) fprintf(stdout, "DBG: set field '%s' to %i\r\n", field->short_name, *(int*)pvalue);
            else fprintf(stdout, "DBG: set field '%s'[%u] to %i\r\n", field->short_name, lpFVRef->index, *(int*)pvalue);
          }
        break;
        case FVT_BOOL:
          rc = SetBoolValue((bool*)pvalue, lpValue->bval, uOperator);
          if (RC_IS_OK(rc) && (session->uLogMode >= LOG_MODE_VERBOSE)) {
            if (field->max_name_idx == 0) fprintf(stdout, "DBG: set field '%s' to %s\r\n", field->short_name, *(bool*)pvalue ? "true" : "false");
            else fprintf(stdout, "DBG: set field '%s'[%u] to %s\r\n", field->short_name, lpFVRef->index, *(bool*)pvalue ? "true" : "false");
          }
        break;
        case FVT_POS:
          rc = SetPosValue((POSITION*)pvalue, &lpValue->pval, uOperator);
          if (RC_IS_OK(rc) && (session->uLogMode >= LOG_MODE_VERBOSE)) {
            char posstr[128];
            PrintPosition(posstr, 128, (POSITION*)pvalue);
            if (field->max_name_idx == 0) fprintf(stdout, "DBG: set field '%s' to %s\r\n", field->short_name, posstr);
            else fprintf(stdout, "DBG: set field '%s'[%u] to %s\r\n", field->short_name, lpFVRef->index, posstr);
          }
        break;
        case FVT_POSFMT:
        case FVT_NOTEPOS:
        case FVT_NOTELEN:
        case FVT_STY:
        case FVT_HLMODE:
          rc = SetNamedValue((unsigned char*)pvalue, lpValue->ival, uOperator);
          if (RC_IS_OK(rc) && (session->uLogMode >= LOG_MODE_VERBOSE)) {
            if (field->max_name_idx == 0) fprintf(stdout, "DBG: set field '%s' to %i\r\n", field->short_name, *(unsigned char*)pvalue);
            else fprintf(stdout, "DBG: set field '%s'[%u] to %i\r\n", field->short_name, lpFVRef->index, *(unsigned char*)pvalue);
          }
        break;
        default:
          rc = RC_ERR;
        break;
      }
    }
    else {
      rc = RC_ERR;
    }
  }
  else rc = RC_ERR;
  return(rc);
}

int SetPageBorders(SESSION *session, FIELD_VALUE_REF *lpFVRef, FIELD_VALUE *lpValue, unsigned int uOperator)
{
  if (lpFVRef->id != FID_PB) return(RC_ERR);
  if (lpValue->type != FVT_INT) return(RC_WARN);
  int rc = RC_OK;
  if (RC_IS_OK(rc)) rc = SetIntValue((int*)&session->set.dim.page.borders.top, 0, 0, lpValue->ival, uOperator);
  if (RC_IS_OK(rc)) rc = SetIntValue((int*)&session->set.dim.page.borders.bottom, 0, 0, lpValue->ival, uOperator);
  if (RC_IS_OK(rc)) rc = SetIntValue((int*)&session->set.dim.page.borders.left, 0, 0, lpValue->ival, uOperator);
  if (RC_IS_OK(rc)) rc = SetIntValue((int*)&session->set.dim.page.borders.right, 0, 0, lpValue->ival, uOperator);
  if (RC_IS_OK(rc) && (session->uLogMode >= LOG_MODE_VERBOSE)) fprintf(stdout, "DBG: set field 'pb' to %i\r\n", (int)session->set.dim.page.borders.top);
  return(rc);
}

int SetNoteBorderRight(SESSION *session, FIELD_VALUE_REF *lpFVRef, FIELD_VALUE *lpValue, unsigned int uOperator)
{
  if (lpFVRef->id != FID_NBR) return(RC_ERR);
  if (lpValue->type != FVT_INT) return(RC_WARN);
  int rc = RC_OK;
  if (RC_IS_OK(rc)) rc = SetIntValue((int*)&session->set.dim.picture.notes.borders.right, 0, 0, lpValue->ival, uOperator);   // for melody style
  if (RC_IS_OK(rc)) rc = SetIntValue((int*)&session->set.dim.picture.info.c1_dist, 0, 0, lpValue->ival, uOperator);          // for bass style
  if (RC_IS_OK(rc) && (session->uLogMode >= LOG_MODE_VERBOSE)) fprintf(stdout, "DBG: set field 'nbr' to %i\r\n", (int)session->set.dim.picture.notes.borders.right);
  return(rc);
}

int SetStringDistance(SESSION *session, FIELD_VALUE_REF *lpFVRef, FIELD_VALUE *lpValue, unsigned int uOperator)
{
  if (lpFVRef->id != FID_SD) return(RC_ERR);
  if (lpValue->type != FVT_INT) return(RC_WARN);
  int rc = RC_OK;
  if (RC_IS_OK(rc)) rc = SetIntValue((int*)&session->set.dim.picture.col.width, 0, 0, lpValue->ival, uOperator);   // for melody style
  if (RC_IS_OK(rc)) rc = SetIntValue((int*)&session->set.dim.chords.dist, 0, 0, lpValue->ival, uOperator);         // for bass style
  if (RC_IS_OK(rc) && (session->uLogMode >= LOG_MODE_VERBOSE)) fprintf(stdout, "DBG: set field 'sd' to %i\r\n", (int)session->set.dim.picture.col.width);
  return(rc);
}

int SetArtist(SESSION *session, FIELD_VALUE_REF *lpFVRef, FIELD_VALUE *lpValue, unsigned int uOperator)
{
  if (lpFVRef->id != FID_A) return(RC_ERR);
  if (lpValue->type != FVT_STR) return(RC_WARN);
  int rc = RC_OK;
  if (RC_IS_OK(rc)) rc = SetStrValue(session->song.artists.composer, MAXLEN_ARTIST, lpValue->sval, uOperator);
  if (RC_IS_OK(rc)) rc = SetStrValue(session->song.artists.texter, MAXLEN_ARTIST, lpValue->sval, uOperator);
  if (RC_IS_OK(rc) && (session->uLogMode >= LOG_MODE_VERBOSE)) fprintf(stdout, "DBG: set field 'a' (composer) to '%s'\r\n", session->song.artists.composer);
  if (RC_IS_OK(rc) && (session->uLogMode >= LOG_MODE_VERBOSE)) fprintf(stdout, "DBG: set field 'a' (texter) to '%s'\r\n", session->song.artists.composer);
  return(rc);
}

int SetLyricsVisibility(SESSION *session, FIELD_VALUE_REF *lpFVRef, FIELD_VALUE *lpValue, unsigned int uOperator)
{
  if (lpFVRef->id != FID_LV) return(RC_ERR);
  if (lpValue->type != FVT_IMASK) return(RC_WARN);
  if (lpValue->ilval >= (0x01 << MAX_LYRICS)) return(RC_WARN);
  int rc = RC_OK;
  for (unsigned int l = 0; l < MAX_LYRICS; ++l) {
    bool set = lpValue->ilval & (0x01 << l);
    switch (uOperator) {
      case OPERATOR_SET:
        session->song.lyrics.lyrics[l].visible = set;
        if (session->uLogMode >= LOG_MODE_VERBOSE) fprintf(stdout, "DBG: set field 'lv'[%u] to %s\r\n", l + 1, session->song.lyrics.lyrics[l].visible ? "true" : "false");
      break;
      case OPERATOR_ADD:
        session->song.lyrics.lyrics[l].visible |= set;
        if (set && (session->uLogMode >= LOG_MODE_VERBOSE)) fprintf(stdout, "DBG: set field 'lv'[%u] to %s\r\n", l + 1, session->song.lyrics.lyrics[l].visible ? "true" : "false");
      break;
      case OPERATOR_SUB:
        session->song.lyrics.lyrics[l].visible &= !set;
        if (!set && (session->uLogMode >= LOG_MODE_VERBOSE)) fprintf(stdout, "DBG: set field 'lv'[%u] to %s\r\n", l + 1, session->song.lyrics.lyrics[l].visible ? "true" : "false");
      break;
  /*    case OPERATOR_MUL:
        uNewValue *= iValue;
      break;
      case OPERATOR_DIV:
        uNewValue /= iValue;
      break;
      case OPERATOR_MOD:
        uNewValue %= iValue;
      break;*/
      default:
        fprintf(stderr, "ERR: index list operator %u is not yet supported\r\n", uOperator);
        return(RC_ERR);
      //break;
    }

  }
  return(rc);
}

int SetAdditionalText(SESSION *session, FIELD_VALUE_REF *lpFVRef, FIELD_VALUE *lpValue, unsigned int uOperator)
{
  if (lpFVRef->id != FID_ATx) return(RC_ERR);
  if (lpValue->type != FVT_POS_STR) return(RC_ERR);
  if ((lpFVRef->index <= 0) || (lpFVRef->index > MAX_ADDITIONS)) return(RC_ERR);
  ADDITION *add = &session->song.add.adds[lpFVRef->index - 1];
  int rc = RC_OK;
  if (RC_IS_OK(rc)) rc = SetPosValue(&add->pos, &lpValue->pval, uOperator);
  if (RC_IS_OK(rc)) rc = SetStrValue(add->text, MAXLEN_ADDITION, lpValue->sval, uOperator);
  if (RC_IS_OK(rc) && (session->uLogMode >= LOG_MODE_VERBOSE)) {
    char posstr[128];
    PrintPosition(posstr, 128, &add->pos);
    fprintf(stdout, "DBG: set field 'at'[%u] to %s, '%s'\r\n", lpFVRef->index, posstr, add->text);
  }
  return(rc);
}

int SetFixCompression(SESSION *session, FIELD_VALUE_REF *lpFVRef, FIELD_VALUE *lpValue, unsigned int uOperator)
{
  if (lpFVRef->id != FID_FC) return(RC_ERR);
  if (lpValue->type != FVT_INT) return(RC_WARN);
  int rc = RC_OK;
  if (RC_IS_OK(rc)) rc = SetIntValue((int*)&session->set.compression.min, 0, 0, lpValue->ival, uOperator);
  if (RC_IS_OK(rc)) rc = SetIntValue((int*)&session->set.compression.max, 0, 0, lpValue->ival, uOperator);
  if (RC_IS_OK(rc) && (session->uLogMode >= LOG_MODE_VERBOSE)) fprintf(stdout, "DBG: set field 'fc' to %i\r\n", session->set.compression.min);
  return(rc);
}

int SetRetune(SESSION *session, FIELD_VALUE_REF *lpFVRef, FIELD_VALUE *lpValue, unsigned int uOperator)
{
  if (lpFVRef->id != FID_R) return(RC_ERR);
  if (lpValue->type != FVT_P2PMAP) return(RC_WARN);
  int rc = RC_OK;
  if (uOperator == OPERATOR_SET) {
    for (unsigned int r = 0; r < MAX_RETUNED_NOTES; ++r) {
      RETUNED_NOTE *rn = &session->set.retuned.notes[r];
      rn->old_pitch = PITCH_NONE;
      rn->new_pitch = PITCH_NONE;
    }
  }
  for (unsigned int i = 0; i < lpValue->p2pmval.item_count; ++i) {
    FIELD_P2PMAP_ITEM *mi = &lpValue->p2pmval.items[i];
    RETUNED_NOTE *rn = (RETUNED_NOTE*)NULL;
    for (unsigned int r = 0; r < MAX_RETUNED_NOTES; ++r) {
      RETUNED_NOTE *trn = &session->set.retuned.notes[r];
      if (trn->old_pitch != mi->key) continue;
      rn = trn;
      break;
    }
    switch (uOperator) {
      case OPERATOR_SET:
      case OPERATOR_ADD:
        if ((rn == NULL) && (mi->value != PITCH_NONE) && (mi->key != mi->value)) {
          for (unsigned int r = 0; r < MAX_RETUNED_NOTES; ++r) {
            RETUNED_NOTE *trn = &session->set.retuned.notes[r];
            if (trn->old_pitch != PITCH_NONE) continue;
            rn = trn;
            break;
          }
          if (rn == NULL) {
            fprintf(stderr, "ERR: too many '@retune' entries\r\n");
            return(RC_ERR); // error: no empty retuning item available
          }
        }
        if (rn != NULL) {
          rn->old_pitch = mi->key;
          rn->new_pitch = mi->value;
          if ((rn->new_pitch == PITCH_NONE) || (rn->old_pitch == rn->new_pitch)) {
            rn->old_pitch = PITCH_NONE;
            rn->new_pitch = PITCH_NONE;
          }
        }
      break;
      case OPERATOR_SUB:
        if (rn != NULL) {
          rn->old_pitch = PITCH_NONE;
          rn->new_pitch = PITCH_NONE;
        }
      break;
      default:
        fprintf(stderr, "ERR: int-int map operator %u is not yet supported\r\n", uOperator);
        return(RC_ERR);
      break;
    }
  }

  if (RC_IS_OK(rc) && (session->uLogMode >= LOG_MODE_VERBOSE)) {
    fprintf(stdout, "DBG: set field 'r' to ");
    bool first = true;
    for (unsigned int r = 0; r < MAX_RETUNED_NOTES; ++r) {
      RETUNED_NOTE *rn = &session->set.retuned.notes[r];
      if (rn->old_pitch == PITCH_NONE) continue;
      if (!first) fprintf(stdout, ",");
      fprintf(stdout, "%u:%u", rn->old_pitch, rn->new_pitch);
    }
    fprintf(stdout, "\r\n");
  }

  return(rc);
}

int SetPitchMarks(SESSION *session, FIELD_VALUE_REF *lpFVRef, FIELD_VALUE *lpValue, unsigned int uOperator)
{
  if (lpFVRef->id != FID_PM) return(RC_ERR);
  if (lpValue->type != FVT_PLIST) return(RC_WARN);
  int rc = RC_OK;
  if (uOperator == OPERATOR_SET) {
    for (unsigned int m = 0; m < MAX_MARKED_PITCHES; ++m) {
      PITCH_MARK *pm = &session->set.pitch_marks.marks[m];
      pm->pitch = PITCH_NONE;
    }
  }
  for (unsigned int i = 0; i < lpValue->plval.item_count; ++i) {
    FIELD_PLIST_ITEM *li = &lpValue->plval.items[i];
    PITCH_MARK *pm = (PITCH_MARK*)NULL;
    for (unsigned int m = 0; m < MAX_MARKED_PITCHES; ++m) {
      PITCH_MARK *tpm = &session->set.pitch_marks.marks[m];
      if (tpm->pitch != li->value) continue;
      pm = tpm;
      break;
    }
    switch (uOperator) {
      case OPERATOR_SET:
      case OPERATOR_ADD:
        if ((pm == NULL) && (li->value != PITCH_NONE)) {
          for (unsigned int m = 0; m < MAX_MARKED_PITCHES; ++m) {
            PITCH_MARK *tpm = &session->set.pitch_marks.marks[m];
            if (tpm->pitch != PITCH_NONE) continue;
            pm = tpm;
            break;
          }
          if (pm == NULL) {
            fprintf(stderr, "ERR: too many '@pitch-mark' entries\r\n");
            return(RC_ERR); // error: no empty pitch mark item available
          }
        }
        if (pm != NULL) {
          pm->pitch = li->value;
        }
      break;
      case OPERATOR_SUB:
        if (pm != NULL) {
          pm->pitch = PITCH_NONE;
        }
      break;
      default:
        fprintf(stderr, "ERR: int list operator %u is not yet supported\r\n", uOperator);
        return(RC_ERR);
      break;
    }
  }

  if (RC_IS_OK(rc) && (session->uLogMode >= LOG_MODE_VERBOSE)) {
    fprintf(stdout, "DBG: set field 'pm' to ");
    bool first = true;
    for (unsigned int m = 0; m < MAX_MARKED_PITCHES; ++m) {
      PITCH_MARK *pm = &session->set.pitch_marks.marks[m];
      if (pm->pitch == PITCH_NONE) continue;
      if (!first) fprintf(stdout, ",");
      fprintf(stdout, "%u", pm->pitch);
    }
    fprintf(stdout, "\r\n");
  }

  return(rc);
}


//*** init session fields ***
//
//   ######   ##    ##   ######   ########
//     ##     ###   ##     ##        ##
//     ##     ####  ##     ##        ##
//     ##     ## ## ##     ##        ##
//     ##     ##  ####     ##        ##
//     ##     ##   ###     ##        ##
//   ######   ##    ##   ######      ##
//

#define _FIELD(fid) session->field_store->fields[fid]

#define INIT_FIELD_S(fid,sn,ln,pval,mlen)           _FIELD(FID_##fid) = { .id=FID_##fid, .short_name=sn, .long_name=ln, .max_name_idx=0, .value_type=FVT_STR, .pvalue=pval, ._str = { .max_length=mlen } }
#define INIT_FIELD_S_GS(fid,sn,ln,get,set)          _FIELD(FID_##fid) = { .id=FID_##fid, .short_name=sn, .long_name=ln, .max_name_idx=0, .value_type=FVT_STR, .getter=get, .setter=set }
#define INIT_FIELD_Sx(fid,sn,ln,mni,pval,mlen)      _FIELD(FID_##fid) = { .id=FID_##fid, .short_name=sn, .long_name=ln, .max_name_idx=mni, .value_type=FVT_STR, .pvalue=pval, ._str = { .max_length=mlen } }

#define INIT_FIELD_I(fid,sn,ln,pval)                _FIELD(FID_##fid) = { .id=FID_##fid, .short_name=sn, .long_name=ln, .max_name_idx=0, .value_type=FVT_INT, .pvalue=pval }
#define INIT_FIELD_I_R(fid,sn,ln,pval,vmin,vmax)    _FIELD(FID_##fid) = { .id=FID_##fid, .short_name=sn, .long_name=ln, .max_name_idx=0, .value_type=FVT_INT, .pvalue=pval, ._int = { .min_value=vmin, .max_value=vmax } }
#define INIT_FIELD_I_GS(fid,sn,ln,get,set)          _FIELD(FID_##fid) = { .id=FID_##fid, .short_name=sn, .long_name=ln, .max_name_idx=0, .value_type=FVT_INT, .getter=get, .setter=set }
#define INIT_FIELD_Ix(fid,sn,ln,mni)                _FIELD(FID_##fid) = { .id=FID_##fid, .short_name=sn, .long_name=ln, .max_name_idx=mni, .value_type=FVT_INT, .pvalue=pval }

#define INIT_FIELD_B(fid,sn,ln,pval)                _FIELD(FID_##fid) = { .id=FID_##fid, .short_name=sn, .long_name=ln, .max_name_idx=0, .value_type=FVT_BOOL, .pvalue=pval }
#define INIT_FIELD_Bx(fid,sn,ln,mni,pval)           _FIELD(FID_##fid) = { .id=FID_##fid, .short_name=sn, .long_name=ln, .max_name_idx=mni, .value_type=FVT_BOOL, .pvalue=pval }

#define INIT_FIELD_P(fid,sn,ln,pval)                _FIELD(FID_##fid) = { .id=FID_##fid, .short_name=sn, .long_name=ln, .value_type=FVT_POS, .pvalue=pval }
#define INIT_FIELD_Px(fid,sn,ln,mni,pval)           _FIELD(FID_##fid) = { .id=FID_##fid, .short_name=sn, .long_name=ln, .max_name_idx=mni, .value_type=FVT_POS, .pvalue=pval }

#define INIT_FIELD_PSx_GS(fid,sn,ln,mni,get,set)    _FIELD(FID_##fid) = { .id=FID_##fid, .short_name=sn, .long_name=ln, .max_name_idx=mni, .value_type=FVT_POS_STR, .getter=get, .setter=set }

#define INIT_FIELD_PF(fid,sn,ln,pval)               _FIELD(FID_##fid) = { .id=FID_##fid, .short_name=sn, .long_name=ln, .max_name_idx=0, .value_type=FVT_POSFMT, .pvalue=pval }
#define INIT_FIELD_NP(fid,sn,ln,pval)               _FIELD(FID_##fid) = { .id=FID_##fid, .short_name=sn, .long_name=ln, .max_name_idx=0, .value_type=FVT_NOTEPOS, .pvalue=pval }
#define INIT_FIELD_NL(fid,sn,ln,pval)               _FIELD(FID_##fid) = { .id=FID_##fid, .short_name=sn, .long_name=ln, .max_name_idx=0, .value_type=FVT_NOTELEN, .pvalue=pval }
#define INIT_FIELD_HLM(fid,sn,ln,pval)              _FIELD(FID_##fid) = { .id=FID_##fid, .short_name=sn, .long_name=ln, .max_name_idx=0, .value_type=FVT_HLMODE, .pvalue=pval }

#define INIT_FIELD_IM_GS(fid,sn,ln,imax,get,set)    _FIELD(FID_##fid) = { .id=FID_##fid, .short_name=sn, .long_name=ln, .max_name_idx=0, .value_type=FVT_IMASK, .getter=get, .setter=set, ._imask = { .max_index=imax } }

#define INIT_FIELD_STY(fid,sn,ln,pval)              _FIELD(FID_##fid) = { .id=FID_##fid, .short_name=sn, .long_name=ln, .max_name_idx=0, .value_type=FVT_STY, .pvalue=pval }

#define INIT_FIELD_P2PM_GS(fid,sn,ln,imax,get,set)  _FIELD(FID_##fid) = { .id=FID_##fid, .short_name=sn, .long_name=ln, .max_name_idx=0, .value_type=FVT_P2PMAP, .getter=get, .setter=set, ._p2pmap = { .max_items=imax } }
#define INIT_FIELD_PL_GS(fid,sn,ln,imax,get,set)    _FIELD(FID_##fid) = { .id=FID_##fid, .short_name=sn, .long_name=ln, .max_name_idx=0, .value_type=FVT_PLIST, .getter=get, .setter=set, ._plist = { .max_items=imax } }

int InitFields(SESSION *session)
{
  session->field_store = (FIELD_STORE*)calloc(1, sizeof(FIELD_STORE));
  // special fields
  // FID_END,  // end, end of input
  INIT_FIELD_S(IN, "in", "input-file", NULL, 0);
  INIT_FIELD_I(TR, "tr", "transpose", &session->song.transpose);
  // page layout
  INIT_FIELD_I_GS(PB, "pb", "page-borders", GetPageBorders, SetPageBorders);
  INIT_FIELD_I(PBT, "pbt", "page-border-top", &session->set.dim.page.borders.top);
  INIT_FIELD_I(PBB, "pbb", "page-border-bottom", &session->set.dim.page.borders.bottom);
  INIT_FIELD_I(PBL, "pbl", "page-border-left", &session->set.dim.page.borders.left);
  INIT_FIELD_I(PBR, "pbr", "page-border-right", &session->set.dim.page.borders.right);
  INIT_FIELD_I(NBL, "nbl", "note-border-left", &session->set.dim.picture.notes.borders.left);
  INIT_FIELD_I(NBR, "nbr", "note-border-right", &session->set.dim.picture.notes.borders.right);
  _FIELD(FID_NBR).setter = SetNoteBorderRight;
  // zither layout
  INIT_FIELD_I(SC, "sc", "string-count", &session->set.count.cols);
  INIT_FIELD_I(SD, "sd", "string-distance", &session->set.dim.picture.col.width);
  _FIELD(FID_SD).setter = SetStringDistance;
  INIT_FIELD_I(CC, "cc", "chord-count", &session->set.count.chords);
  INIT_FIELD_I(CD, "cd", "chord-distance", &session->set.dim.chords.c1_dist);
  INIT_FIELD_I(CW, "cw", "chord-width", &session->set.dim.chords.width);
  // pitches and retunings
  INIT_FIELD_P2PM_GS(R, "r", "retune", MAX_RETUNED_NOTES, GetRetune, SetRetune);
  INIT_FIELD_HLM(HR, "hr", "highlight-retuned", &session->set.retuned.highlight_mode);
  INIT_FIELD_PL_GS(PM, "pm", "pitch-marks", MAX_MARKED_PITCHES, GetPitchMarks, SetPitchMarks);
  // default values
  INIT_FIELD_NL(DL, "dl", "default-length", &session->set.defaults.length);
  INIT_FIELD_I_R(DO, "do", "default-octave", &session->set.defaults.octave, 1, 3);
  // song information
  INIT_FIELD_S(SN, "sn", "short-name", session->song.short_name, MAXLEN_SHORT_NAME);
  INIT_FIELD_S(T, "t", "title", session->song.title, MAXLEN_TITLE);
  INIT_FIELD_S(S, "s", "sub-title", session->song.subtitle, MAXLEN_SUBTITLE);
  INIT_FIELD_S_GS(A, "a", "artist", GetArtist, SetArtist);
  INIT_FIELD_S(C, "c", "composer", session->song.artists.composer, MAXLEN_ARTIST);
  INIT_FIELD_S(TE, "te", "texter", session->song.artists.texter, MAXLEN_ARTIST);
  INIT_FIELD_S(ARR, "arr", "arrangement", session->song.artists.arrangement, MAXLEN_ARRANGEMENT);
  // markers
  for (int m = 0; m < MAX_MARKERS; ++m) session->field_store->tmp.m[m] = &session->markers[m].pos;
  INIT_FIELD_Px(Mx, "m", "marker", MAX_MARKERS, session->field_store->tmp.m);
  _FIELD(FID_Mx).short_name_ignore_case = true;
  // lyrics
  for (int l = 0; l < MAX_LYRICS; ++l) session->field_store->tmp.l[l] = session->song.lyrics.lyrics[l].text;
  INIT_FIELD_Sx(Lx, "l", "lyrics", MAX_LYRICS, session->field_store->tmp.l, MAXLEN_LYRICS);
  for (int l = 0; l < MAX_LYRICS; ++l) session->field_store->tmp.lp[l] = &session->song.lyrics.lyrics[l].pos;
  INIT_FIELD_Px(LPx, "lp", "lyrics-position", MAX_LYRICS, session->field_store->tmp.lp);
  INIT_FIELD_S(LR, "lr", "lyrics-refrain", session->song.lyrics.refrain.text, MAXLEN_LYRICS);
  INIT_FIELD_P(LPR, "lpr", "lyrics-position-refrain", &session->song.lyrics.refrain.pos);
  INIT_FIELD_B(LN, "ln", "print-lyrics-numbers", &session->set.print_lyric_numbers);
  INIT_FIELD_PF(LPF, "lpf", "lyrics-positioning-format", &session->song.lyrics.pos_fmt);
  for (int l = 0; l < MAX_LYRICS; ++l) session->field_store->tmp.lv[l] = &session->song.lyrics.lyrics[l].visible;
  INIT_FIELD_Bx(LVx, "lv", "lyrics-visible", MAX_LYRICS, session->field_store->tmp.lv);
  INIT_FIELD_IM_GS(LV, "lv", "lyrics-visible", MAX_LYRICS, GetLyricsVisibility, SetLyricsVisibility);
  INIT_FIELD_I(LRS, "lrs", "lyrics-rel-size", &session->song.lyrics.rel_size);
  // additional texts
  INIT_FIELD_S(H, "h", "hint", session->song.hint, MAXLEN_HINT);
  INIT_FIELD_S(W, "w", "warning", session->song.warning, MAXLEN_WARNING);
  INIT_FIELD_PF(ATPF, "atpf", "additional-text-positioning-format", &session->song.add.pos_fmt);
  INIT_FIELD_PSx_GS(ATx, "at", "additional-text", MAX_ADDITIONS, GetAdditionalText, SetAdditionalText);
  // layout parameters
  INIT_FIELD_STY(STY, "sty", "style", &session->set.layout_style);
  INIT_FIELD_NP(NP, "np", "notes-position", &session->set.notes_position);
  INIT_FIELD_I(LC, "lc", "lowest-compression", &session->set.compression.min);
  INIT_FIELD_I(HC, "hc", "highest-compression", &session->set.compression.max);
  INIT_FIELD_I_GS(FC, "fc", "fix-compression", GetFixCompression, SetFixCompression);
  // cut markers and grid
  INIT_FIELD_B(G, "g", "print-grid", &session->set.print_grid);
  INIT_FIELD_PF(GF, "gf", "grid-format", &session->set.grid_fmt);
  INIT_FIELD_B(LG, "lg", "print-light-grid", &session->set.print_light_grid);
  INIT_FIELD_I(CEW, "cew", "cut-edge-width", &session->set.dim.cut_edge.width);
  INIT_FIELD_I(CEH, "ceh", "cut-edge-height", &session->set.dim.cut_edge.height);
  INIT_FIELD_B(CE, "ce", "print-cut-edge", &session->set.print_cut_edge);
  INIT_FIELD_I(CSW, "csw", "cut-side-width", &session->set.dim.cut_side.width);
  INIT_FIELD_B(CS, "cs", "print-cut-side", &session->set.print_cut_side);
  INIT_FIELD_I(CAW, "caw", "clip-area-width", &session->set.dim.clip_area.width);
  INIT_FIELD_I(CAH, "cah", "clip-area-height", &session->set.dim.clip_area.height);
  INIT_FIELD_I(CACH, "cach", "clip-area-center-height", &session->set.dim.clip_area.center_height);
  INIT_FIELD_B(CA, "ca", "print-clip-area", &session->set.print_clip_area);

  return(RC_OK);
}

int FreeFields(SESSION *session)
{
  free(session->field_store);
  return(RC_OK);
}

#ifdef PSZL_DEVELOP

int TestFieldRef(SESSION *session, FIELD *field, FIELD_VALUE_REF *lpFVRef)
{
  int rc = RC_OK;
  FIELD_VALUE_REF fvref = {0};
  FIELD_VALUE fval = {0};
  FIELD_VALUE reffval = {0};
  char fname[32] = {0};
  char atfname[32] = {0};

  if (lpFVRef->index == 0) snprintf(fname, 32, "%s", field->short_name);
  else snprintf(fname, 32, "%s%u", field->short_name, lpFVRef->index);
  snprintf(atfname, 32, "@%s", fname);

  if (RC_IS_OK(rc)) rc = GetFieldValue(session, lpFVRef, &fval);
  if (RC_IS_OK(rc)) if (fval.type == FVT__NONE) rc = RC_ERR;
  if (RC_IS_OK(rc)) rc = GetFieldValueRef(session, fname, &fvref);
  if (RC_IS_OK(rc)) if (memcmp(lpFVRef, &fvref, sizeof(FIELD_VALUE_REF)) != 0) rc = RC_ERR;
  if (RC_IS_OK(rc)) rc = ParseFieldValue(session, &fvref, atfname, &reffval);
  if (RC_IS_OK(rc)) if (memcmp(&fval, &reffval, sizeof(FIELD_VALUE)) != 0) rc = RC_ERR;
  if (RC_IS_OK(rc)) rc = SetFieldValue(session, lpFVRef, &fval, OPERATOR_SET);

  if (RC_IS_OK(rc)) {
    if (field->max_name_idx == 0) fprintf(stdout, "DBG: field '%s' test passed\r\n", field->short_name);
    else fprintf(stdout, "DBG: field '%s'[%u] test passed\r\n", field->short_name, lpFVRef->index);
  }
  else {
    if (field->max_name_idx == 0) fprintf(stdout, "ERR: field '%s' test FAILED with error %i\r\n", field->short_name, rc);
    else fprintf(stdout, "ERR: field '%s'[%u] test FAILED with error %i\r\n", field->short_name, lpFVRef->index, rc);
  }

  return(rc);
}

int TestField(SESSION *session, unsigned int field_id, FIELD *field)
{
  int rc = RC_OK;
  FIELD_VALUE_REF fvref = {0};
  FIELD_VALUE fval = {0};

  // skipping test of special fields
  if (field_id == FID__NONE) return(RC_OK);
  if (field_id == FID_END) return(RC_OK);
  if (field_id == FID_IN) return(RC_OK);

  if (field->id != field_id) {
    fprintf(stderr, "ERR: field [%u] has a different ID %u\r\n", field_id, field->id);
    return(RC_ERR);
  }
  if (field->short_name == NULL) {
    fprintf(stderr, "ERR: field [%u] does not have a short name\r\n", field_id);
    return(RC_ERR);
  }
  if (field->long_name == NULL) {
    fprintf(stderr, "WRN: field [%u] does not have a long name\r\n", field_id);
  }
  if ((field->pvalue == NULL) && (field->getter == NULL)) {
    fprintf(stderr, "ERR: field '%s' does not have a value nor a getter\r\n", field->short_name);
    return(RC_ERR);
  }
  if ((field->pvalue == NULL) && (field->setter == NULL)) {
    fprintf(stderr, "ERR: field '%s' does not have a value nor a setter\r\n", field->short_name);
    return(RC_ERR);
  }
  fvref.id = field->id;
  if (field->max_name_idx > 0) fvref.index = 1;
  fvref.value_type = field->value_type;
  if (RC_IS_OK(rc)) rc = TestFieldRef(session, field, &fvref);
  if (field->max_name_idx > 0) {
    fvref.index = field->max_name_idx;
    if (RC_IS_OK(rc)) rc = TestFieldRef(session, field, &fvref);
  }

  return(rc);
}

int TestFields(SESSION *session)
{
  int rc = RC_OK;

  fprintf(stdout, "-- DBG: testing all fields --\r\n");

  for (int fid = 0; (RC_IS_OK(rc)) && (fid < MAX_FIELDS); ++fid) {
    rc = TestField(session, fid, &session->field_store->fields[fid]);
  }

  if (RC_IS_OK(rc)) {
    fprintf(stdout, "-- DBG: testing all fields passed --\r\n");
  }
  else {
    fprintf(stderr, "-- ERR: testing all fields FAILED with error %i --\r\n", rc);
  }

  return(rc);
}

#endif