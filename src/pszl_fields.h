/*
  This file is part of PS ZitherLayout.

  PS ZitherLayout is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  PS ZitherLayout is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with PS ZitherLayout.  If not, see <http://www.gnu.org/licenses/>.


  Diese Datei ist Teil von PS ZitherLayout.

  PS ZitherLayout ist Freie Software: Sie können es unter den Bedingungen
  der GNU General Public License, wie von der Free Software Foundation,
  Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren
  veröffentlichten Version, weiterverbreiten und/oder modifizieren.

  PS ZitherLayout wird in der Hoffnung, dass es nützlich sein wird, aber
  OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
  Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
  Siehe die GNU General Public License für weitere Details.

  Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
  Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
*/

/*
**  © 2011-2023 Carsten Pfeffer (PiperSoft)
**
**  @project: PS ZitherLayout
**  @file: pszl_fields.h
**  @brief definition of field handling module
**
**  @date: 2022-11-29
**  @author: Carsten Pfeffer (nee Gaede)
*/
#pragma once

#include "pszl_definitions.h"

// field IDs                              if no default is specified, field value defaults to 0 / false / empty string
enum FIELD_IDS {
  FID__NONE = 0,
  // special fields
  FID_END,  // end, end of input
  FID_IN,   // in | input-file
  FID_TR,   // tr | transpose
  // page layout
            // page height                defaults to DIM_DEF_PAGE_HEIGHT
            // page width                 defaults to DIM_DEF_PAGE_WIDTH
  FID_PB,   // pb | page-borders          defaults to DIM_DEF_PAGE_BORDER
  FID_PBT,  // pbt | page-border-top
  FID_PBB,  // pbb | page-border-bottom
  FID_PBL,  // pbl | page-border-left
  FID_PBR,  // pbr | page-border-right
  FID_NBL,  // nbl | note-border-left     defaults to half of @sd - string distance, or column width
  FID_NBR,  // nbr | note-border-right    defaults to DIM_DEF_BORDER_RIGHT for melody style
            //                            defaults to DIM_DEF_INFO_C1_DIST for bass style
            // nbt/nbb - top and bottom note borders are calculated, each is half of row height
  // zither layout
  FID_SC,   // sc | string-count          defaults to DEF_NUM_STRINGS
  FID_SD,   // sd | string-distance       defaults to DIM_DEF_COL_WIDTH
  FID_CC,   // cc | chord-count           defaults to DEF_MAX_CHORD
  FID_CD,   // cd | chord-distance        defaults to DIM_DEF_ACCORD_DIST
  FID_CW,   // cw | chord-width           defaults to DIM_DEF_ACCORD_WIDTH
  // default values
  FID_DL,   // dl | default-length
  FID_DO,   // do | default-octave
  // pitches and retuning
  FID_R,    // r | retune
  FID_HR,   // hr | highlight-retuned
  FID_PM,   // pm | pitch-marks
  // song information
  FID_SN,   // sn | short-name
  FID_T,    // t | title
  FID_S,    // s | sub-title
  FID_A,    // a | artist
  FID_C,    // c | composer
  FID_TE,   // te | texter
  FID_ARR,  // arr | arrangement
  // markers
  FID_Mx,   // m<x> | marker-<x>
  // lyrics
  FID_Lx,   // l<x> | lyrics-<x>
  FID_LPx,  // lp<x> | lyrics-position-<x>
  FID_LR,   // lr | lyrics-refrain
  FID_LPR,  // lpr | lyrics-position-refrain
  FID_LN,   // ln | print-lyrics-numbers
  FID_LPF,  // lpf | lyrics-positioning-format
  FID_LVx,  // lv<x> | lyrics-visible-<x>
  FID_LV,   // lv | lyrics-visibile
  FID_LRS,  // lrs | lyrics-rel-size
  // additional texts
  FID_H,    // h | hint
  FID_W,    // w | warning
  FID_ATPF, // atpf | additional-text-positioning-format
  FID_ATx,  // at<x> | additional-text-<x>
  // layout parameters
  FID_STY,  // sty | style
  FID_NP,   // np | notes-position        defaults to NOTES_POS_CENTER
  FID_LC,   // lc | lowest-compression    defaults to DEF_MIN_COMPRESSION
  FID_HC,   // hc | highest-compression   defaults to DEF_MAX_COMPRESSION
  FID_FC,   // fc | fix-compression
  // cut markers and grid
  FID_G,    // g | print-grid
  FID_GF,   // gf | grid-format
  FID_LG,   // lg | print-light-grid
  FID_CEW,  // cew | cut-edge-width       defaults to DIM_DEF_CUT_WIDTH
  FID_CEH,  // ceh | cut-edge-height      defaults to DIM_DEF_CUT_HEIGHT
  FID_CE,   // ce | print-cut-edge
  FID_CSW,  // csw | cut-side-width
  FID_CS,   // cs | print-cut-side
  FID_CACH, // cach | clip-area-center-height (above ground line)
  FID_CAH,  // cah | clip-area-height
  FID_CAW,  // caw | clip-area-width
  FID_CA,   // ca | print-clip-area
  MAX_FIELDS
};

enum field_value_types {
  FVT__NONE = 0,
  FVT_STR,      // string
  FVT_INT,      // (un)signed integer value
  FVT_BOOL,     // boolean value: 0|1|true|false
  FVT_POS,      // position: int,int
  FVT_POS_STR,  // position and string: int,int,str
  FVT_POSFMT,   // positioning format: cr|col-row |
  FVT_NOTEPOS,  // notes positioning (top | botton | center)
  FVT_NOTELEN,  // note length
  FVT_IMASK,    // index mask (list)
  FVT_STY,      // layout style
  FVT_P2PMAP,   // pitch-to-pitch map (dictionary)
  FVT_HLMODE,   // highlight mode
  FVT_PLIST,    // pitch list
};

#define MAXLEN_P2PMAP   MAX_RETUNED_NOTES
#define MAXLEN_PLIST    MAX_MARKED_PITCHES

struct FIELD_VALUE_REF {
  unsigned int id;
  unsigned int index;
  unsigned int value_type;
};

struct FIELD_P2PMAP_ITEM {
  unsigned char key;
  unsigned char value;
};
struct FIELD_P2PMAP_VALUE {
  FIELD_P2PMAP_ITEM items[MAXLEN_P2PMAP];
  unsigned int item_count;
};

struct FIELD_PLIST_ITEM {
  unsigned char value;
};
struct FIELD_PLIST_VALUE {
  FIELD_PLIST_ITEM items[MAXLEN_PLIST];
  unsigned int item_count;
};

struct FIELD_VALUE {
  unsigned char type;
  // FVT_STR, FVT_POS_STR - string
  char sval[MAX_INPUT_LENGTH + 1];
  // FVT_INT - (un)signed int
  // FVT_POSFMT - positioning format
  // FVT_NOTEPOS - notes positioning
  // FVT_NOTELEN - note length
  // FVT_STY - layout style
  // FVT_HLMODE - highlight mode
  int ival;
  // FVT_BOOL - boolean
  bool bval;
  // FVT_POS, FVT_POS_STR - position
  POSITION pval;
  // FVT_IMASK - index mask (list)
  unsigned int ilval;
  // FVT_P2PMAP - pitch-to-pitch map
  FIELD_P2PMAP_VALUE p2pmval;
  // FVT_PLIST - pitch list
  FIELD_PLIST_VALUE plval;
};


int InitFields(SESSION *session);
int FreeFields(SESSION *session);

int GetFieldValueRef(SESSION *session, const char *lpFieldName, FIELD_VALUE_REF *lpFVRef);

int ParseFieldValue(SESSION *session, FIELD_VALUE_REF *lpFVRef, const char *lpInput, FIELD_VALUE *lpValue);

bool IsPositionDefined(POSITION *pos);
bool IsMarkerDefined(MARKER *mrk);
int PrintPosition(char *str, unsigned int maxlen, POSITION *pos);

int GetFieldValue(SESSION *session, FIELD_VALUE_REF *lpFVRef, FIELD_VALUE *lpValue);
int SetFieldValue(SESSION *session, FIELD_VALUE_REF *lpFVRef, FIELD_VALUE *lpValue, unsigned int uOperator);

#ifdef PSZL_DEVELOP
int TestFields(SESSION *session);
#endif  // develop