/*
  This file is part of PS ZitherLayout.

  PS ZitherLayout is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  PS ZitherLayout is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with PS ZitherLayout.  If not, see <http://www.gnu.org/licenses/>.


  Diese Datei ist Teil von PS ZitherLayout.

  PS ZitherLayout ist Freie Software: Sie können es unter den Bedingungen
  der GNU General Public License, wie von der Free Software Foundation,
  Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren
  veröffentlichten Version, weiterverbreiten und/oder modifizieren.

  PS ZitherLayout wird in der Hoffnung, dass es nützlich sein wird, aber
  OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
  Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
  Siehe die GNU General Public License für weitere Details.

  Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
  Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
*/

/*
**  © 2011-2023 Carsten Pfeffer (PiperSoft)
**
**  @project: PS ZitherLayout
**  @file: pszl_layout.h
**  @brief definition of layout module
**
**  @date: 2011-11-14
**  @author: Carsten Pfeffer (nee Gaede)
**           Carsten Gaede
*/
#pragma once


#include "pszl_definitions.h"


// default dimensions, all in 1/10 mm
#define DIM_DEF_PAGE_HEIGHT  2100   // 21cm is paper height of A4 landscape
#define DIM_DEF_PAGE_WIDTH   2970   // 29.7cm is paper width of A4 landscape
#define DIM_DEF_PAGE_BORDER    50   // 5mm page border for cutting and so on
// page border offsets for manual calibration of tex-caused offsets
#define DIM_PBLR_OFFSET       -30   // shift 3mm to the left
#define DIM_PBTB_OFFSET         0   // no shift in top-bottom direction

#define DIM_DEF_COL_WIDTH      88   // 8.8mm string distance
#define DIM_DEF_BORDER_RIGHT  200   // 2cm distance between highest note value and right border

#define DIM_DEF_SUB_TITLE_DIST 20  // 2mm distance between title and subtitle
#define DIM_DEF_TITLE_BORDER  200 // 2cm border left and right of subtitle

#define DIM_DEF_ACC_DIST           5  // 0.5mm distance between notes and accords
#define DIM_DEF_LYRIC_NUMBER_DIST 10  // 1mm distance between lyric number and lyric text

#define DIM_DEF_CUT_WIDTH   560   // ~5.6cm cut edge width
#define DIM_DEF_CUT_HEIGHT  760   // ~7.6cm cut edge height

// default dimensions for bass style
#define DIM_DEF_ACCORD_WIDTH    220
#define DIM_DEF_ACCORD_DIST     306
#define DIM_DEF_ACCORD_C1_DIST  105
#define DIM_DEF_INFO_C1_DIST    50


/*            norm  smal  tiny
note height   7010  5608  4474
note width    2204  1763  1406
note w. dotd  3439  2751  2195
row height    4000  3000  2500  default
*/
#define DIM_DEF_MAX_ROW_HEIGHT      50  // maximum row height for compression 0
#define MAX_COMPRESSION_NORMAL_SIZE 15
#define MAX_COMPRESSION_SMALL_SIZE  22
#define DEF_MIN_COMPRESSION    0
#define DEF_MAX_COMPRESSION   28   // see MAX_COMPRESSION_xx, tiny note height is 4.4mm, so min row should be 2.2mm = 4.0mm - 1.8mm


int SetDefaultLayoutOptions(SESSION *s);
int CheckAndFixPitchRange(SESSION *s);
int GenerateLayout(SESSION *s);
