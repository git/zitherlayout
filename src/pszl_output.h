/*
  This file is part of PS ZitherLayout.

  PS ZitherLayout is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  PS ZitherLayout is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with PS ZitherLayout.  If not, see <http://www.gnu.org/licenses/>.


  Diese Datei ist Teil von PS ZitherLayout.

  PS ZitherLayout ist Freie Software: Sie können es unter den Bedingungen
  der GNU General Public License, wie von der Free Software Foundation,
  Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren
  veröffentlichten Version, weiterverbreiten und/oder modifizieren.

  PS ZitherLayout wird in der Hoffnung, dass es nützlich sein wird, aber
  OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
  Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
  Siehe die GNU General Public License für weitere Details.

  Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
  Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
*/

/*
**  © 2011-2023 Carsten Pfeffer (PiperSoft)
**
**  @project: PS ZitherLayout
**  @file: pszl_output.h
**  @brief definition of output module
**
**  @date: 2011-11-14
**  @author: Carsten Pfeffer (nee Gaede)
**           Carsten Gaede
*/
#pragma once


#include <stdio.h>

#include "pszl_definitions.h"


// fix texts (german and english version)
#define GERMAN_TEXTS
//#define ENGLISH_TEXTS

#ifdef GERMAN_TEXTS
  #define TEXT_ZITHER1      "Zither"
  #define TEXT_ZITHER2      "akkordig"
  #define TEXT_LAYOUT       "Layout"
  #define TEXT_C1_LINE      "Diese Linie muss unter der Melodiesaite C1 liegen."
  #define TEXT_ARTIST       "Musik und Text"
  #define TEXT_COMPOSER     "Musik"
  #define TEXT_TEXTER       "Text"
  #define TEXT_ARRANGEMENT  "Satz"
  #define TEXT_WARNING      "ACHTUNG"
  #define TEXT_HINT         "Hinweis"
#elif defined ENGLISH_TEXTS
  #define TEXT_ZITHER1      "Zither"
  #define TEXT_ZITHER2      "accords"
  #define TEXT_LAYOUT       "Layout"
  #define TEXT_C1_LINE      "Position this line under the melody string C1."
  #define TEXT_ARTIST       "Artist"
  #define TEXT_COMPOSER     "music"
  #define TEXT_TEXTER       "lyrics"
  #define TEXT_ARRANGEMENT  "arrangement"
  #define TEXT_WARNING      "WARNING"
  #define TEXT_HINT         "Hint"
#else
  #error no fix text definitions
#endif


int Export(FILE *lpFile, SESSION *s);
