/*
  This file is part of PS ZitherLayout.

  PS ZitherLayout is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  PS ZitherLayout is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with PS ZitherLayout.  If not, see <http://www.gnu.org/licenses/>.


  Diese Datei ist Teil von PS ZitherLayout.

  PS ZitherLayout ist Freie Software: Sie können es unter den Bedingungen
  der GNU General Public License, wie von der Free Software Foundation,
  Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren
  veröffentlichten Version, weiterverbreiten und/oder modifizieren.

  PS ZitherLayout wird in der Hoffnung, dass es nützlich sein wird, aber
  OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
  Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
  Siehe die GNU General Public License für weitere Details.

  Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
  Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
*/

/*
**  © 2011-2023 Carsten Pfeffer (PiperSoft)
**
**  @project: PS ZitherLayout
**  @file: pszl_definitions.h
**  @brief definition of default values and structures
**
**  @date: 2011-11-13
**  @author: Carsten Pfeffer (nee Gaede)
**           Carsten Gaede
*/
#pragma once

//*** definitions ************************************************************

// maximum length of input line
#define MAX_INPUT_LENGTH  1024

// maximum number of notes
#define MAX_CHORDS_PER_NOTE 8
#define MAX_NOTES_PER_GROUP 4
#define MAX_MELODY_ITEMS    256
#define MAX_MARKERS         32    // maximum number of markers
// maximum length of info fields
#define MAXLEN_SHORT_NAME 16
#define MAXLEN_TITLE 64
#define MAXLEN_SUBTITLE 256
#define MAXLEN_ARTIST 128
#define MAXLEN_ARRANGEMENT MAXLEN_ARTIST
#define MAXLEN_WARNING  128
#define MAXLEN_HINT 128
#define MAXLEN_LYRICS MAX_INPUT_LENGTH
#define MAXLEN_ADDITION  128
// maximum number of lyrics
#define MAX_LYRICS  16
// maximum number of additions (addional text entries)
#define MAX_ADDITIONS 16
// maximum number of retuned notes
#define MAX_RETUNED_NOTES 4
// maximum number of marked pitches
#define MAX_MARKED_PITCHES 16

// export data length (for MIDI)
#define MAXLEN_EXPORT_DATA_PER_SONG   8192    // <=14B per note <=256 notes, plus some extra data like track name..

// default pitch range
#define DEF_NUM_STRINGS 25  // default number of melody strings (c1..c3), may be changed by layout settings

// layout styles
#define LAYOUT_STYLE_MELODY 0
#define LAYOUT_STYLE_BASS   1


// definition of note values (pitches) (1 byte)
#define MAX_PITCH   127 // pitch range 0=C0..127=G9
#define PITCH_NONE  255

// definition of length values
#define LENGTH_NONE     0x00
#define LENGTH_INVALID  0xFF
#define LENGTH_1        0x01
#define LENGTH_2        0x02
#define LENGTH_4        0x03
#define LENGTH_8        0x04
#define LENGTH_16       0x05
#define LENGTH_32       0x06
#define LENGTH_64       0x07
//#define LENGTH_128      0x08
//#define LENGTH_256      0x09
#define LENGTH_MASK     0x3F
#define LENGTH_PT       0x80  // pointed note
//#define LENGTH_DPT      0x40  // double pointed note

// definition of melody item types
#define MI_TYPE_NOTE        1
#define MI_TYPE_SEPARATOR   2
#define MI_TYPE_GROUP       3
#define MI_TYPE__NAME(t) ( \
  ((t) == MI_TYPE_NOTE) ? "note" : \
  ((t) == MI_TYPE_SEPARATOR) ? "separator" : \
  ((t) == MI_TYPE_GROUP) ? "group" : \
  "??" \
)
  // ((t) == MI_TYPE_) ? "" :

// definition of chords
#define CHORD_VALUE_MASK  0x0F
#define CHORD_PT          0x10  // pointed chord
#define CHORD_SLR         0x20  // slured chord
#define DEF_MAX_CHORD     6     // default value, may be changed

// definition of note group types
#define GRP_TYPE_NONE     0     // normal group without specials between the notes
#define GRP_TYPE_TIE      1     // tied group

// definition of separator styles
#define SEP_STYLE_SPACE   0
#define SEP_STYLE_DOTTED  1
#define SEP_STYLE_SOLID   2

// definition of relative positions
#define RPOS_NONE   0
#define RPOS_LEFT   1
#define RPOS_TL     2
#define RPOS_TOP    3
#define RPOS_TR     4
#define RPOS_RIGHT  5
#define RPOS_BR     6
#define RPOS_BOTTOM 7
#define RPOS_BL     8
#define RPOS_L  RPOS_LEFT
#define RPOS_R  RPOS_RIGHT
#define RPOS_T  RPOS_TOP
#define RPOS_B  RPOS_BOTTOM


#define MAX_INPUT_FILES 256 // maximum number of settings and input files
#define MAX_CL_SETTINGS 16  // maximum number of command line settings items

// definition of log modes
#define LOG_MODE_QUIET    0
#define LOG_MODE_NORMAL   1
#define LOG_MODE_VERBOSE  2

// definition of output modes
#define OUTPUT_MODE_ZITHER          1
#define OUTPUT_MODE_LYRICS_NORMAL   2   // export normal lyrics (simple format)
#define OUTPUT_MODE_LYRICS_CMD      3   // export lyrics as commands
#define OUTPUT_MODE_LYRICS_CMD_ALT  4   // export lyrics as commands, alternative mode
#define OUTPUT_MODE_NOTE_CODES      5   // export PS ZitherLayout note codes
#define OUTPUT_MODE_MTX             6   // export singer notes in MTX format
#define OUTPUT_MODE_MIDI            7   // export melody in MIDI format

// lyrics/additions positioning formats and flags
#define POSFMT_NONE     0
#define POSFMT_CR       1     // col,row format, scaled with notes (default)
#define POSFMT_CR_ABS   2     // col,row absolute format, not scaled with notes
#define POSFMT_XY       3     // x,y format, absolute positions given in milli meters
#define POSFMT_MASK     0x0F
// positioning modifiers
#define POSMOD_FINE     0x10  // fine positions, same as 'basic', but in 1/10 of accordant unit

// highlight modes
#define HLM_NONE          0     // no highlight
#define HLM_COLOR       0x1     // highlight by color
#define HLM_LINE        0x2     // highlight by a line

// definition of I/O flags
#define IO_FLAG_PART      0x01
#define IO_FLAG_COMBINE   0x02


struct POSITION_FIELD {
  unsigned int ref_mrk_id;  // reference to marker ID
  int value;                // logical value or offset to referenced value
};

struct POSITION {
  unsigned int ref_mi_idx;    // reference to melody item index
  POSITION_FIELD lcol, lrow;  // logical position or offset
  int x, y;                   // absolute position
};


//*** structure definitions **************************************************

//*** settings structure definition ***
// definition of dimensions preset
#define NORMAL_DIMENSIONS 0
#define SMALL_DIMENSIONS  1
#define TINY_DIMENSIONS   2

// notes positions
#define NOTES_POS_TOP     0
#define NOTES_POS_CENTER  1
#define NOTES_POS_BOTTOM  2


struct INPUT_DEFAULTS {
  unsigned int length;        // default length if omitted, set with: \dl
  unsigned int octave;        // default octave if omitted, set with: \do
};

// dimensions in 1/10 mm
struct DIMENSIONS {
  struct {
    unsigned int height, width;                       // set with: \ph / \pw
    struct {
      unsigned int top, bottom, left, right;          // set with: \pb / \pbt / \pbb / \pbl / \pbr   (page border)
    } borders;
  } page;
  struct {
    unsigned int height, width;                       // calculated with page dimensions and page borders
    struct {
      unsigned int width;                             // calculated
      unsigned int offset;                            // calculated (if note part aligned right hand (melody style))
      struct {
        struct {
          unsigned int top, bottom;
        } c0; // dimension without compression
        unsigned int top, bottom, left, right;        // calculated, set with: \nbl / \nbr (note border left / right)
      } borders;
    } notes;
    struct {
      unsigned int width;                             // calculated
      unsigned int offset;                            // calculated (if info part aligned right hand (bass style))
      unsigned int c1_dist;                           // set with \nbr
      struct {
        unsigned int top, bottom;                     // calculated (set to notes borders)
      } borders;
      struct {
        unsigned int border;
        unsigned int width;
        unsigned int sub_title_dist;
      } title;
    } info;
    struct {
      struct {
        unsigned int height;
      } c0;
      unsigned int height;                            // fix, depending on note size (which depends on max row needed)
                                                        // may be changed later to fit all notes on the page
    } row;
    struct {
      unsigned int width;                             // set with: \sd (string distance)
    } col;
  } picture;
  struct {
    struct {
      unsigned int width;
      unsigned int width_dotted;
    } c0;
    unsigned int width;                               // note width, fix, depending on note size
    unsigned int width_dotted;                        // width of dotted note, fix, depending on note size
    unsigned int dist_grouped;                        // distance of grouped notes, fix, depending on note size
    unsigned int start_needle_size;                   // calculated with note width
    unsigned int circle_size;                         // calculated with note width
    int dx;                                           // calculated with note width
    int dy;                                           // fix, depending on string distance
    struct {
      unsigned int dist;
      int dx_l;                                       // calculated with note width
      int dx_r;                                       // calculated with note width dotted
      int dy_b;                                       // calculated with note height
    } chord;
  } notes;
  struct {
    unsigned int number_dist;
  } lyrics;
  struct {
    unsigned int height, width;                       // set with \ceh/\cew, (cut edge height / width) default available
  } cut_edge;
  struct {
    unsigned int width;                               // set with \csw (cut edge width, default: 0 = not shown)
  } cut_side;
  struct {
    unsigned int height, width;                       // set with \cah/\caw (cut area height / width)
    unsigned int center_height;                       // set with \cach (cut area center height), horizontal position above ground line
  } clip_area;
  struct {
    unsigned int width;                               // set with \cw (chord width)
    unsigned int c1_dist;                             // set with \cd (chord (to c1) distance)
    unsigned int dist;                                // set with \sd
  } chords;
};

struct COUNTERS {
  unsigned int cols;                                   // set with: \sc (string count)
  struct {
    unsigned int rows;
  } c0;
  unsigned int rows;                                   // calculated with pic.h, nbt,nbb and height
  unsigned int chords;                                 // set with: \cc (chord count)
};

struct RETUNED_NOTE {
  unsigned char old_pitch;    // old original pitch as noted and shown in zither note sheet; PITCH_NONE: retuning entry not set
  unsigned char new_pitch;    // new pitch as audible and shown in singer notes
  bool in_use;                // old pitch is actually used in the song
  POSITION pos;
};

struct RETUNED {
  RETUNED_NOTE notes[MAX_RETUNED_NOTES];
  unsigned char highlight_mode;     // highlighting mode for retuned notes, @see HLM_.., set with @hr
};

struct PITCH_MARK {
  unsigned char pitch;        // pitch to be marked; PITCH_NONE: entry not set
  POSITION pos;
};

struct PITCH_MARKS {
  PITCH_MARK marks[MAX_MARKED_PITCHES];
};

struct SETTINGS {
  unsigned int layout_style;                           // set with \sty
  struct {
    char *items[MAX_CL_SETTINGS];
    unsigned int count;
  } cmd_line;
  INPUT_DEFAULTS defaults;
  bool print_grid;   // print grid?   // set with \g
  unsigned char grid_fmt;             // grid format, taken from lpf by default; set with \gf
  bool print_light_grid;              // set with \lg
  bool print_lyric_numbers;           // set with \ln
  bool print_cut_edge;                // set with \ce
  bool print_cut_side;                // set with \cs
  bool print_clip_area;               // set with \ca
  DIMENSIONS dim;
  COUNTERS count;
  RETUNED retuned;
  PITCH_MARKS pitch_marks;
  unsigned int notes_position;                         // set with: \np (center is default)
  struct {
    unsigned int min;                                  // set with: \lc/\fc (lowest compression), default 0
    unsigned int max;                                  // set with: \hc/\fc (highest compression), default depending on lc and some dimensions
    unsigned int current;
  } compression;
};

struct MI_BASE {
  unsigned int idx;     // melody item index
  unsigned char type;   // item type, @see MI_TYPE_..
};
struct NOTE { // note and pause
  MI_BASE base;
  // input values
  bool is_pause;                                // is pause? otherwise is note
  unsigned char length;                         // note length and point flag
  unsigned char pitch;                          // note value (pitch, 0..127), PITCH_NONE: no note here, next note maybe start of new song part
  unsigned char chords[MAX_CHORDS_PER_NOTE];    // chord value and type flag
  unsigned char chord_count;                    // number of chords
  // layout values
  unsigned char dir;                            // up, down, turn, stay
  POSITION pos;                                 // position
  POSITION start_needle_pos;                    // calculated from note position if note is first (in song part)
  unsigned char line_pos_in;                    // relative position of line coming from previous note
  unsigned char chord_pos;                      // relative position of chord
};
// grouped note is the same, but with max. one chord

struct NOTE_GROUP { // group of notes and pauses
  MI_BASE base;
  unsigned char type;     // note group type, @see GRP_TYPE_..
  NOTE notes[MAX_NOTES_PER_GROUP];
  unsigned int note_count;
  // each note may have only one chord
  // layout parameters for group are stored in first note
  int note_pos_x[MAX_NOTES_PER_GROUP];
};

struct SEPARATOR {  // separator
  MI_BASE base;
  // input values
  unsigned char size;       // size, default: 1
  unsigned char style;      // style, see SEP_STYLE_..
  unsigned char line_pos;   // line position from top, default: 1
  // layout values
  POSITION pos;             // position
};


//*** song structure definition ***
struct MELODY_ITEM {
  union {
    MI_BASE base;
    NOTE note;      //  '[N]..',  MI_TYPE_NOTE
    // same for         'P..',    MI_TYPE_PAUSE
    SEPARATOR sep;  //  'S..',    MI_TYPE_SEPARATOR
    NOTE_GROUP grp; //  '{'..'}', MI_TYPE_GROUP
  };
};
struct MELODY {
  MELODY_ITEM items[MAX_MELODY_ITEMS];
  unsigned int item_count;
};

struct MARKER {   // marker
  unsigned int id;  // marker ID
  POSITION pos;     // position - may be reference to another marker or a melody item, maybe with absolute offset
};

struct LYRIC {
  char text[MAXLEN_LYRICS + 1];       // set with @l<x> / @lr
  bool visible;                       // set with \lv<x>=1/0 or \lv=<x>,<x2>...
  POSITION pos;                       // set with \lp<x> & \lpr, real coordinates calculated
};
struct LYRICS {
  LYRIC lyrics[MAX_LYRICS];
  unsigned int max_num;
  LYRIC refrain;                     // set with @lr
  int rel_size;                     // relative lyrics text size; set with \lrs|lyrics-rel-size
  unsigned char pos_fmt;            // positioning fomat, see LAPF_...; set with \lpf|lyrics-positioning-format
};
struct ADDITION {
  char text[MAXLEN_ADDITION + 1];             // set with \at<x>=pos,pos,text
  POSITION pos;                      // set with \at<x>=pos,pos,text; real coordinates calculated
};
struct ADDITIONS {
  ADDITION adds[MAX_ADDITIONS];
  int rel_size;                   // relative text size; set with \atrs|additional-text-rel-size
  unsigned char pos_fmt;          // positioning format, see LAPF_...; set with \atpf|additional-text-positioning-format
};
struct SONG {
  MELODY melody;
  MARKER *markers[MAX_MARKERS];
  int transpose;                           //  \tr=<x>
  char short_name[MAXLEN_SHORT_NAME + 1];   // set with @sn (@short_name)
  char title[MAXLEN_TITLE + 1];             // set with @t(itle)
  char subtitle[MAXLEN_SUBTITLE + 1];       // set with @s(ubtitle)
  struct {
    char composer[MAXLEN_ARTIST + 1];         // set with @a(rtist) or @c(omposer)
    char texter[MAXLEN_ARTIST + 1];           // set with @a(rtist) or @te(xter)
    char arrangement[MAXLEN_ARRANGEMENT + 1]; // set with @arr(angement)
  } artists;
  char warning[MAXLEN_WARNING + 1];         // set with @w(arning)
  char hint[MAXLEN_HINT + 1];               // set with @h(int)
  LYRICS lyrics;                     // set with @l<x>, \lp<x>
  ADDITIONS add;                     // set with @at<x>
  unsigned char rows_needed;                // calculated from notes
  // export data
  unsigned char export_data[MAXLEN_EXPORT_DATA_PER_SONG];
  unsigned int export_data_len;
};


//*** session structure ***

struct SESSION {
  struct {                    // -s --settings-file   -i --input-file
    struct {
      char *lpFileName;
      bool is_settings_file;
    } files[MAX_INPUT_FILES];
    unsigned int uFileCount;
    int first_input_file_idx;
    bool bReadStdIn;
    char temp_buffer[MAX_INPUT_LENGTH + 1];
  } input;
  struct {
    unsigned char mode;       //  -O  --output-mode
    char *lpFileName;         //  -o  --output-file
  } output;
  unsigned int uLogMode;      //  -q  --quiet  -v  --verbose
  unsigned int uIOFlags;      //  -p  --part  -c  --combine
  bool printHelp;             //  -h  --help
  SETTINGS set;
  SONG song;
  MARKER markers[MAX_MARKERS];
  struct FIELD_STORE *field_store;
};


//*** generic macros ***

#define MAX(a,b)  ((a)>(b)?(a):(b))
#define MIN(a,b)  ((a)<(b)?(a):(b))


//*** global return codes ***
#define RC_OK   0
#define RC_WARN 1   // warned, but continue
#define RC_ERR  (RC_ERR_BASE - __LINE__)  // error, break
#define RC_IS_OK(rc) ((rc) >= 0)
#define RC_IS_ERR(rc) ((rc) < 0)

#define RC_ERR_BASE_PSZL      -10000
#define RC_ERR_BASE_FIELDS    -20000
#define RC_ERR_BASE_INPUT     -30000
#define RC_ERR_BASE_LAYOUT    -40000
#define RC_ERR_BASE_OUTPUT    -50000
