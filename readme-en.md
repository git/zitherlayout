# PiperSoft ZitherLayout
PiperSoft ZitherLayout is a small tool to layout songs to get Zither-style note sheets, to be put underneeth the strings.
Therefore the program reads inputs and notes from simple text files and creates a LaTeX file. \
This file can be compiled with LaTeX, where MusixTex is needed.

## Usage
The tool is called like this:
```
PS_ZitherLayout [-q|-v] [-p|-c] [-s <setfile>]* [-S <set>]* [-i <infile>*]* [-O <outmode>] [-o <outfile>]
```
(`*` means any number of such parameter) \
Each option has a short and a long form. Like common for Linux, the short forms use a single dash (`-`), while the long forms use a double dash.

### Program Arguments
The parameters have the following meaning:
* `-q  --quiet` \
    run in quiet mode \
    only errors and warnings are printed to stderr \
    if neither `-v` nor `-q` is given, the program runs in 'normal' mode printing some process information (and of course errors and warnings)
* `-v  --verbose` \
    run in verbose mode \
    additional information is printed
* `-p  --part` \
    export part document only, parts can be combined later using the `-c` (`--combine`) option
* `-c  --combine` \
    combine multiple part documents (given by `-i` options) into one export document
* `-s  --settings-file  <setfile>` \
    specify settings file (read as normal input file, see [Input File Format](#input-file-format)) \
    `<setfile>` is the name of the settings file
* `-S  --set  <set>` \
    specify field value \
    field values given on command line overwrite accordant values from input files, so they are useable for 'latest changes' or temporary settings (like 'g=1') \
    `<set>` is the field name and the value, see [Field Value Input Format](#field-value-input-format)
* `-i  --input-file  <infile>*` \
    specify input file \
    if no input file is given (but maybe one or more settings files), stdin is read in addition \
    `<infile>` is the name of the input file, multiple files can be specified
* `-O  --output-mode  <outmode>` \
    specify output mode, see [Output Formats](#output-formats) \
    `<outmode>` is the output mode: \
    * `z` | `zither` : creates [Zither-Style Notes](#zither-style-notes) (default)
    * `l` | `lyrics` : creates [Lyrics in normal Mode](#normal-mode) \
        *replaces obsolete option `-l 0` (`--lyrics-mode 0`)*
    * `lc` | `lyrics-cmd` : creates [Lyrics as LaTeX-Kommandos](#latex-commands) \
        *replaces obsolete option `-l 1` (`--lyrics-mode 1`)*
    * `lca` | `lyrics-cmd-alt` : creates [Lyrics as LaTeX-Commands for alternative Texts](#latex-commands-alternative-lyrics) \
        *replaces obsolete option `-l 2` (`--lyrics-mode 2`)*
    * `nc` | `note-codes` : creates [Note Codes](#note-codes) in text format (e.g. after transposition)
    * `mtx` : creates notes in M-TX format, e.g. for creation of [Singer-style Notes](#singer-style-notes)
    * `midi` : creates [MIDI file](#midi-files)
* `-o  --output-file  <outfile>` \
    specify output file \
    if no output file is given, use stdout \
    `<outfile>` is the name of the output file
* `-h  --help` \
    print short usage and exit

Input and settings files are read in the order given at the command line. Each input file overwrites settings made from the files before, but adds! notes. \
If there is a file `~/.PiperSoft/ZitherLayout/default-settings`, this file will be read before any of the given input and settings files.

In default mode the programm will create the file `<file>.tex` (if specified by `-o=<file>.tex`).
To create a PDF file containing the Zither-style notes you can call the command `pdflatex <file>` afterwards, where `<file>` is the name of the output file (see Option `-o`) without extension.


### Input File Format

Each input file may contain notes and field values, where field values are song information like lyrics or the title, and settings like lyrics positions, string distance and many more. \
Each line can consisti of one or more items seperated by white space (space, tabulator). An exception are lines starting with `@` (see below), where white space characters are part of the value (which is important for lyrics, for example). \
Each line starting with `;` is ignored, so represents comments, or just something you don't want to delete, but you also don't want to be printed right now. \
Each line starting with `\` or `@` contains field values, so settings or song information. If the line starts with `\`, field values end at the next white space, if the line starts with `@`, the field value ends at the end of the line. \
All other lines are melody lines containing notes, pauses, note groups and separators. \
Each input line can contain `;`, the rest of the line is ignored as a comment. An exception is a line starting with `@` again, where the `;` and the following text will be part of the field value. \
A special field `end` marks the end of the input file, and can be used on stdin for example.

Lines starting with `#` have a special meaning, those can be used to generate a warning (`#warning <message>`) or an error message (`#error <message>`) like known from C programming language. In case of an error (`#error`) PS ZitherLayout will terminate with an error code, in case of a warning the programm will continue.

In the following format descriptions `<xx>` represents a value, `[xx]` an optional parameter and `x|y` possible values of the field.


### Note Input Format
Each note item contains of note value, the pitch and optionally a chord and has the following format:
```
[N][<length>][.][+|-]<pitch-name>[<octave>][(C|A)<chord>[.|_]]*
```
where the fields have following meanings:
* `[N]` : optional mark of a note, see also [Pause Notation](#pause-notation), [Separators](#separation-of-song-parts) and [Markers](#using-markers-referring-to-melody-items)
* `[<length>]` : base length of note value, one of `1` (whole note), `2` (half note), `4` (quarter), `8`, ... \
    set to default length, if omitted *)
* `[.]` : if present, produces a dotted note
* `[+|-]` : rise / lower the given pitch by semitone
* `<pitch-name>` : name of pitch (`a`-`h`) \
    *Pitch names use **GERMAN** notation, so `h` will produce english `b`, `b` is the same as `-h` or english `-b`!*
* `[<octave>]` : octave (`1`-x) \
    set to default octave, if omitted *)
* `C|A` : marks the presence of a chord (ger: Akkord)
* `<chord>` : chord number (`1`-x)
* `[.|_]` : specifies the chord type (produces dotted or underlined chord number)
* `*` multiple chords may be specified

*) Default length is 4 (quarter note), default octave is 1, but these values may be changed by specific commands, see `dl` / `do`. \
Which pitches (octaves) can be used depends on the lowest and highest pitch, default range is `c1` to `c3`.

#### Samples
Code            | Result
----------------|------------------------------------------------
`4g1`           | quarter note at first g pitch
`2.e2A2`        | dotted half note at second e pitch with chord 2
`32-h1A3_`      | 1/32 note at first b (=bh, = #a) pitch with underlined chord 3
`c d e f g`     | beginning of the c scale with default note values in the default octave
`2.e1A1A1.A1.`  | dotted half note at pitch e1 with 3 chords to be played

### Pause Notation
Pauses (rests) can be specified in the following format:
```
[N][<length>][.][+|-]<pitch-name>[<octave>][(C|A)<chord>[.|_]]*
```
where `P` markes a pause, all other fields are the same as for 'normal' notes.

The pitch is required for positioning the pause sign, also chords can be played during a pause (in the melody).

#### Samples
  Code          |   Result
----------------|-------------------------------------------------
`P4g1`          | quarter pause at pitch g1
`P2.e2A2`       | dotted hafl pause at second e pitch with chord 2

### Grouping Notes and Pauses
If a melody contains a sequence multiple notes of the same pitch, those can be grouped. In that case they will be placed at the 'same' location side-by-side in the Zither-style layout. This is to avoid the usage of too many rows since otherwise horizontal alignment of those notes is not possible due to the same pitch.
Grouped notes and pauses (almost) need the space of a single note of the same pitch. \
Note groups (which also can contain psuses) are specified using the following format:
```
{ <note|pause>* }
```
so notes and pauses to be grouped are put into curly braces. Notation of the single notes and pauses remains unchanged, with the following restrictions:
* all notes and pauses of a group need to have the same pitch
* each note or pause may specify only a single chord

#### Grouped Tied Notes
If the notes of a group form a single tone, they can be connected by a tie. To achieve this, a note group can be created again (see above), but the curly braces are replaced by normal parentheses:
```
( <note|pause>* )
```

Tie groups are used, if the required note length cannot be noted in another way, e.g. if a quarter note is followed by a whole note, without attacking the string again.

#### Samples
  Code             |   Ergebnis
-------------------|-----------------------------------------------------------------
`{ 8g1 8g1 8g1 }`  | three grouped eigth notes of pitch g1
`{ 2.e1A2 4e1A1 }` | group of a dotted half and a quarter note of pitch e1, each with a separate chord
`( 2.e1A2 4e1A1 )` | group of the same notes, combined by a tie

### Separation of Song Parts
Separators can be used to visually split multiple parts of a song. A separator is a horizontal free space without any notes.

The following format is used for separators:
```
S[<height>][<linestyle>[<position>]]
```
where the single fields have the following meaning:
* `S` : marks a separator
* `[<height>]` : height of the separator, default value is 1 \
    \>1, if a line is requested
* `[<linestyle>]` : style of the separation line:
    * `s` : *(space)* do not print a separation line (default)
    * `l` : *(line)* print a solid line
    * `d` : *(dotted line)* print a dotted line
* `[<position>]` : position of the separation line inside the separator, default is 1 (at the top) \
    has to be less than the height of the separator

Separators can e.g. be used to separate the notation of stropes and refrain of a song.

#### Samples
  Code          |   Result
----------------|------------------------------------------------
`S`             | empty separator (height 1)
`S5d`           | separator with height 5 with dotted line at the top
`S4l3`          | separator with height 4 with solid line at the bottom

### Using Markers Referring to Melody Items
Markers can be used as a kind of reference to the position of a melody item (a note or a note group, a pause or a separator). Those markers have a unique ID, and can later be used to position other elements like additional texts or even lyrics relative to the referenced melody item.

A marker in a melody always refers to the position of the **following** melody item.

The following format is used for markers in a melody:
```
M<id>
```
where the single fields have the following meaning:
* `M` : starts a marker definition
* `<id>` : is the marker ID

#### Samples
  Code          |   Result
----------------|------------------------------------------------
`M1 4g1 M2 4a1` | defines two markers `1` and `2` at different note positions
`4e1 M3 M4 4d1` | defines two markers `3` and `4` at the same note position (the one from `4d1`)


### Field Value Input Format
To specify field values (song information or settings), the input line has to start with `\` or `@`, see above. Each entry has to be given in the following format:
```
<name>[op]=<value>
```
The fields have the following meanings:
* `<name>` : name of field to set, see below \
    each field has a short and a long form, both handled the same way \
    if and unknown field name is specified, a warning is given, and the program ignores the entry.
* `[op]` : optional operator \
    the following operators are supported:
    * `+`  given value will be added to the current field value (strings will be concatenated)
    * `-`  given value will be subtracted from the current field value (numerical values only)
* `<value>` : value to set field to \
    using the `@<name2>` or `^<name2>` notation allows to reference the value or a component of any field, see below \
    *(referencing by `\<name2>` does not work, since LaTeX commands are commonly used, which also start with `\`)*

#### Field References
When defining field values it is possible to re-use the (complete) value of another field by using the `@<name2>` notation for the value. This will copy the value from field named `name2`.

In some cases it is required to only take a component of another field's value, this is possible by using the `^<name2>` notation. This will only set one component of the field to the same component of the field named `name2`. \
This way it is for example possible to define a lyrics position re-using the column of one field and re-using the row of another field.

#### Samples
Code | Result
-----|--------s
`@l1=this is the first lyrics` | defines the lyrics for the first strophe
`\lp1=1,1`                     | defines the position of the first strophe
`\ l2=@l1 l2+=again`           | sets the second strophe to the text of the first strophe, which is extended afterwards
`\lp2=@lp1 lp2+=0,2`           | defines the position of the second strophe's lyrics exactly 2 rows below the first
`\lp3=@M1`                     | defines the position of the third strophe's lyrics to the position of marker `1`
`@at1=^lp1,5,some text`        | defines the first additional text item, printing `some text` at the column of the first stroph's position and the fixed row `5`
`@at2=2,6,^at1`                | defines the second additional text item, printing the same text again at another (fixed) position


#### Fields
The following fields can be used:
1. special fields:
    * `end` : end of input file \
        marks the end of the input file (usefull for breaking read on stdin)
    * `in` | `input-file` : input file name \
        read given file as additional input file \
        `<value>` is a relative file name (to location of current input file)
    * `tr` | `transpose` : transpose a song \
        transpose all note pitches up (>0) or down (<0) by the given value \
        `<value>` is the number of half steps to transpose \
        if a note pitch is out of the valid range during transposition, a warning is generated and the note is shifted by one octave
2. fields for the page layout *):
    * `pb` | `page-borders` : page borders \
        change page border (default is 5mm at each side) \
        `<value>` is the border width in 1/10mm \
        changes all borders (top, bottom, left and right)
    * `pb(t|b|l|r)` | `page-border-(top|bottom|left|right)` : page border \
        only changes top / bottom / left / right border
    * `nb(l|r)` | `note-border-(left|right)` : note border \
        change note border width (left default depends on string distance, right defaults to 20mm) \
        `<value>` is border width in 1/10mm
3. fields for the Zither layout *):
    * `sc` | `string-count` : number of strings \
        change string count (default is 25 for range c1 to c3) \
        `<value>` is the number of melody strings (numeric)
    * `sd` | `string-distance` : string distance \
        change string distance (default is 8.8mm for 3 1/2 Zither) \
        `<value>` is string distance in 1/10mm
    * `cc` | `chord-count` : number of chords \
        change number of chords (chord count, 6 is default) \
        `<value>` is the number of chords (numeric)
    * `cd` | `chord-distance` : distance between chords \
        change chord distance \
        `<value>` is the distance in 1/10mm
    * `cw` | `chord-width` : width of a chord \
        change the width of one chord \
        `<value>` is the width in 1/10mm
4. fields for default values:
    * `dl` | `default-length` : default length of a note \
        specify default note value (length) \
        `<value>` is one of 1,2,4, ... (see above)
    * `do` | `default-octave` : default octave \
        specify default octave \
        `<value>` is one of 1,2,...
5. fields for pitches and retuning:
    * `r` | `retune` : retuned strings \
        specify original and new pitch of retuned strings, see [how to use it](#how-to-use-retuned-strings) \
        `<value>` is a list of `<old>[:<new>]` pitch pairs \
        if `<new>` pitch is not specified or `none`, retuning entry for `<old>` pitch is removed \
        notation for the pitches is `[+|-]<pitch-name><octave>` as specified [above](#note-input-format) \
        for the new pitch also octave `0` is supported
    * `hr` | `highlight-retuned` : highlight retuned strings \
        speficy if and how retuned strings are highlighted \
        `<value>` is the highlighting mode:
        * `n` | `none` : do not highlight retuned strings
        * `c` | `color` : color retuned notes
        * `l` | `line` : draw vertical lines for retuned strings
        * `cl` | `color-and-line` : combination of colored notes and vertical lines
    * `pm` | `pitch-marks` : mark strings \
        specify pitches to be marked and labeled \
        `<value>` is a list of pitches \
        notation for the pitches is `[+|-]<pitch-name><octave>` as specified [above](#note-input-format)
6. fields for song information:
    * `sn` | `short-name` : short name of the song
    * `t` | `title` : song title
    * `s` | `sub-title` : subtitle
    * `a` | `artist` : artist (music and lyrics)
    * `c` | `composer` : composer of the song (music)
    * `te` | `texter` : texter of the song (lyrics)
    * `arr` | `arrangement` : arrangement
7. markers:
    * `m<x>` | `M<x>` | `marker-<x>` : position of marker \
        set the marker to a fixed position (or by reference to another marker)
        `<x>` is the marker ID \
        *the `M<x>` notation is supported to allow the same notation as used when defining [markers refering to melody items' positions](#using-markers-referring-to-melody-items)*
8. fields for lyrics:
    * `l<x>` | `lyrics-<x>` : strophe lyrics \
        lyrics for x-th strophe, x is in 1 to 16 (I think more than 16 strophes won't have place on the sheet...) \
        `<x>` is the number of the strophe \
        `<value>` is the text
    * `lp<x>` | `lyrics-position-<x>` : position of strophe lyrics \
        set lyrics position for x-th strophe (see [How to Position Lyrics](#how-to-position-lyrics))
        `<x>` is the number of the strophe \
        `<value>` is the position in the format `<col>,<row>` \
        depending on the format (see `lpf`) these are relative or absolute postion values
    * `lr` | `lyrics-refrain` : refrain lyrics \
        set the lyrics of the refrain \
        `<value>` is the text used as refrain lyrics
    * `lpr` | `lyrics-position-refrain` : position of refrain lyrics \
        set the position of the refrain \
        `<value>` is the position in `<col>,<row>` format
    * `ln` | `show-lyric-numbers` : lyric numbering \
        print / omit lyric numbers \
        `<value>` is either `1`|`true` (print numbers) or `0`|`false` (do not)
    * `lpf` | `lyrics-positioning-format` format of lyric positions \
        change the format of lyrics position field values \
        `<value>` is the format, and is one of:
        * `cr` | `col-row` : position given in column and row, relative to notes' positions
        * `cra` | `col-row-absolute` :position given in column and row, but independent from the current compression level
        * `xy` : position given in absolute x-y-coordinates
        * `<x>f` | `<x>-fine` : with higher accuracy, scaled by 1/10th of the basic unit
    * `lv<x>` | `lyrics-visible-<x>` : lyrics visibility \
        change visibility of a strophe's lyrics
        `<x>` is the strophe number \
        `<value>` is either `1`|`true` (show lyrics) or `0`|`false` (don't show) \
        as default all lyrics will be printed
    * `lv` | `lyrics-visible` : lyrics visibility \
        change vilibility of a set of strophes' lyrics \
        `<value>` is a list of strophe number to show / hide \
        depending on the optional operator `<op>` the behaviour changes:
        * `=` : only the given strophes will be printed later
        * `+=` : the given strophes's lyris will be printed, additionally the already marked
        * `-=` : the given strophes will not be printed
    * `lrs` | `lyrics-rel-size` : relative text size for lyrics \
        change the relative text size of lyrics
        `<value>` is the relative size (<0: use smaller font, >0: use larger font)
9. fields for additional texts:
    * `h` | `hint` : hint
    * `w` | `warning` : warning \
        (maybe used to say e.g. 'you have to change chord tuning before you play this!') \
        text will be displayed in red
    * `atpf` | `additional-text-positioning-format` : format of the positions of additional texts \
        `<value>` is the format, see `lpf` | `lyrics-positioning-format`
    * `at<x>` | `additional-text-<x>` : additional text including its position \
        `<value>` is the position and the text in the format `<col>,<row>,<text>`
10. fields for layout parameters:
    * `sty` | `style` : output style \
        `<value>` is the style, the following styles are available:
        * `m` | `melody` : melody-style, print note pages for melody strings
        * `b` | `bass` : bass-style, print note pages for bass strings (in preparation)
    * `np | notes-position` : note position \
        change position of notes (see [How to Position Notes](#how-to-position-notes))
        `<value>` is the position:
        * `t` | `top` : position notes on top of the page
        * `c` | `center` : center notes vertically
        * `b` | `bottom` : position on the bottom
    * `(l|h|f)c | (lowest|highest|fix)-compression` : compression \
        change minimum, maximum or fixed compression (see [How to Position Notes](#how-to-position-notes))
        `<value>` is the compression level (numeric) \
        `fc` sets minimum and maximum compression to the same value
11. fields for cut markers and helper grid:
    * `g` | `print-grid` : helper grid \
        print/omit grid \
        `<value>` is either `1`|`true` (print grid) or `0`|`false` (do not) \
        you can use the grid to find the best lyric positions (see [How to Position Lyrics](#how-to-position-lyrics))
    * `gf` | `grid-format` : helper grid format \
        change format of the helper grid \
        `<value>` is the format, see `lpf` | `lyrics-positioning-format` \
        in col/row format column and row numbers are printed \
        in x/y format absolute coordinates are printed
    * `lg` | `print-light-grid` : light, reduced helper grid \
        print/omit light helper grid \
        `<value>` is either `1`|`true` (print grid) or `0`|`false` (do not) \
        the light grid uses thinner lines without coordinate labels
    * `cew` | `cut-edge-width` : width of cut edge \
        set width of cut edge \
        `<value>` is width in 1/10mm (from pictures right edge, see green frame of helper grid)
    * `ceh` | `cut-edge-height` : height of cut edge \
        set height of cut edge \
        `<value>` is heigth in 1/10mm (from pictures top edge, see green frame of helper grid)
    * `ce` | `print-cut-edge` : print cut edge \
        print/omit cut edge \
        `<value>` is either `1`|`true` (print cut edge) or `0`|`false` (do not) \
        the cut edge is automatically printed with the helper grid
    * `csw` | `cut-side-width` : right-side cut width \
        set width of right border to be cut \
        `<value>` is the width of that border in 1/10mm
    * `cs` | `print-cut-side` : print right-side cut marker \
        print or ommit cut side \
        `<value>` is `1`|`true` (print) or `0`|`false` (don't print)
        the right-side cut marker is automatically printed with the helper grid
    * `caw` | `clip-area-width` : width of clip area \
        set width of rectangle around clip area \
        `<value>` is the width in 1/10mm (from pictures right edge, see green frame of helper grid)
    * `cah` | `clip-area-height` : height of clip area \
        set height of rectangle around clip area \
        `<value>` is the height in 1/10mm
    * `cach` | `clip-area-center-height` : horizontal position of clip area \
        set horizontal position of rectangle around clip area \
        `<value>` is the height in 1/10mm (from pictures bottom edge, see green frame of helper grid, to the center of the clip area)
    * `ca` | `print-clip-area` : print clip area cut marker \
        print or ommit clip area \
        `<value>` is `1`|`true` (print) or `0`|`false` (don't print)
        the clip area cut marker is automatically printed with the helper grid

*) Hint: The commands for page layout change can be put into a page-layout specific settings file (like `page-layout-A4ls.conf`), the commands for zither layout can be put into a zither specific file (like `zither-layout-3,5.conf`). This way you can easily swap between one or more page layouts and zither layouts by specifying these files on the command line, see [Usage](#usage).


## How to Position Notes
When you create a note sheet to be put under your zither's strings, you have to keep in mind you may have to cut the upper right edge of the sheet.

For this purpose, there is a cut edge marking line printed with the helper grid. You can use this line to look for notes positioned in the area that will be cut. If there are such notes, you should tell PS ZitherLayout to position them outside of the cut area.

This can be done by using a higher compression (see commands `lc` or `fc`) and aligning the notes at the bottom of the page (see `np=b` command). This way you will get some free space on top of the page. You should just play around with the parameters for `lc` or `fc` to find the best compression rate.


## How to Position Lyrics
If you want to include lyrics into your note sheet, you have to specify the lyrics itself and their positions. \
The same applies to additional text items.

To get possible positions, just create the sheet without lyrics, print the grid (see command `g=1`) and look for free space on the sheet.

Then specify the lyrics positions in your input file (using commands `lp<x>=<pos>`) and remove the grid (omit `g=1` command or use `g=0`). \
So you need at least two steps, but feel free to use even more iterations to find the best positions...

The input format of lyrics positions and positions of additional texts can be changed using the `lpf` and `atpf` fields. This way it is possible to use either relative positions (relative to the prited notes), or absolute positions in form of x-y-coordinates. The `fine` mode allows specifing positions in 1/10th of the accordant basic units.

For positioning lyrics or additional text items relative to melody items like notes, pauses or separators, you can define markers in the melody, and refer to such markers when specifying the lyrics or alternative text positions, see above.


## How to Use Retuned Strings
For some songs you need pitches, that are not in the default pitch range of the Zither, a common sample is the B or Bb just below the lower C (`c1`). Those pitches can be used and played, if unused strings are retuned to those pitches, e.g. the pitch D# (`+d1`) is often not required (at least in common christmas songs in Germany), and can be retuned to B (`h0`) or Bb (`-h0`) as required. \
*Remember that pitch names are german names, see [above](#note-input-format).*

If a song contains retuned strings, an accordant warning on the note sheet should inform about it. \
In addition to this it could be usefull to also formally define retuned strings, so that e.g. also MIDI and MTX output can be generated with the correct pitches. For this definition the field `r`|`retune` is used. \
If retuned strings are formally defined, retuned notes and strings can be highlighted on the note sheet, in addition to the warning mentioned above. For this the field `hr`|`highlight-retuned` can be set.

### Samples
command      | description
-------------|-------------
`@r=+d1:-h0` | retune string D# to Bb below lower C
`@hr=c`      | color all notes noted as D#, but sounding like Bb
`@hr=l`      | highlight retuned D# string by a vertical linehervorheben
`@hr=cl`     | combine highlight of retuned notes and strings


## Output Formats
PS ZitherLayout support different output formats that can be selected with the output mode (parameter `-O`|`--output-mode`), see [Program Arguments](#program-arguments). The default is the generation of Zither-style notes (mode `z`|`zither`).

The following sections the different output formats are described, accordant samples are linked.
In addition to those samples there is a [makefile](samples/sdgnk/makefile), that contains the required steps to create the sample files.

### Zither-style Notes
Zither-style notes are a speficic format, that can be put below the strings of a Zither, so that the melody can be played quite simple.

As a quite simple example here is a quite common german christmas song: ![](samples/sdgnk/sdgnk_znG.png "Süßer die Glocken nie klingen")

PS ZitherLayout itself generates (in output mode `z`|`zither`) a [LaTeX source file](samples/sdgnk/sdgnk_znG.tex), which can be compiled into [PDF document](samples/sdgnk/sdgnk_znG.pdf) using `pdflatex`.

With the help of additional program arguments (`-p`|`--part` and `-c`|`--combine`) it is possible to change the output format, so that multiple Zither-style note sheets can be combined in one PDF document.

### Lyrics

#### normal mode
In normal lyrics mode (output format `l`|`lyrics`) the output is a simple [LaTeX document](samples/sdgnk/sdgnk_l.tex), which can be compiled to a [PDF](samples/sdgnk/sdgnk_l.pdf) using `pdflatex`. This document contains the lyrics in a quite simple format.

#### LaTeX commands
Instead of this simple format the lyrics can also be generated as [LaTeX commands](samples/sdgnk/sdgnk_lc.tex) (output mode `lc`|`lyrics-cmd`). The generated document contains all song information like title and also the lyrics as parametrs to specific LaTeX commands. this allows, especially in the part mode (parameter `-p`|`--part`), to adapt the visual style as you like. If the part mode is not used, default definitions of the commands will be generated, so that the output can again be compiled into a [PDF document](samples/sdgnk/sdgnk_lc.pdf) using `pdflatex`.

#### LaTeX commands (alternative lyrics)
The LaTeX mode for alternative lyrics (output mode `lca`|`lyrics-cmd-alt`) is nearly identical to the 'normal' LaTeX mode, but it generates [alternative LaTeX commands](samples/sdgnk/sdgnk_lca.tex). This allows displaying alternative lyrics in a diffent way, e.g. in a larger text collection document.

#### commands in LaTeX modes
The output document can contain the following LaTeX commands:
command  | alt       | description / content
---------|-----------|-----------------------
`\ZLt`   | `\ZLat`   | song title
`\ZLs`   | `\ZLas`   | subtitle
`\ZLi`   | `\ZLai`   | song information (composer, arrangement, ..)
`\ZLl`   | `\ZLal`   | lyrics environment
`\ZLlof` | `\ZLalof` | lyrics environment for a single strophe
`\ZLlf`  | `\ZLalf`  | lyrics of the first strophe
`\ZLlr`  | `\ZLalr`  | refrain
`\ZLlx`  | `\ZLalx`  | lyrics of the 2nd to `<x>`th strophe

### Note Codes
In addition PS ZitherLayout supports generating the notes in its own input format (Ausgabemodus `nc`|`note-codes`). This output contains the [note codes](samples/sdgnk/sdgnk_nc.txt), that can be used as an input to PS ZitherLayout itself. This might be helpful if e.g. songs are transposed (field `tr`|`transpose`), if the result of the transpisition needs to be re-stored to a file.

### Singer-style Notes
The output mode for singer-style notes (output mode `mtx`) generates notes in [MTX format](samples/sdgnk/sdgnk_mtx.mtx). This format can be converted to the more complicated MusiXTeX format using `prepmx` and `pmxab`. The latter can be embedded into a [LaTeX document](samples/sdgnk/sdgnk_gnmain.tex), e.g. using the LaTeX mode for the lyrics, to generate [singer-style notes](samples/sdgnk/sdgnk_gn.pdf) or a complex score or song book.

### MIDI Files
PS ZitherLayout also supports exporting songs in [MIDI format](samples/sdgnk/sdgnk_znG.mid) (output mode `midi`). This allows listening to the melody in a third party program, and e.g. checking if the notation is correct.

In combine mode (`-c`|`--combine`) all input files have to be in MIDI format, all tracks in those files are combined into a common MIDI file. This can be usefull when creating a MIDI file for a multi-voice song.
